@Regression
Feature: HCP web [Activity and Care plan] flow verification

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By: Himanshu Chaudhary
  Updated On: 12/03/2022

  Background:
    Given Launch the browser
    Then Login into dashboard

  Scenario: Prerequisite Add new patient and login into patient app
    When Add a new patient
    Then Launch the "HH" App with reset
    And Scan QR and login into app

  @TestCaseKey=HOHP-T294 @Workflow
  Scenario: To verify Care Plan container under Activities and Care plan tab
    And Navigate to patient under my patients
    Then Verify Care plan page

  @Sanity
  Scenario: To verify Add and delete a Care Plan from HCP web
    And Navigate to patient under my patients
    Then Verify Add a Care plan
    And Verify Delete a Care plan

  @TestCaseKey=HOHP-T296 @Sanity @Workflow
  Scenario: To verify Delete Care Plan on Patient app
    And Launch the "HH" App with noReset
    Then Navigate to patient under my patients
    Then Verify Add a Care plan
    Then Verify Added Care plan displayed in Patient App
    And Verify Delete a Care plan
    Then Verify Deleted Care plan displayed in Patient App

  @TestCaseKey=HOHP-T295
  Scenario: To verify Add Care Plan on Patient app
    And Launch the "HH" App with noReset
    Then Navigate to patient under my patients
    Then Verify Add a Care plan
    Then Verify Added Care plan displayed in Patient App

  @TestCaseKey=HOHP-T298 @Sanity
  Scenario: Try to add care plan for other patients
    Then Navigate to patient under all patients
    And Adding a care plan should not be applicable for other patients

  @TestCaseKey=HOHP-T279
  Scenario: To verify Activity Page details under my patients
    And Navigate to patient under my patients
    Then Verify Activity Page details of clinic

  @TestCaseKey=HOHP-T279
  Scenario: To verify Activity Timeline and filter options under my patients
    Then Navigate to patient under my patients
    Then Verify Activity Page details of clinic

  @TestCaseKey=HOHP-T281
  Scenario: To verify Add an Activity
    And Launch the "HH" App with noReset
    And Navigate to patient under my patients
    Then Verify Add an Activity "Drink water"
    And Verify added activity "Drink water" in patient app

  @TestCaseKey=HOHP-T288 @Sanity @Workflow
  Scenario: To verify Delete an Activity
    And Launch the "HH" App with noReset
    And Navigate to patient under my patients
    Then Verify Add an Activity "Use nebulizer"
    And Verify added activity "Use nebulizer" in patient app
    Then Verify Update an Activity "Use nebulizer"
    And Verify updated activity "Use nebulizer" in patient app
    Then Verify Delete an Activity "Use nebulizer"
    And Verify deleted activity "Use nebulizer" in patient app

  @TestCaseKey=HOHP-T287
  Scenario: To verify Update an Activity
    And Launch the "HH" App with noReset
    And Navigate to patient under my patients
    Then Verify Add an Activity "Use inhaler"
    And Verify added activity "Use inhaler" in patient app
    Then Verify Update an Activity "Use inhaler"
    And Verify updated activity "Use inhaler" in patient app