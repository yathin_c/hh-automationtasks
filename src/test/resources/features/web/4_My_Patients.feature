@Regression
Feature: HCP web [My/All Patient] features verification flow

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By: Himanshu Chaudhary
  Updated On: 12/03/2022

  Background:
    Given Launch the browser
    And Login into dashboard

  @TestCaseKey=HOHP-T248 @Sanity @Workflow
  Scenario: Home screen should show [My patient] list by default
    Then Verify My patient list displayed

  @TestCaseKey=@HOHP-T258 @Sanity @Workflow
  Scenario: To verify navigation on LHN menus and respected pages
    Then Verify LHN menu options displayed
      | My patients  |
      | All patients |
      | Discharged   |
      | Tasks        |
      | Inventory    |
    And Verify each menu is clickable and should navigate to respected page

  @TestCaseKey=HOHP-T264
  Scenario: Verify the search input field
    Then Verify search input field is clickable, accept input and clear by clicking on cross(X) icon

  @TestCaseKey=HOHP-T265 @Sanity
  Scenario: Verify the search input field
    Then Search patient under my patients

  @TestCaseKey=HOHP-T271 @Sanity
  Scenario: Navigate to view QR code screen from patient list
    Then Search patient under my patients
    Then Verify QR code displayed by navigating through overflow menu

  @TestCaseKey=HOHP-T275 @Workflow
  Scenario: Navigate to view QR code screen from patient list
    Then Verify QR code displayed by navigating through overflow menu
    And Try to close the QR code popup page

  @TestCaseKey=HOHP-T266 @Sanity @Workflow
  Scenario: Check for [All Patient] list
    Then User should navigate successfully to All patient screen
    And All patient list of My patient and Other patient
