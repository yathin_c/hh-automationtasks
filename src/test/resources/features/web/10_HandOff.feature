@Regression
Feature: HCP web patient [Hand Off] functionality verification

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By: Himanshu Chaudhary
  Updated On: 12/03/2022

  Background:
    Given Launch the browser
    And Login into dashboard

  Scenario: Prerequisite for automation
    And Add a new patient

  @TestCaseKey=HOHP-T679
  Scenario: Check all section for handoff submission
    Then Search patient under my patients
    And Verify handoff container details

  @TestCaseKey=HOHP-T680
  Scenario: Check all section is mandatory fields
    Then Search patient under my patients
    And Verify handoff mandatory fields

  @TestCasekey=HOHP-T681
  Scenario: To verify multiple and customize of awareness
    And Navigate to patient under my patients
    And Verify all Illness severity are selectable and highlighted after selected

  @TestCaseKey=HOHP-T682
  Scenario: To verify multiple and customize of awareness
    And Navigate to patient under my patients
    Then Verify multi select awareness options

  @TestCaseKey=HOHP-T683
  Scenario: To verify multiple and customize of awareness
    And Navigate to patient under my patients
    And Verify customize option of awareness

  @TestCaseKey=HOHP-T677
  Scenario: Check user is able to cancel the reset on handoff page
    Then Search patient under my patients
    And Verify handoff container details
    Then Verify adding HandOff with "Stable" Illness severity
    And Navigate to patient under my patients
    Then Verify HandOff details after successfully HandOff patient

  @TestCaseKey=HOHP-T684
  Scenario: Check user is able to cancel the reset on handoff page
    Then Navigate to patient under my patients
    Then Update HandOff details with "Unstable" Illness severity
    And Verify decline reset No change priory visit
    Then Verify HandOff details after successfully HandOff patient

  @TestCaseKey=HOHP-T685
  Scenario: Check user only able to handoff patients assigned to him/her from all patient list page
    And Logout user
    Then Login nurse into dashboard
    And Search patient under my patients
    Then Remove this patient from my patients
    And Navigate to All patient screen
    And Search my patient under all patients
    And Verify HandOff details should be read only mode on patient list page

  @TestCaseKey=HOHP-T686
  Scenario: Check user only able to handoff patients assigned to him/her from individual patient page
    And Logout user
    Then Login nurse into dashboard
    And Navigate to All patient screen
    Then Search my patient under all patients
    And Verify HandOff details should be read only mode on patient individual page

  @TestCaseKey=HOHP-T678
  Scenario: Check user is able to cancel the reset on handoff page
    Then Navigate to patient under my patients
    Then Update HandOff details with "Watcher" Illness severity
    And Verify accept reset No change priory visit
    Then Verify Nothing should change after decline reset

