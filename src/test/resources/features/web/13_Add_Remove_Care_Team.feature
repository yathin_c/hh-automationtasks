@Regression
Feature: HCP web [Add remove care team] features verification flow

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By:
  Updated On:

  Background:
    Given Launch the browser
    And Login into dashboard

  @TestCaseKey=HOHP-T523
  Scenario: Check for the caregiver column in patient list when no caregiver is assigned to a patient
    Then Add a patient with clinician only
    When Search patient under my patients
    Then Remove this patient from my patients
    And Navigate to All patient screen
    And Search my patient under all patients
    Then Verify No caregiver message