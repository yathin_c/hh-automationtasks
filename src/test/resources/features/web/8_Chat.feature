@Regression
Feature: Audio, Video and Chat feature verification

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By: Himanshu Chaudhary
  Updated On: 12/03/2022

  Background:
    Given Launch the browser
    And  Login into dashboard

  @TestCaseKey=ChatPrerequisite
  Scenario: Prerequisite for Chat feature
    When Add a new patient
    Then Launch the "HH" App with reset
    And Scan QR and login into app

  @TestCaseKey=HOHP-T319 @Workflow
  Scenario: To verify chat container and send text message to patient
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    Then Navigate to patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient details screen
    Then Verify sending 2 text message from HCP web dashboard

  @TestCaseKey=HOHP-T320
  Scenario: Try to read the received chat
    Given Launch the "HH" App with noReset
    Then Verify sending 2 text message from Patient app to clinician
    Then Search patient under my patients
    And Verify badge count on HCP web dashboard

  @TestCaseKey=HOHP-T321 @Sanity @Workflow
  Scenario: Try to read the received chat
    Given Launch the "HH" App with noReset
    Then Verify sending 2 text message from Patient app to clinician
    Then Search patient under my patients
    And Verify received text message in HCP web dashboard

  @TestCaseKey=HOHP-T322 @Sanity
  Scenario: Try to send chat from web dashboard
    Given Launch the "HH" App with noReset
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient and click Check now
    And Verify text message received in patient app

  @TestCaseKey=HOHP-T323 @Workflow
  Scenario: Try to do chat while doing video/audio call
    And Launch the "HH" App with noReset
    Then Search patient under my patients
    And Start Video call
    Then Verify Accept call from patient app
    And Minimize the call container
    And Verify sending text message from HCP web dashboard

  @TestCaseKey=HOHP-T325
  Scenario: Try to do video call
    Given Launch the "HH" App with noReset
    Then Navigate to patient under my patients
    And Verify start Video call on patient details screen
    Then Receive Video call on patient app
    And Verify Accept call from patient app
    Then Verify call Accepted successfully in HCP web dashboard
    And Verify Video call container details in HCP web dashboard

  @TestCaseKey=HOHP-T326 @Sanity @Workflow
  Scenario: Minimize and maximize the video/audio screen
    Then Search patient under my patients
    And Start Audio call
    Then Verify minimize and maximize chat container
    Then Navigate to patient under my patients
    And Start Video call
    Then Verify minimize and maximize chat container

  @TestCaseKey=HOHP-T327
  Scenario: Try to drag the call popup in vital screen
    Then Search patient under my patients
    And Start Audio call
    Then Verify perform action without minimize the call container
    And Minimize the container and drag on other location
    Then Perform action after minimize the call container

  @TestCaseKey=HOHP-T328 @Workflow
  Scenario: Navigate to web screens while doing video/audio call
    And Launch the "HH" App with noReset
    Then Search patient under my patients
    And Start Video call
    Then Verify Accept call from patient app
    And Minimize the call container
    Then Verify Video and Audio call icons are disable while clinician in call
    And Verify sending text message from HCP web dashboard
    Then Navigate to My patient screen
    And Verify sending text message for other patient from HCP web dashboard
    Then Navigate to My patient screen
    And Verify Video, Audio call options are disable for other patients

  @TestCaseKey=HOHP-T329
  Scenario: Verify the video option for other patient in My patient list during an ongoing call
    And Launch the "HH" App with noReset
    Then Search patient under my patients
    And Start Video call
    Then Verify Accept call from patient app
    And Minimize the call container
    Then Verify Video and Audio call icons are disable while clinician in call
    Then Navigate to My patient screen
    And Verify Video, Audio call options are disable for other patients

  @TestCaseKey=HOHP-T330
  Scenario: To verify send single text message to patient
    Then Search patient under my patients
    And Verify start Video call on patient home screen and cut
    Then Navigate to patient under my patients
    And Verify start Audio call on patient details screen and cut

  @TestCaseKey=HOHP-T331
  Scenario: Verify the chat, audio and video call option in patient list screen for other patient
    Then Navigate to All patient screen
    And Search patient under all patients without refresh
    Then Verify Video, Audio call and chat icons are invisible for unassigned patients

  @TestCaseKey=HOHP-T332
  Scenario: Verify the chat, audio and video call option in patient list screen for other patient
    And Navigate to patient under all patients
    Then Verify Video, Audio call and chat icons are disable for unassigned patients

  @TestCaseKey=HOHP-T333 @Sanity @Workflow
  Scenario: Try to receive the call
    Given Launch the "HH" App with noReset
    Then Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard

  @TestCaseKey=HOHP-T334 @Sanity @Workflow
  Scenario: Try to decline the call
    Given Launch the "HH" App with noReset
    Then Navigate to clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  @TestCaseKey=HOHP-T335
  Scenario: Verify the call connectivity by staying minimum 1 min in call
    Given Launch the "HH" App with noReset
    Then Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call connectivity more than 1 min