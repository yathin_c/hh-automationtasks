@Regression
Feature: HCP web set [Therese-hold] values and alerts features

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By: Himanshu Chaudhary
  Updated On: 12/03/2022

  Background:
    Given Launch the browser
    When Login into dashboard

  Scenario: Prerequisite
    When Add a new patient

  @TestCaseKey=HOHP-T304 @TestCaseKey=HOHP-T307 @TestCaseKey=HOHP-T308 @TestCaseKey=HOHP-T309
  Scenario:Verify set therese hold value container, default values, set and reset values
    And Navigate to patient under my patients
    Then verify therese hold option and container
    And verify default therese hold values
    Then verify closing therese hold container

  @TestCaseKey=HOHP-T305 @TestCaseKey=HOHP-T565 @TestCaseKey=HOHP-T306
  Scenario: Verify the confirmation message while resetting the threshold limit
    And Navigate to patient under my patients
    And Verify setting therese hold values
    Then Verify reset therese hold values

  @TestCaseKey=HOHP-T311 @TestCaseKey=HOHP-T312 @TestCaseKey=HOHP-T313 @TestCaseKey=HOHP-T314
  Scenario:Verify open/close the alert popup, badge count and empty screen
    And Navigate to patient under my patients
    Then verify alert option on individual patient page
    And verify badge count when no alert

  @TestCaseKey=HOHP-T306 @TestCasekey=HOHP-T315 @TestCasekey=HOHP-T316
  Scenario:Verify that the Event is triggering or not on set threshold values
    And Navigate to patient under my patients
    Then verify alert after set therese hold values for vital "High HR" as 100 and sync for 30 minutes with "Everion"
    And verify resolving an alert and status after resolved

  @TestCaseKey=HOHP-T1297 @TestCasekey=HOHP-T1298 @TestCaseKey=HOHP-T1300
  Scenario: Verify global event alert, badge count and resolve it
    And Navigate to patient under my patients
    Then verify alert badge count and triggered event should be visible in global alert
    And Navigate to patient under all patients
    Then verify alert badge count and triggered event should not be visible in global alert

  @TestCasekey=HOHP-T310 @TestCaseKey=HOHP-T317 @TestCaseKey=HOHP-T1299
  Scenario: Try to set the threshold and resolve alert for other patient in All patient list
    And Navigate to patient under all patients
    Then verify set therese hold values for un-assigned patient
    And verify alert for vital "High HR" with "Everion"

  #@TestCaseKey=HOHP-T318
    # @No yet completed because simulator is not working
  #Scenario: Check for maximum number of badge count shown for alert