@Regression
Feature: HCP web [Clinical Notes] flow verification

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By: Himanshu Chaudhary
  Updated On: 12/03/2022

  Background:
    Given Launch the browser
    Then Login into dashboard

  @TestCaseKey=HOHP-T982 @Dev
  Scenario: To verify Clinical Notes page
    And Navigate to patient under my patients
    Then Verify clinical notes page

  Scenario: To verify Clinical Notes mandatory fields error messages
    And Navigate to patient under my patients
    Then Verify empty fields error message

  @TestCaseKey=HOHP-T983 @Sanity
  Scenario: To verify add new Clinical note
    Then Navigate to patient under my patients
    And Verify Add a note

  @TestCaseKey=HOHP-T984 @Sanity @Workflow
  Scenario: To verify update a Clinical note
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Verify update a note

  @TestCaseKey=HOHP-T985 @Sanity @Workflow
  Scenario: To verify delete a Clinical note
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Verify Delete a note

  @TestCaseKey=HOHP-T986
  Scenario:Verify Search filter options of Clinical note
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Verify Search filter input field

  @TestCaseKey=HOHP-T987
  Scenario:Verify Calendar filter options of Clinical note
    Then Navigate to patient under my patients
    And Verify Add a note
    And Verify Calendar filter option

  @TestCaseKey=HOHP-T988
  Scenario:Verify Caregiver filter options of Clinical note
    Then Navigate to patient under my patients
    And Verify Add a note
    And Verify Caregiver filter option

  @TestCaseKey=HOHP-T990
  Scenario:Verify Calendar and Caregiver filter options of Clinical note
    Then Navigate to patient under my patients
    And Verify Add a note
    And Verify Calendar and Caregiver filter options together

  @TestCaseKey=HOHP-T989
  Scenario:Verify Clear all filters options
    Then Navigate to patient under my patients
    And Verify Add a note
    And Verify Calendar and Caregiver filter options together and Clear all

  @TestCaseKey=HOHP-T1392 @Sanity @Workflow
  Scenario: To verify Add, Update, Delete and Search a Clinical note for not assigned caregiver
    Then Navigate to patient under all patients
    And Verify Add a note
    Then Verify update a note
    Then Verify Delete a note

  @TestCaseKey=HOHP-T991
  Scenario: Verify that caregiver is not able to edit/delete clinical notes of the patient which have been created by other caregivers
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Logout user
    And Login nurse into dashboard
    Then Navigate to patient under my patients
    And Verify edit and delete option not visible