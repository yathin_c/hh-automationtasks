@Regression
Feature: HCP web [Tasks] functionality verification

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By: Himanshu Chaudhary
  Updated On: 12/03/2022

  Background:
    Given Launch the browser
    And Login into dashboard

  @TestCaseKey=HOHP-T689 @Sanity @Workflow
  Scenario: Verify Task page navigation by clicking on LHN menu
    When Navigate to Tasks tab on HCP web dashboard
    Then verify Task page navigated successfully

  @TestCaseKey=HOHP-T687 @Sanity @Workflow
  Scenario: Verify Active Task Tab
    When Navigate to Tasks tab on HCP web dashboard
    Then verify Task page navigated successfully
    And verify Active task tab

  @TestCaseKey=HOHP-T688 @Sanity @Workflow
  Scenario: Verify Archived Task Tab
    When Navigate to Tasks tab on HCP web dashboard
    Then verify Task page navigated successfully
    And verify Archived task tab

  @TestCaseKey=HOHP-T698 @Sanity @Workflow
  Scenario: Verify User is able to add new task for patients on individual patient page
    When Navigate to patient under my patients
    Then verify create new task under Patient Page with Required fields in HCP web

  @TestCaseKey=HOHP-T701
  Scenario: Verify User is able to edit new task for patients on individual patient page
    When Navigate to patient under my patients
    Then verify create new task under Patient Page with Required fields in HCP web
    Then verify edit task with All fields

  @TestCaseKey=HOHP-T710 @Workflow
  Scenario: Verify User is able to comment on task for patients on individual patient page
    When Navigate to patient under my patients
    Then verify create new task under Patient Page with Required fields in HCP web
    And verify task comment on Patient page

  @TestCaseKey=HOHP-T699 @Workflow
  Scenario: Verify User is able to add new task for patients under Tasks Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify create new task under Tasks Page with All fields in HCP web
    And verify task comment on Task page
    Then verify edit task with Required fields

  @TestCaseKey=HOHP-T700
  Scenario: Verify User is able to edit new task for patients under Tasks Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify create new task under Tasks Page with All fields in HCP web
    Then verify edit task with Required fields

  @TestCaseKey=HOHP-T709
  Scenario: Verify User is able to comment on task for patients under Tasks Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify create new task under Tasks Page with All fields in HCP web
    And verify task comment on Task page

  @TestCaseKey=HOHP-T693 @Workflow
  Scenario: Verify User is able to complete and reopen the task from Individual patient page
    When Navigate to patient under my patients
    Then verify create new task under Patient Page with All fields in HCP web
    When Verify complete a task
    Then verify completed task section displayed correctly

  @TestCaseKey=HOHP-T703 @Sanity
  Scenario: Verify User is able to complete and reopen the task from Individual patient page
    When Navigate to patient under my patients
    Then verify create new task under Patient Page with All fields in HCP web
    When Verify complete a task

  @TestCaseKey=HOHP-T705
  Scenario: Verify User is able to complete and reopen the task from Individual patient page
    When Navigate to patient under my patients
    Then verify create new task under Patient Page with All fields in HCP web
    When Verify complete a task
    And Verify re-open completed task

  @TestCaseKey=HOHP-T702
  Scenario: Verify User is able to complete and reopen the task from Tasks Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify create new task under Tasks Page with Required fields in HCP web
    And Verify complete a task

  @TestCaseKey=HOHP-T704 @Sanity @Workflow
  Scenario: Verify User is able to complete and reopen the task from Tasks Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify create new task under Tasks Page with Required fields in HCP web
    And Verify complete a task
    Then verify completed task section displayed correctly
    And Verify re-open completed task on tasks tab

  @TestCaseKey=HOHP-T691
  Scenario: Verify User is able to complete and reopen the task from Tasks Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify create new task under Tasks Page with Required fields in HCP web
    And Verify complete a task
    Then verify completed task section displayed correctly

  @TestCaseKey=HOHP-T696
  Scenario: Verify Cancel button works as expected when adding new task on Task Page
    When Navigate to Tasks tab on HCP web dashboard
    Then verify cancel button functionality on Tasks page while creating new task

  @TestCaseKey=HOHP-T697
  Scenario: Verify Cancel button works as expected when adding new task on Individual patient page
    When Navigate to patient under my patients
    Then verify cancel button functionality on Patient page while creating new task