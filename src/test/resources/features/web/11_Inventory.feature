@Regression
Feature: HCP web patient [Inventory] functionality verification

  Created By: Himanshu Chaudhary
  Created On: 12/03/2022
  Updated By:
  Updated On:

  Background:
    Given Launch the browser
    And Login into dashboard

  Scenario: To verify Add a device in inventory
    Then Navigate to Inventory tab on HCP web dashboard
    And Add new device "AED - [Care Team]" in inventory
    And Assign device to caregiver
    And Delete device