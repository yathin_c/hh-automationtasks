Feature: Patient app [My health] screen validation

  Background:
    Given Launch the browser
    And Launch the "HH" App with noReset
    And Login into dashboard

  @TestCaseKey=HOHP-T144 @TestCaseKey=HOHP-T835 @TestCaseKey=HOHP-T849
  Scenario: Verify My health screen vitals, enter vitals button and Close icon of Select vital popup
    Then verify my health screen vitals
    And is Enter vitals button displayed
    Then click on Enter vitals button and verify close icon

  @TestCaseKey=HOHP-T836 @TestCaseKey=HOHP-T837 @TestCaseKey=HOHP-T838 @TestCaseKey=HOHP-T839
  Scenario: Verify Entering BP vitals manually
    And verify entering invalid BP vitals and close pop-up icon
    When verify entering valid BP vitals
    And Navigate to patient under my patients
    Then verify entered Blood pressure value on details page

  @TestCaseKey=HOHP-T840 @TestCaseKey=HOHP-T841 @TestCaseKey=HOHP-T842
  Scenario: Verify Entering Weight vitals manually
    And verify entering invalid Weight and cancel option
    When verify entering valid Weight
    And Navigate to patient under my patients
    Then verify entered Weight value on details page

  @TestCaseKey=HOHP-T843 @TestCaseKey=HOHP-T844 @TestCaseKey=HOHP-T845
  Scenario: Verify Entering Temporal temperature vitals manually
    And verify entering invalid Temporal temperature and cancel option
    When verify entering valid Temporal temperature
    And Navigate to patient under my patients
    Then verify entered Temporal temperature value on details page

  @TestCaseKey=HOHP-T846 @TestCaseKey=HOHP-T847 @TestCaseKey=HOHP-T848
  Scenario: Verify Entering SpO2 vitals manually
    And verify entering invalid SpO2 and cancel option
    When verify entering valid SpO2
    And Navigate to patient under my patients
    Then verify entered SpO2 value on details page

  @TestCaseKey=HOHP-T955 @TestCaseKey=HOHP-T956 @TestCaseKey=HOHP-T957
  Scenario: Verify Entering Blood Glucose vitals manually
    And verify entering invalid Blood Glucose and cancel option
    When verify entering valid Blood Glucose
    And Navigate to patient under my patients
    Then verify entered Blood glucose value on details page
