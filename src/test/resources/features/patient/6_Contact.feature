Feature: Patient app [Contact] feature validation

  Background:
    Given Launch the "HH" App with noReset

  Scenario: To verify chat options on all screens and send text message
    Then Verify Contact option visible on all screens
    And Navigate to clinician contact
    Then Verify sending text message from patient app

  Scenario: To verify start an audio call
    And Navigate to clinician contact
    Then Verify start Audio call from patient app

  Scenario: To verify start a video call
    And Navigate to clinician contact
    Then Verify start Video call from patient app


    #####END TO END#######
  Scenario: Verify make audio call from patient app and reject on HCP web dashboard
    And Login into dashboard
    Then Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  Scenario: Verify make audio call from patient app and receive on HCP web dashboard
    And Login into dashboard
    Then Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call Accepted successfully in patient app
    Then Verify End call from HCP Web dashboard
    And Verify Call ended successfully in patient app

  Scenario: Verify make video call from patient app and reject on HCP web dashboard
    And Login into dashboard
    Then Navigate to clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  Scenario: Verify make video call from patient app and receive on HCP web dashboard
    And Login into dashboard
    Then Navigate to clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call Accepted successfully in patient app
    Then Verify End call from HCP Web dashboard
    And Verify Call ended successfully in patient app

  Scenario: Verify make audio call from HCP web dashboard and reject on patient app
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify start Audio call on patient details screen
    Then Receive Audio call on patient app
    And Verify Reject call from patient app
    Then Verify call Rejected successfully in HCP web dashboard

  Scenario: Verify make audio call from HCP web dashboard and receive on patient app
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify start Audio call on patient details screen
    Then Receive Audio call on patient app
    And Verify Accept call from patient app
    Then Verify call Accepted successfully in HCP web dashboard
    Then Verify End call from patient app
    And Verify Call ended successfully in HCP Web dashboard

  Scenario: Verify make video call from HCP web dashboard and reject on patient app
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify start Video call on patient details screen
    Then Receive Video call on patient app
    And Verify Reject call from patient app
    Then Verify call Rejected successfully in HCP web dashboard

  Scenario: Verify make video call from HCP web dashboard and receive on patient app
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify start Video call on patient details screen
    Then Receive Video call on patient app
    And Verify Accept call from patient app
    Then Verify call Accepted successfully in HCP web dashboard
    Then Verify End call from patient app
    And Verify Call ended successfully in HCP Web dashboard

  Scenario: Verify Send message to patient
    And Login into dashboard
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient
    Then Verify chat count badge in patient app
    And Verify received text message from HCP clinician "Himanshu"

  Scenario: Verify Send message to patient
    And Login into dashboard
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 2 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient
    Then Verify chat count badge in patient app
    And Verify received text message from HCP clinician "Himanshu"

  Scenario: Verify Send message to patient
    And Login into dashboard
    Then Verify sending 1 text message from Patient app to clinician "Himanshu"
    Then Search patient under my patients
    And Verify badge count and chat on HCP web dashboard

  Scenario: Verify Send message to patient
    And Login into dashboard
    Then Verify sending 2 text message from Patient app to clinician "Himanshu"
    Then Search patient under my patients
    And Verify badge count and chat on HCP web dashboard
