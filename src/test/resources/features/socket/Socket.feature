@Load
Feature: Web-socket load testing for emit messages mobileDeviceStatusUpdate, heartbeat and deviceStatusUpdate

  Background:
    Given Generate patient data for load execution

  Scenario: Load testing using web-socket
    And Generate executable artillery file
    Then Start load testing execution

