Feature: Clinical Notes feature HCP App

  Background:
    Given Launch the browser
    Given Login into dashboard
    Given Launch the "HCP" App with reset

  Scenario: Prerequisite for automation
    Given Add a new patient
    Then Login into HCP app

  @TestCaseKey=HOHP-T1260
  Scenario: Verify Clinical notes screen in HCP
    And Search My patient in HCP APP and open Clinical notes
    Then verify clinical notes page

  @TestCaseKey=HOHP-T1261
  Scenario: Verify Clinical note UI auto-refresh when updates are done from web dashboard
    And Search My patient in HCP APP and open Clinical notes
    Then Navigate to patient under my patients
    When Verify Add a note
    Then verify auto refresh clinical note screen
    Then Verify update a note
    Then verify auto refresh clinical note screen
    Then Verify Delete a note
    Then Verify deleted note not displayed in HCP App
    
  @TestCaseKey=HOHP-T1262
  Scenario: Verify Clinical note UI auto-refresh when updates are done from web dashboard for unassigned patient
    And Search Unassigned patient in HCP APP and open Clinical notes
    Then Navigate to patient under all patients
    When Verify Add a note
    Then verify auto refresh clinical note screen
    Then Verify update a note
    Then verify auto refresh clinical note screen
    Then Verify Delete a note
    Then Verify deleted note not displayed in HCP App