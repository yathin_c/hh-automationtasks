Feature: HCP Authentication

  Background:
    Given Launch the "HCP" App with noReset
    Then Launch the browser

  Scenario: Verify HCP APP login button enable / disable
    Then Verify Login button is disable without filling email and password
    And Verify Login button is disable after filling email only
    And Verify Login button is disable after filling password only
    Then Verify Login button is enable after filling email and password

  @TestCaseKey=HOHP-T417 @TestCaseKey=HOHP-T397
  Scenario: Verify Login with invalid credentials
    Then Enter invalid email and password
    And Verify invalid user password message

  @TestCaseKey=HOHP-T421 @TestCaseKey=HOHP-T420
  Scenario: Verify HCP app Logout
    When Login into HCP app
    Then verify HCP app logout successfully

  @TestCaseKey=HOHP-T416 @TestCaseKey=HOHP-T418
  Scenario: Verify Login with valid credentials
    Then Enter valid email and password
    And Verify login successfully
    When Login into dashboard
    Then verify user should be logged in both browser and app
    And logout HCP user
    Then Enter valid email and password
    And Verify login successfully
    Then verify user should be logged in both browser and app


