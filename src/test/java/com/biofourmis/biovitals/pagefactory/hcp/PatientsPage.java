package com.biofourmis.biovitals.pagefactory.hcp;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class PatientsPage extends CommonUtils {

    private WebDriver driver;

    public PatientsPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @iOSXCUITFindBy(id = "ic search")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/searchIcon")
    private WebElement search;

    @iOSXCUITFindBy(xpath = "//*[contains(@value,'Search by Name')]")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/searchEditText")
    private WebElement searchInput;

    @iOSXCUITFindBy(xpath = "//*[@label='ic overflow menu']")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/menuIcon")
    private WebElement threeDots;

    @iOSXCUITFindBy(id = "Clinical notes")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvClinicalNotes")
    private WebElement clinicalNotesOption;

    @iOSXCUITFindBy(id = "View tasks")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvPatientTasks")
    private WebElement viewTaskOption;

    @iOSXCUITFindBy(id = "Handoff patient")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tv_handoff_BS")
    private WebElement handOffOption;

    @iOSXCUITFindBy(id = "Remove from My patients")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvAddRemovePatient")
    private WebElement AddRemovePatientOption;

    @iOSXCUITFindBy(id = "Cancel")
    private WebElement cancel;

    public void searchPatient(String mrn) throws Exception {
        _waitForProgressBarDisappearHCP();
        _waitForElementVisible(search);
        _sleep(5);
        _click(search);
        _waitForElementVisible(searchInput);
        _sendKeys(searchInput, mrn);
        _waitForProgressBarDisappearHCP();
    }

    public void openPatientMenu() throws Exception {
        _waitForProgressBarDisappearHCP();
        _click(threeDots);
    }

    public void clickOnMenu(String menu) throws Exception {
        _waitForProgressBarDisappearHCP();
        _clickOnText(menu);
        _waitForProgressBarDisappearHCP();
    }

    public boolean isPatientPageDisplayed() {
        _waitForProgressBarDisappearHCP();
        return _isTextContainsPresent("My patients");
    }

    public void clickOnCancel() throws Exception {
        _click(cancel);
    }

    public void cancelSearchOption() throws Exception {
        _click(By.xpath("//XCUIElementTypeButton[@label='Cancel']"));
    }

    public void navigateToAllPatients() throws Exception {
        _waitForProgressBarDisappearHCP();
        _clickOnText("All patients");
        _waitForProgressBarDisappearHCP();
    }
}
