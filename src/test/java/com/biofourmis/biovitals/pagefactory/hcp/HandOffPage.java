package com.biofourmis.biovitals.pagefactory.hcp;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.cucumber.java.an.E;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j
public class HandOffPage extends CommonUtils {

    private WebDriver driver;

    public HandOffPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@label='Hand off']")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/handOffBtn")
    private WebElement handOffBtn;

    @iOSXCUITFindBy(xpath = "//*[@label='Patient summary']/..//XCUIElementTypeTextView")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/patientSummaryET")
    private WebElement summary;

    @iOSXCUITFindBy(id = "Patient summary")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tilPatientSummary")
    private WebElement summartTitle;

    @iOSXCUITFindBy(id = "icEdit")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/etOtherInput")
    private WebElement customSituation;

    @iOSXCUITFindBy(id = "Synthesis")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/synthesisHeading")
    private WebElement synthesisHeading;

    @iOSXCUITFindBy(xpath = "//*[@label='Synthesis']/..//XCUIElementTypeTextView")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tilNotes")
    private WebElement synthesisTitile;

    @iOSXCUITFindBy(xpath = "//*[@label='Synthesis']/..//XCUIElementTypeTextView")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/notesET")
    private WebElement note;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@label='No change from prior visit']")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/noChangesCB")
    private WebElement noChange;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@label='Proceed']")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/btnYes")
    private WebElement btnProceed;

    @iOSXCUITFindBy(id = "Patients")
    private WebElement backToMyPatients;

    @iOSXCUITFindBy(xpath = "//*[@name=\"Patients\"]")
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/bottom_nav_patients")
    private WebElement patientsTabOnHomeScreen;

    public void waitForhandOffPageLoad() {
        _waitForProgressBarDisappearHCP();
        _waitForElementVisible(handOffBtn);
    }

    public void selectSeverity(String s) throws Exception {
        _clickOnTextContains(s);
    }

    public void enterSummary(String s) throws Exception {
        _click(summartTitle);
        _sendKeys(summary, s);
    }

    public String selectSituationalOptions(String s, boolean isPredefined) throws Exception {
        if (isPredefined) {
            String[] str = s.split(",");
            _clickOnTextContains(str[str.length - 1].strip());
        } else {
            _swipeUp(1);
            _click(customSituation);
            _sendKeys(customSituation, s);
            _hideKeyBoard();
        }
        return "error";
    }

    public void enetrSynthesis(String s) throws Exception {
        _swipeUp(1);
        _sleep(2);
        _click(synthesisTitile);
        _sendKeys(note, s);
    }

    public void clickOnHandOffbtn() throws Exception {
        _click(handOffBtn);
    }

    public boolean ishandOffBtnEnable() throws Exception {
        _waitForProgressBarDisappearHCP();
        return _isElementEnable(handOffBtn);
    }

    public void selectNoChanges() throws Exception {
        _click(noChange);
        _click(btnProceed);
        _waitForElementInvisible(handOffBtn);
    }

    public boolean isHandoffPageDisplayed() {
        return _isTextContainsPresent("Patient Handoff");
    }

    public void swipeUp() {
        try {
            _swipeUp(1);
            _sleep(2);
        } catch (Exception e) {
        }
    }

    public void swipeDown() {
        try {
            _swipeDown(1);
            _sleep(2);
        } catch (Exception e) {
        }
    }

    public boolean isTextDisplayed(String text) {
        return _isTextContainsPresent(text);
    }

    public boolean isHandOffBtnDisplayed() {
        return _isElementVisible(handOffBtn);
    }

    public boolean isCustomiseOption() {
        if (driver instanceof IOSDriver)
            return _isElementVisible(By.xpath("//*[@value='Customise your own option']"));
        else
            return _isTextContainsPresent("Customise your own option");
    }


    public boolean waitForProceedPop() {
        return _waitForElementVisible(btnProceed);
    }

    public void clickOnCancelOption() throws Exception {
        _clickOnTextContains("Cancel");
    }

    public void clickOnProceedOption() throws Exception {
        _clickOnTextContains("Proceed");
        _waitForProgressBarDisappearHCP();
    }

    public boolean verifyHandOffSuccessToast(String name) {
        _waitForProgressBarDisappearHCP();
        return _isTextContainsPresent(name + " is handed off successfully");
    }

    public boolean verifyhandOffSummary(String summary) throws Exception {
        return _getText(this.summary).strip().equalsIgnoreCase(summary.strip());
    }

    public void clickNoChange() throws Exception {
        _click(noChange);
    }

    public String getHandOffSummary() throws Exception {
        return _getText(this.summary);
    }

    public boolean isNoChaneSelected() {
        if (driver instanceof IOSDriver) {
            return driver.findElement(By.xpath("//XCUIElementTypeButton[@label='No change from prior visit']")).isSelected();
        } else {
            throw new RuntimeException("Fix it here.");
        }
    }

    public void goBackToMyPatient() throws Exception {
        _click(backToMyPatients);
        _waitForProgressBarDisappearHCP();
    }

    public boolean isHomePageDisplayed() {
        _waitForProgressBarDisappearHCP();
        return _isElementVisible(patientsTabOnHomeScreen);
    }

    public boolean lastUpdatedTime(String value) {
        return _isTextContainsPresent(value);
    }

    public boolean isSummaryEditable() throws Exception {
        _click(summartTitle);
        _sendKeys(summary, "ABCD-ABCD");
        return !_getText(summary).equals("ABCD-ABCD");
    }

    public boolean isAutoReloadPopTextDisplayed(String s) {
        return _isElementVisible(By.xpath("//XCUIElementTypeStaticText[@label='" + s + "']"));
    }

    public boolean waitForAutoReloadHandOffPopUp() {
        _waitForProgressBarDisappearHCP();
        _sleep(8);
        return _waitForElementVisible(By.xpath("//XCUIElementTypeStaticText[@label='Someone else has updated handoff details']"));
    }
}

