package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import com.github.javafaker.Faker;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.SkipException;

import java.time.LocalDateTime;
import java.util.*;

@Log4j
public class ActivityNCarePlanPage extends CommonUtils {

    private WebDriver driver;
    private Faker faker;

    public ActivityNCarePlanPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        faker = new Faker();
    }

    @FindBy(xpath = "//*[contains(@class,'care-plan')]//*[contains(text(),'Care plan')]")
    private WebElement title;

    @FindBy(xpath = "//*[contains(@class,'care-plan')]//*[contains(text(),'Add a plan')]")
    private WebElement addPlanText;

    @FindBy(xpath = "//*[contains(@class,'care-plan')]//*[@type='add--alt']")
    private WebElement addPlanIcon;

    @FindBy(id = "planName")
    private WebElement planInput;

    @FindBy(id = "isRecurringPlans")
    private WebElement isRecurringPlans;

    @FindBy(xpath = "//*[contains(@class,'care-plan')]//*[@type='ic-close']")
    private WebElement closePlanInput;

    @FindBy(id = "filterBtn")
    private WebElement adherence;

    @FindBy(xpath = "//*[@id='filterBtn']//following::div//*[contains(@class,'list-item')]")
    private List<WebElement> adherenceMenu;

    @FindBy(xpath = "//*[@id='activity-drag-item-0']//following::div[@class='label']")
    private List<WebElement> activityMenu;

    @FindBy(xpath = "//*[contains(@class,'timeline-sidebar') and contains(@class,'h-auto')]//child::div")
    private List<WebElement> activityTimeline;

    @FindBy(xpath = "//*[contains(text(),'Save')]")
    private WebElement saveActivity;

    @FindBy(xpath = "//*[contains(@class,'show-popout')]//*[@type='trash-can']")
    private WebElement deleteActivityIcon;

    private By deletePlan(String plan) {
        return By.xpath("//*[contains(@class,'care-plan')]//*[contains(text(),\"" + plan + "\")]/..//*[@type='trash-can']");
    }

    private By hoverAPlan(String plan) {
        return By.xpath("//*[contains(@class,'care-plan')]//*[contains(text(),\"" + plan + "\")]");
    }

    private By activity(String activityName) {
        return By.xpath("//*[@id='activity-drag-item-0']//*[contains(text(),'" + activityName + "')]");
    }

    private By timelineSpace(int count) {
        return By.xpath("//*[@id='timeline-drag-item-" + ((+count / 2)) + "']");
    }

    private By numberOfGlass(int no) {
        return By.xpath("//*[@formcontrolname ='numberOfOccasions']//*[@value='" + no + "']//parent::label");
    }

    private By activityInTimeline(String activity) {
        return By.xpath("//*[contains(@class,'activity-timeline-item')]//*[contains(text(),'" + activity + "')]");
    }

    private By activityInTimelineHandle(String activity) {
        return By.xpath("//*[contains(@class,'activity-timeline-item')]//*[contains(text(),'" + activity + "')]/../..//*[contains(@class,'cdk-drag-handle')]");
    }


    public void navigateToActivityNCarePlan() throws Exception {
        _waitForPageLoad();
        log("Click on Activities & Care plan.");
        _clickOnTextContains("Activities & Care plan ");
        _waitForPageLoad();
    }

    public boolean verifyCarePlanPanelDetails() throws Exception {
        log("Verify care plan title.");
        Assert.assertTrue(_isElementVisible(title), "Care plan title not visible.");
        log("Verify Add care plan option is visible.");
        Assert.assertTrue(_isElementVisible(addPlanText), "Add a plan option not visible.");
        return _isElementVisible(addPlanIcon);
    }

    public boolean addCarePlan(String quote) throws Exception {
        log("Click on Add plan text.");
        _click(addPlanText);
        log("Enter values in input filed.");
        _sendKeys(planInput, quote);
        log("Click on ENTER button.");
        _sendKeys(planInput, Keys.ENTER);
        _waitForLoading();
        return _isTextContainsPresent(quote);
    }

    public boolean deleteCarePlan(String carePlan) throws Exception {
        _turnOnImplicitWaits();
        log("Hover on care plan. Message : [" + carePlan + "]");
        _mouseOver(hoverAPlan(carePlan));
        log("Click on delete icon besides care plan message.");
        _waitForElementVisible(deletePlan(carePlan));
        _click(deletePlan(carePlan));
        _waitForPageLoad();
        _turnOffImplicitWaits();
        return _isTextContainsPresent(carePlan);
    }


    public boolean verifyAddPlanTextClickable() {
        try {
            log("Click on Add plan.");
            _click(addPlanText);
            log("Compare add plan input box placeholder [Enter a plan].");
            return _getAttributeValue(planInput, "placeholder").equalsIgnoreCase("Enter a plan");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyCancelPlanInput() {
        try {
            log("Click on close add care plan input field");
            _click(closePlanInput);
            return _isElementVisible(addPlanText);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyAddPlanIconClickable() {
        try {
            log("Click on Add care plan icon.");
            _click(addPlanIcon);
            return _getAttributeValue(planInput, "placeholder").equalsIgnoreCase("Enter a plan");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyActivityPageDetails() {
        String todayDate = LocalDateTime.now().getMonth().name().substring(0, 1).toUpperCase(Locale.ROOT) +
                LocalDateTime.now().getMonth().name().substring(1, 3).toLowerCase(Locale.ROOT) + " " + LocalDateTime.now().getDayOfMonth() +
                ", " + LocalDateTime.now().getYear();
        Assert.assertTrue(_isTextContainsPresent("Drag and drop activities onto the timeline."), "Text [Drag and drop activities onto the timeline] not displayed.");
        Assert.assertTrue(_isTextContainsPresent("Activity timeline"), "Text [Activity timeline] not displayed.");
        Assert.assertTrue(_isTextContainsPresent("Adherence"), "Filter [Adherence] not displayed.");
        return _isTextContainsPresent(todayDate);
    }

    public boolean verifyAdherenceFilter() throws Exception {
        log("Click on Adherence filter.");
        _click(adherence);
        _waitForPageLoad();
        return _isTextContainsPresent("Apply filter");
    }

    public List<String> verifyAdherenceFilterItems() {
        _waitForPageLoad();
        List<String> actualMenu = new ArrayList<>();
        log("Collect present activities from filter.");
        for (WebElement element : adherenceMenu)
            actualMenu.add(element.getText().strip());
        //Set<String> hSet = new HashSet<>(menu);
        return actualMenu;
    }

    public boolean verifyActivities(List<String> menu) {
        _waitForPageLoad();
        Set<String> actualMenu = new HashSet<>();
        log("Collect present activities from LHN menu.");
        for (WebElement element : activityMenu)
            actualMenu.add(element.getText().strip());
        Set<String> hSet = new HashSet<>(menu);
        return hSet.equals(actualMenu);
    }

    public ArrayList<String> verifyActivityTimeline() {
        ArrayList<String> actual = new ArrayList<>();
        log("Collect present activities timeline values.");
        for (WebElement element : activityTimeline) {
            String value = element.getText().strip();
            if (!actual.contains(value))
                actual.add(value);
        }
        return actual;
    }

    public void addActivityDrinkWater(String arg0, String time, Integer glass) throws Exception {
        WebElement element = driver.findElement(activity(arg0));
        _scrollToElement(element);
        int count = 0;
        for (WebElement element1 : activityTimeline) {
            count++;
            if (element1.getText().equalsIgnoreCase(time)) {
                WebElement to = driver.findElement(timelineSpace(count));
                scrollTillTimeline(to);

                try {
                    Actions builder = new Actions(driver);
                    Action dragAnddrop = builder.clickAndHold(element)
                            .moveToElement(to)
                            .release(to)
                            .build();
                    dragAnddrop.perform();
                    break;
                } catch (MoveTargetOutOfBoundsException e) {
                    element = driver.findElement(activity(arg0));
                    _scrollToElement(element);
                    count = 0;
                    for (WebElement elementIn1 : activityTimeline) {
                        count++;
                        if (elementIn1.getText().equalsIgnoreCase(time)) {
                            to = driver.findElement(timelineSpace(count));
                            scrollTillTimeline(to);

                            try {
                                Actions builder = new Actions(driver);
                                Action dragAnddrop = builder.clickAndHold(element)
                                        .moveToElement(to)
                                        .release(to)
                                        .build();
                                dragAnddrop.perform();
                                break;
                            } catch (MoveTargetOutOfBoundsException e1) {
                                throw new SkipException("Not able to drag and drop activity. Please check you drag and drop method.");
                            }
                        }
                    }
                    //throw new SkipException("Not able to drag and drop activity. Please check you drag and drop method.");
                }
            }
        }
        Assert.assertTrue(_isElementVisible(numberOfGlass(glass)),
                "Configuration pop-up not displayed of activity.");
        _smartClick(numberOfGlass(glass));
        clickOnSaveActivity();
        _waitForPageLoad();
    }

    private void clickOnSaveActivity() {
        _waitForPageLoad();
        _sleep(5);
        List<WebElement> saveBtn = driver.findElements(By.xpath("//*[contains(text(),'Save')]"));
        saveBtn.get(saveBtn.size() - 1).click();
    }

    public void addActivityUseInhaler(String arg0, String time) throws Exception {
        WebElement element = driver.findElement(activity(arg0));
        _scrollToElement(element);
        int count = 0;
        for (WebElement element1 : activityTimeline) {
            count++;
            if (element1.getText().equalsIgnoreCase(time)) {
                WebElement to = driver.findElement(timelineSpace(count));
                scrollTillTimeline(to);

                try {
                    Actions builder = new Actions(driver);
                    Action dragAnddrop = builder.clickAndHold(element)
                            .moveToElement(to)
                            .release(to)
                            .build();
                    dragAnddrop.perform();
                    break;
                } catch (MoveTargetOutOfBoundsException e) {
                    element = driver.findElement(activity(arg0));
                    _scrollToElement(element);
                    count = 0;
                    for (WebElement elementIn1 : activityTimeline) {
                        count++;
                        if (elementIn1.getText().equalsIgnoreCase(time)) {
                            to = driver.findElement(timelineSpace(count));
                            scrollTillTimeline(to);

                            try {
                                Actions builder = new Actions(driver);
                                Action dragAnddrop = builder.clickAndHold(element)
                                        .moveToElement(to)
                                        .release(to)
                                        .build();
                                dragAnddrop.perform();
                                break;
                            } catch (MoveTargetOutOfBoundsException e1) {
                                throw new SkipException("Not able to drag and drop activity. Please check you drag and drop method.");
                            }
                        }
                    }
                    //throw new SkipException("Not able to drag and drop activity. Please check you drag and drop method.");
                }
            }
        }
//        Assert.assertTrue(_isElementVisible(numberOfGlass(glass)),
//                "Configuration pop-up not displayed of activity.");
//        _smartClick(numberOfGlass(glass));
        // _smartClick(saveActivity);
        clickOnSaveActivity();
        _waitForPageLoad();
    }

    private void scrollTillTimeline(WebElement to) {
        int heightOfElement = to.getLocation().getY();
        int totalLength = driver.manage().window().getSize().getHeight();
        if ((heightOfElement > (totalLength * 30) / 100))
            return;
    }

    private void _scrollDown(int count) {
        Actions actionProvider = new Actions(driver);
        while (count-- == 0) {
            try {
                Action keydown = actionProvider.sendKeys(Keys.ARROW_DOWN).build();
                keydown.perform();
            } catch (Exception e) {
            }
        }
    }

    public String verifyUpdateDrinkWaterActivity(String activity, String nextTimeline) {
        WebElement element = driver.findElement(activityInTimelineHandle(activity));
        int count = 0;
        for (WebElement element1 : activityTimeline) {
            count++;
            String time = element1.getText();
            if (time.equalsIgnoreCase(nextTimeline)) {
                WebElement to = driver.findElement(timelineSpace(count));
                scrollTillTimeline(to);
                try {
                    Actions builder = new Actions(driver);
                    Action dragAnddrop = builder.clickAndHold(element)
                            .moveToElement(to)
                            .release(to)
                            .build();
                    dragAnddrop.perform();
                    return time;
                } catch (Exception e) {
                    throw e;
                }
            }
        }
        return null;
    }

    public void verifyDeleteAnActivity(String arg0) throws Exception {
        _waitForPageLoad();
        _click(activityInTimeline(arg0));
        _click(deleteActivityIcon);
        _waitForPageLoad();
    }

    public boolean verifyActivityAdded(String arg0) {
        return _isElementVisible(activityInTimeline(arg0));
    }

    public boolean isActivityDeleted(String arg0) {
        return !_isElementVisible(activityInTimeline(arg0));
    }

    public boolean verifyAddCarePlanIsNotVisible() throws Exception {
        try {
            _click(addPlanIcon);
            return false;
        } catch (ElementClickInterceptedException e) {
            return true;
        }
    }
}
