package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Log4j
public class PatientDiaryPage extends CommonUtils {

    private WebDriver driver;

    public PatientDiaryPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private By symptomType(String title, String symptomName) {
        return By.xpath("//*[contains(text(),\"" + title + "\")]/../../../..//*[contains(@class,'profile-img-rounde')]//*[contains(@src,'" + symptomName.toLowerCase(Locale.ROOT) + "')]");
    }

    private By symptomImages(String title) {
        return By.xpath("//*[contains(text(),\"" + title + "\")]/../../../..//*[contains(@class,'symptom-image')]");
    }

    private By talkSoon(String title) {
        return By.xpath("//*[contains(text(),\"" + title + "\")]/../../../..//*[contains(text(),'Talk soon')]");
    }

    public void navigateToPatientDiary() throws Exception {
        _waitForPageLoad();
        _clickOnTextContains("Patient diary");
    }

    public boolean verifyDiaryNoteDetails(Map<String, String> diaryDetails, boolean isRequired) throws Exception {
        String expected = diaryDetails.get("Title");

        Assert.assertTrue(_isTextContainsPresent(expected),
                "Diary note title not visible. Expected : " + expected);

        expected = diaryDetails.get("Description");
        Assert.assertTrue(_isTextContainsPresent(expected),
                "Diary note description not visible. Expected : " + expected);

        if (isRequired) {
            Assert.assertTrue(_isElementVisible(symptomType(diaryDetails.get("Title"), "other")),
                    "Diary note symptom type should be [other].");
        } else {
            expected = diaryDetails.get("Type");
            Assert.assertTrue(_isElementVisible(symptomType(diaryDetails.get("Title"), expected)),
                    "Diary note symptom type should be [" + expected + "].");

            expected = diaryDetails.get("Images");
            Assert.assertEquals(_getElementSize(symptomImages(diaryDetails.get("Title"))), Integer.parseInt(expected),
                    "Diary note should have [" + expected + "] images attached.");
        }
        return true;
    }

    public void respondOnDiaryNote(String title) throws Exception {
        log.info("Click on talk soon.");
        _click(talkSoon(title));
    }

    public boolean verifyPatientDiaryEmptyScreen() {
        return _isTextContainsPresent("This patient has not added diary entry yet");
    }

    private By isNotAcknowledged = By.xpath("//*[@id='isNotAcknowledged']//..");

    public void selectUnacknowledgedToggle() throws Exception {
        _click(isNotAcknowledged);
        _waitForPageLoad();
    }

    public boolean isNoteDisplayed(String patientDiary) {
        return _isTextContainsPresent(patientDiary);
    }

    public void clickOnCalendarFilter() throws Exception {
        _clickOnTextContains("Select date");
        _sleep(2);
    }

    public void selectCalenderOption(String current_day) throws Exception {
        _clickOnTextContains(current_day);
        _waitForPageLoad();
    }

    public void clickOnFilter() throws Exception {
        _clickOnTextContains("Filter");
        _sleep(2);
    }

    public List<String> getFilterOptions() {
        List<WebElement> items = driver.findElements(By.xpath("//*[contains(@class,'list-item')]"));
        List<String> data = new ArrayList<>();
        for (WebElement ele : items)
            data.add(ele.getText());

        return data;
    }

    public boolean isFilterOptionSelected(String s) {
        return driver.findElement(By.xpath("//*[contains(text(),'" + s + "')]/..//input")).isSelected();
    }

    public void selectFilterOption(String icon, boolean isAll) {
        driver.findElement(By.xpath("//*[contains(text(),'" + icon + "')]/..//label")).click();
        if (!isAll)
            _waitForPageLoad();
    }

    public void clickOnApplyFilter() throws Exception {
        _clickOnTextContains("Apply filter");
        _waitForPageLoad();
    }

    public boolean isResetFilterDisplayed() {
        return _isTextContainsPresent("Reset filter");
    }

    public void clickOnResetFilter() throws Exception {
        _clickOnTextContains("Reset filter");
        _waitForPageLoad();
    }
}
