package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.SkipException;

import java.util.ArrayList;
import java.util.List;

@Log4j
public class MyPatientAllPatientPage extends CommonUtils {

    private WebDriver driver;

    public MyPatientAllPatientPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private By patientList = By.xpath("//*[contains(@class,'patient-details-container')]");
    @FindBy(id = "filterPatient")
    private WebElement searchPatient;
    @FindBy(xpath = "//*[contains(@class,'search-input')]//*[@type='ic-close']")
    private WebElement searchCrossIcon;
    @FindBy(xpath = "//*[@type='ic-overflow']//img")
    private WebElement userDetailsIcon;
    @FindBy(xpath = "//*[@class='qr-code-image']")
    private WebElement qrImage;
    @FindBy(xpath = "//*[contains(@class,'activate-patient')]//*[@type='ic-close']//img")
    private WebElement closeQR;
    @FindBy(id = "confirm")
    private WebElement remove;

    private By getVitalLastSyncValue(String vitalName) {
        return By.xpath("//*[*='" + vitalName + "']//ancestor::*[contains(@class,'vitals-head')]//*[contains(@class,'header')]");
    }

    private By getVitalLastSyncTime(String vitalName) {
        return By.xpath("//*[*='" + vitalName + "']//ancestor::*[contains(@class,'vitals-head')]//*[contains(@class,'sync-title')]");
    }

    public void clickOnRemove() throws Exception {
        _waitForElementVisible(remove);
        _click(remove);
        _waitForPageLoad();
       _sleep(10);
    }

    public boolean verifyPatientListDisplayed() {
        if (_isElementVisible(patientList))
            return true;
        else {
            return _isTextContainsPresent("No patients added yet!");
        }
    }

    public boolean verifySearchFieldDisplayed() {
        if (!_isTextContainsPresent("No patients added yet!"))
            return _isElementVisible(searchPatient);
        else
            throw new SkipException("Not a valid test case until no patient added.");
    }

    public boolean verifySearchFieldClickable() throws Exception {
        try {
            _click(searchPatient);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifySearchFieldAcceptInput() throws Exception {
        try {
            _sendKeys(searchPatient, "Search here");
            return _getAttributeValue(searchPatient, "value").equalsIgnoreCase("Search here");
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyClickOnClearIcon() {
        return _isElementVisible(searchCrossIcon);
    }

    public boolean verifySearchFieldClear() throws Exception {
        _click(searchCrossIcon);
        return !_getAttributeValue(searchPatient, "value").equalsIgnoreCase("Search here");
    }

    public void searchInput(String s) throws Exception {
        _sendKeys(searchPatient, s);
    }

    public boolean verifyNoExactResultFind() {
        return _isTextContainsPresent("There's no exact match for your search");
    }

    public void clickOnThreeDots() throws Exception {
        _isElementVisible(userDetailsIcon);
        _click(userDetailsIcon);
    }

    public void clickOnShowQR() throws Exception {
        _clickOnTextContains("View QR code");
    }

    public boolean QRCodeDisplayed() {
        _waitForPageLoad();
        return _isElementVisible(qrImage);
    }

    public boolean closeQRContainer() throws Exception {
        _click(closeQR);
        return _waitForElementInvisible(qrImage);
    }

    public void clickPatientMRN(String arg0) throws Exception {
        _click(driver.findElement(By.xpath("//*[contains(@class,'patient-details')]//*[contains(text(),'" + arg0 + "')]")));
    }

    public String getLastSyncValue(String vital) throws Exception {
        _waitForPageLoad();
        _scrollToElement(driver.findElement(getVitalLastSyncValue(vital)));
        return _getText(getVitalLastSyncValue(vital));
    }

    public String getLastSyncTime(String vital) throws Exception {
        _scrollToElement(driver.findElement(getVitalLastSyncTime(vital)));
        return _getText(getVitalLastSyncTime(vital));
    }

    public List<String> getValues() {
        List<WebElement> elements = driver.findElements(By.xpath("//*[contains(@class,'details-section')]"));
        List<String> values = new ArrayList<>();
        for (WebElement element : elements)
            values.add(element.getText().replace("\n", "").strip());

        return values;
    }

    public boolean isMessageDisplayed(String no_clinician_has_been_assigned) {
        return _isTextContainsPresent(no_clinician_has_been_assigned);
    }

    public void refreshExpiredQR() throws Exception {
        if (_isTextContainsPresent(" Generate a new code ")) {
            _clickOnTextContains("Generate a new code");
            _waitForPageLoad();
            _waitForPageLoad();
        }
    }
}
