package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

@Log4j
public class ThereseHoldPage extends CommonUtils {

    private WebDriver driver;

    public ThereseHoldPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "confirm")
    private WebElement remove;
    @FindBy(xpath = "//*[@type=\"threshold\"]")
    private WebElement thresholdIcon;
    @FindBy(xpath = "//*[@type=\"ic-close\"]")
    private WebElement close;
    @FindBy(xpath = "//*[contains(@class,'reset-btn')]")
    private WebElement reset;
    @FindBy(xpath = "//*[@formcontrolname=\"highHrLimit\" and @type='number']")
    private WebElement hrNumber;
    @FindBy(xpath = "//*[@formcontrolname=\"highDayHrLimit\" and @type='number']")
    private WebElement highDayHrNum;
    @FindBy(xpath = "//*[@type='spinning-circles']")
    private WebElement spinnerLoader;
    @FindBy(xpath = "//*[@type='ic-warning-filled']//img")
    private WebElement alertIconOnPatientPage;
    @FindBy(xpath = "//*[@type='ic-warning-filled']/..//*[contains(@class,'notification-badge')]")
    private WebElement individualBadgeCount;

    public boolean openThresholdContainer() throws Exception {
        _waitForLoading();
        _waitForPageLoad();
        _waitForElementVisible(thresholdIcon);
        _click(thresholdIcon);
        return _isTextContainsPresent("Thresholds");
    }

    public boolean closeThresholdContainer() throws Exception {
        _waitForPageLoad();
        _waitForElementVisible(close);
        _click(close);
        return !_isTextContainsPresent("Thresholds");
    }

    public void clickOnReset() throws Exception {
        if (!_waitForElementVisible(reset))
            Assert.fail("Reset button not visible.");
        _click(reset);
        _waitForLoading();
    }


    public String getHRDefaultValue() {
        if (_waitForElementVisible(highDayHrNum))
            return _getAttributeValue(highDayHrNum, "value");
        else
            return _getAttributeValue(hrNumber, "value");
    }

    public void setHrValue(String newValue) throws Exception {
        if (_waitForElementVisible(highDayHrNum))
            _sendKeys(highDayHrNum, newValue);
        else
            _sendKeys(hrNumber, newValue);

        _waitForLoading();
        _waitForPageLoad();
        _waitForElementInvisible(spinnerLoader);
    }

    public boolean verifyResetPopUp(String value) {
        return _isTextContainsPresent(value, true);
    }

    public void actionOnResetPop(String value) throws Exception {
        _clickOnTextContains(value);
    }

    public boolean verifyContainerTexts(String s) {
        return _isTextContainsPresent(s);
    }

    public boolean isCloseBtnDisplayed() {
        return _isElementVisible(close);
    }

    public void clickOnCloseContainer() throws Exception {
        _click(close);
    }

    public boolean isThereseholdContainerClose() {
        _waitForElementInvisible(close);
        return !_isElementVisible(close);
    }

    public boolean isAlertIconDisplayed() {
        return _isElementVisible(alertIconOnPatientPage);
    }

    public void clickOnAlertIcon() throws Exception {
        _click(alertIconOnPatientPage);
    }

    @FindBy(xpath = "//*[contains(@class,'alerts-dd-menu')]//*[contains(text(),'Active')]")
    private WebElement alertDDMenu;

    public boolean isAlertPopUPDisplayed() {
        return _isElementVisible(alertDDMenu);
    }

    private By alertPop(String s) {
        return By.xpath("//*[contains(@class,'alerts-dd-menu')]//*[contains(text(),'" + s + "')]");
    }

    public boolean isOptionDisplayedOnAlertContainer(String active) {
        return _isElementVisible(alertPop(active));
    }

    public void clickOnRespondedAlert() throws Exception {
        _click(alertPop("Responded"));
    }

    public void clickOnActiveAlert() throws Exception {
        _click(alertPop("Active"));
    }

    private By getVitalsValue(String name, boolean isHigh) {
        if (isHigh)
            return By.xpath("//*[contains(text(),'High " + name + "')]/..//input[@type='number']");
        else
            return By.xpath("//*[contains(text(),'Low " + name + "')]/..//input[@type='number']");
    }

    public String getAllDefaultVitals(String vitalName, boolean isHigh) {

        if (vitalName.contains("Body"))
            if (isHigh)
                return _getAttributeValue(driver.findElement(By.xpath("//*[contains(@formcontrolname,'Body') and contains(@formcontrolname,'high') and @type='number']")),
                        "value");
            else
                return _getAttributeValue(driver.findElement(By.xpath("//*[contains(@formcontrolname,'Body') and contains(@formcontrolname,'low') and @type='number']")),
                        "value");

        if (vitalName.contains("Temporal"))
            if (isHigh)
                return _getAttributeValue(driver.findElement(By.xpath("//*[contains(@formcontrolname,'Temporal') and contains(@formcontrolname,'high') and @type='number']")),
                        "value");
            else
                return _getAttributeValue(driver.findElement(By.xpath("//*[contains(@formcontrolname,'Temporal') and contains(@formcontrolname,'low') and @type='number']")),
                        "value");

        return _getAttributeValue(driver.findElement(getVitalsValue(vitalName, isHigh)), "value");
    }

    public boolean isIndividualBadgeDisplayed() {
        return _isElementVisible(individualBadgeCount);
    }

    public void waitForAlertGeneration(int syncDuration) {
        int count = 0;
        while (!_isElementVisible(individualBadgeCount)) {
            if (count == 6)
                return;
            if (syncDuration == 30) {
                _sleep(60);
                count++;
            }
        }
    }

    public boolean isAlertMessageDisplayed(String s) {
        return _isElementVisible(By.xpath("//*[contains(@class,'common-dropdown alerts-dd-menu')]//*[contains(@class,'alert-card critical')][1]//*[contains(text(),'" + s + "')]"));
    }

    public void clickOnRespond() throws Exception {
        _click(By.xpath("//*[contains(@class,'common-dropdown alerts-dd-menu')]//*[contains(@class,'alert-card critical')][1]//*[contains(text(),'Respond')]"));
    }

    public boolean isRespondBtnVisible() {
        return _isElementVisible(By.xpath("//*[contains(@class,'common-dropdown alerts-dd-menu')]//*[contains(@class,'alert-card critical')][1]//*[contains(text(),'Respond')]"));
    }

    public void clickOnResolveAnAlert() throws Exception {
        _waitForLoading();
        _click(By.xpath("//*[contains(@class,'conti')]//*[contains(text(),' Resolve ')]"));
    }

    public void clickOnSubmitAnAlertResponse() throws Exception {
        _click(By.xpath("//*[contains(@class,'conti')]//*[contains(text(),'Submit')]"));
        _waitForLoading();
        _waitForPageLoad();
    }

    public boolean verifyResolvedAlertMessage(String message) {
        return _isElementVisible(By.xpath("//*[contains(text(),'Resolved')]//ancestor::*[contains(@class,'alert-card critical')]//*[contains(text(),'" + message + "')]"));
    }

    public String getGlobalAlertBadgeCount() throws Exception {
        try {
            return _getText(By.xpath("//*[contains(@class,'main-header')]//*[contains(@class,'notification-badge')]"));
        } catch (Exception e) {
            return "0";
        }
    }

    public void clickOnGlobalAlertIcon() throws Exception {
        _click(By.xpath("//*[contains(@class,'main-header')]//*[@type=\"ic-alert-icon\"]"));
    }

    public void clickOnGlobalRespondAnAlert() throws Exception {
        _click(By.xpath("//*[contains(@class,'main-header')]//*[contains(@class,'alert-card critical')][1]//*[contains(text(),'Respond')]"));
    }

    public boolean isGlobalAlertMessageDisplayed(String hrr) {
        return _isElementVisible(By.xpath("//*[contains(@class,'main-header')]//*[contains(@class,'alert-card critical')][1]//*[contains(text(),'" + hrr + "')]"));
    }

}
