package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Map;

public class InventoryPage extends CommonUtils {

    private WebDriver driver;

    public InventoryPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//label[contains(text(),\"Add device\")]")
    private WebElement addDeviceBtn;
    @FindBy(xpath = "//*[@formcontrolname=\"deviceType\"]")
    private WebElement deviceType;
    @FindBy(xpath = "//*[@formcontrolname=\"assetId\"]")
    private WebElement assetId;
    @FindBy(xpath = "//*[@formcontrolname=\"serialNumber\"]")
    private WebElement serialNumber;
    @FindBy(xpath = "//*[@formcontrolname=\"manufacturer\"]")
    private WebElement manufacturer;
    @FindBy(xpath = "//*[@formcontrolname=\"lastServiceAt\"]")
    private WebElement lastServiceAt;
    @FindBy(xpath = "//*[contains(@class,'inventory-item')]//td[2]")
    private List<WebElement> deviceNameInList;
    @FindBy(xpath = "//*[@role=\"radiogroup\"]//*[text()='Care team']")
    private WebElement careTeam;
    @FindBy(id = "confirm")
    private WebElement confirmDlt;
    @FindBy(xpath = "//*[contains(@class,'inventory-table')]//*[contains(@class,'grid')]")
    private WebElement deviceGrid;
    @FindBy(xpath = "//*[@placeholder=\"Search for care team member\"]")
    private WebElement searchClinician;

    private By assignIcon(String assetId) {
        return By.xpath("//*[contains(@class,'inventory-item')]//*[contains(text(),'" + assetId + "')]/..//*[@type=\"assign\"]");
    }

    private By editIcon(String assetId) {
        return By.xpath("//*[contains(@class,'inventory-item')]//*[contains(text(),'" + assetId + "')]/..//*[@type=\"ic-overflow1\"]");
    }

    private By delete(String assetId) {
        return By.xpath("//*[contains(@class,'inventory-item')]//*[contains(text(),'" + assetId + "')]/..//*[contains(text(),'Delete')]");
    }

    private By deviceSection(String name) {
        return By.xpath("//*[contains(@class,\"device-card\")]//*[contains(text(),'" + name + "')]");
    }

    private By selectDevice(String name) {
        return By.xpath("//*[contains(text(),'" + name + "') and contains(@class,'option')]");
    }

    public boolean clickOnAddDevice() throws Exception {
        _pageWait(addDeviceBtn);
        _click(addDeviceBtn);
        return _waitForElementVisible(deviceType);
    }

    public void fillRequiredFields(Map<String, String> reqDetails) throws Exception {
        _click(deviceType);
        _click(selectDevice(reqDetails.get("deviceName")));
        _sendKeys(assetId, reqDetails.get("assetId"));
        _sendKeys(serialNumber, reqDetails.get("serialNumber"));
        _sendKeys(manufacturer, reqDetails.get("manufacturer"));
    }

    public void clickOnSave() throws Exception {
        _clickOnTextContains("Save");
        _waitForPageLoad();
        _waitForLoading();
    }

    public boolean verifyDeviceAdded(String assetId) throws Exception {
        _waitForPageLoad();
        _waitForLoading();
        for (WebElement element : deviceNameInList)
            if (element.getText().strip().equalsIgnoreCase(assetId))
                return true;
        return false;
    }

    public void selectDeviceSection(String deviceName) throws Exception {
        _click(deviceSection(deviceName));
        _waitForPageLoad();
    }


    public void clickOnAssignIcon(String id) throws Exception {
        _waitForElementVisible(assignIcon(id));
        _click(assignIcon(id));
    }

    public void selectCareTeamOptionForAssign() throws Exception {
        _waitForElementVisible(careTeam);
        _click(careTeam);
        _waitForPageLoad();
    }

    public void clickOnAssign() throws Exception {
        _clickOnText("Assign");
        _waitForPageLoad();
        _waitForPageLoad();
    }

    public void selectCareTeam(String clinicianName) throws Exception {
        _waitForPageLoad();
        _click(careTeam(clinicianName));
    }

    private By careTeam(String clinicianName) {
        return By.xpath("//*[contains(@class,'user-card')]//*[contains(text(),'" + clinicianName + "')]");
    }

    public void moveToAssignedTab() throws Exception {
        _waitForPageLoad();
        _clickOnTextContains("Assigned");
    }

    public void deleteDevice(String assetId) throws Exception {
        _click(editIcon(assetId));
        _click(delete(assetId));
        _waitForElementVisible(confirmDlt);
        _click(confirmDlt);
        _waitForPageLoad();
        _waitForLoading();
    }

    public void moveToAvailableTab() throws Exception {
        _waitForPageLoad();
        _clickOnTextContains("Available");
        _waitForPageLoad();
        _waitForLoading();
    }

    public boolean verifyDeviceDeleted(String assetId) throws Exception {
        if (_getElementSize(deviceGrid) == 1)
            return true;
        else
            for (WebElement element : deviceNameInList)
                if (element.getText().strip().equalsIgnoreCase(assetId))
                    return false;
        return true;
    }


    public boolean searchClinician(String user) throws Exception {
        _sendKeys(searchClinician, user);
        return _isElementVisible(careTeam(user));
    }
}
