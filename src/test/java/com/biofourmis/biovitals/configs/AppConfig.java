package com.biofourmis.biovitals.configs;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.codoid.products.fillo.Fillo;
import com.github.javafaker.Faker;
import lombok.Data;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import java.util.*;

/**
 * This class is read only do not modify anything here
 */
@Data
@Configuration
@ComponentScan(basePackages = "com.biofourmis.biovitals")
@PropertySources({
        @PropertySource("file:./src/test/resources/properties/application.properties"),
        @PropertySource("file:./src/test/resources/properties/custom.properties")
})
public class AppConfig implements ApplicationContextAware {

    @Autowired
    private Environment environment;

    @Autowired
    private CustomConfig customConfig;

    @Value("${app.selenium.timeout.seconds}")
    private long seleniumTimeout;

    @Value("${test.data.fileName}")
    private String testDataFile;

    @Value(("${appium.server.startAutomatic}"))
    private boolean isAppiumAutoStart;

    @Value(("${appium.server.port}"))
    private int appiumPort;

    @Value(("${appium.server.ip}"))
    private String appiumIP;

    @Value(("${appium.server.enableLogs}"))
    private boolean appiumServerLogsEnable;

    @Value("${app.selenium.supported.browser}")
    private String[] browsers;

    private String appActivity;

    private String appPackage;

    private static ApplicationContext context;

    @Autowired(required = false)
    private TestConfig testConfig;

    public void setAppCapabilities(String appName) {
        String platform = getAppPlatform(appName);
        String appActivity = environment.getProperty("app." + platform.toLowerCase(Locale.ROOT) + "." + appName.toLowerCase(Locale.ROOT) + ".activity");
        String appPackage = environment.getProperty("app." + platform.toLowerCase(Locale.ROOT) + "." + appName.toLowerCase(Locale.ROOT) + ".package");

        if (platform.equalsIgnoreCase("android") && appActivity == null)
            throw new RuntimeException("App activity is not specified, please include "
                    + "app." + platform.toLowerCase(Locale.ROOT) + "." + appName.toLowerCase(Locale.ROOT) + ".activity property in the custom.properties");

        if (appPackage == null)
            throw new RuntimeException("App package is not specified, please include "
                    + "app." + platform.toLowerCase(Locale.ROOT) + "." + appName.toLowerCase(Locale.ROOT) + ".package property in the custom.properties");

        setAppPackage(appPackage);
        setAppActivity(appActivity);
    }

    public String getPlatformName() {
        String platformName = testConfig.getBrowser();
        if (platformName != null && new ArrayList<>(Arrays.asList(browsers)).contains(platformName))
            return platformName;
        else
            throw new RuntimeException("Incorrect or no platformName specified in TestNG XML");
    }

    public String getCurrentWorkingDir() {
        return System.getProperty("user.dir");
    }

    public String getAppPlatform(String appName) {
        String appPlatform = getPlatform(appName);
        if (appPlatform != null && new ArrayList<>(Arrays.asList("ios", "android", "tab", "ipad"))
                .contains(appPlatform)) {
            return appPlatform;
        } else
            throw new RuntimeException("Incorrect or no platform specified for " + appName + " in TestNG XML");
    }

    private String getPlatform(String appName) {
        if (appName.equalsIgnoreCase("HH"))
            return testConfig.getPatientAppPlatform().toLowerCase(Locale.ROOT);
        else if (appName.equalsIgnoreCase("HCP"))
            return testConfig.getHcpAppPlatform().toLowerCase(Locale.ROOT);
        else
            throw new RuntimeException("App not added into properties file. App Name :" + appName);
    }

    @Bean
    public Faker faker() {
        return new Faker();
    }

    @Bean
    public Fillo fillo() {
        return new Fillo();
    }

    public String getCurrentOS() {
        return System.getProperty("os.name");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        AppConfig.context = applicationContext;
    }

    public static <T extends Object> T getBean(Class<T> beanClass) {
        return context.getBean(beanClass);
    }

}
