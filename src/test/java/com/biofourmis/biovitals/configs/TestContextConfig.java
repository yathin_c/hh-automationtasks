package com.biofourmis.biovitals.configs;

import org.openqa.selenium.WebDriver;

import java.util.*;

public class TestContextConfig {

    private Set<WebDriver> scenarioDrivers;

    public TestContextConfig() {
        this.scenarioDrivers = new HashSet<>();
    }

    public void setScenarioDriver(WebDriver driver) {
        this.scenarioDrivers.add(driver);
    }

    public Set<WebDriver> getScenarioDrivers() {
        return this.scenarioDrivers;
    }
}
