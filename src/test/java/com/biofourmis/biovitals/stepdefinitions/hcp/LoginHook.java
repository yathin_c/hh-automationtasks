package com.biofourmis.biovitals.stepdefinitions.hcp;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.LoginPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@Log4j
public class LoginHook {

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;

    @Autowired
    private TestConfig testConfig;
    @Autowired
    private Faker faker;

    private CustomUtils customUtils = new CustomUtils(null);
    private LoginPage loginPage;
    private boolean isInvalid = false;

    @Then("Verify Login button is disable without filling email and password")
    public void verifyLoginButtonIsDisableWithoutFillingEmailPassword() throws Exception {
        loginPage = new LoginPage(clinician);

        log.info("Verify Login button is disable.");
        Assert.assertFalse(loginPage.verifyLoginBtnIsDisable(),
                "Login button should be disable.");
    }

    @And("^Verify Login button is disable after filling (email|password) only$")
    public void verifyLoginButtonIsDisableAfterFillingEmailOnly(String arg0) throws Exception {
        boolean flag = arg0.equals("email");

        log.info("Clear Username field.");
        loginPage.clearMail();
        log.info("Clear password field.");
        loginPage.clearPassword();

        log.info("Enter " + arg0);
        if (flag)
            loginPage.enterEmail(testConfig.getClinicianMail());
        else
            loginPage.enterPassword(testConfig.getClinicianPass());

        log.info("Verify login button should be disable until both fields are not filled.");
        Assert.assertFalse(loginPage.verifyLoginBtnIsDisable(),
                "Login button should be disable.");
    }

    @Then("Verify Login button is enable after filling email and password")
    public void verifyLoginButtonIsEnableAfterFillingEmailAndPassword() throws Exception {
        log.info("Clear Username field.");
        loginPage.clearMail();
        log.info("Clear password field.");
        loginPage.clearPassword();

        String mail, password;
        if (isInvalid) {
            mail = "care@123.com";
            password = "Care@1234--";
        } else {
            mail = testConfig.getClinicianMail();
            password = testConfig.getClinicianPass();
        }

        log.info("Enter mail. [" + mail + "]");
        loginPage.enterEmail(mail);
        log.info("Enter password. [" + password + ";");
        loginPage.enterPassword(password);

        log.info("Verify login button should be enable after filling email and password.");
        Assert.assertTrue(loginPage.verifyLoginBtnIsDisable(),
                "Login button should be enable.");
    }

    @Then("^Enter (valid|invalid) email and password$")
    public void enterEmailAndPassword(String arg0) throws Exception {
        loginPage = new LoginPage(clinician);
        isInvalid = arg0.equals("invalid");

        log.info("Clear Username field.");
        loginPage.clearMail();
        log.info("Clear password field.");
        loginPage.clearPassword();

        String mail, password;
        if (isInvalid) {
            mail = "care@123.com";
            password = "Care@1234--";
        } else {
            mail = testConfig.getClinicianMail();
            password = testConfig.getClinicianPass();
        }

        log.info("Enter mail. [" + mail + "]");
        loginPage.enterEmail(mail);
        log.info("Enter password. [" + password + ";");
        loginPage.enterPassword(password);
    }

    @And("Verify login successfully")
    public void verifyLoginSuccessfully() throws Exception {
        loginPage.clickOnLoginBtn();
        log.info("Verify login successfully in HCP App.");
        Assert.assertTrue(loginPage.verifyLogin(),
                "Failed while login into app.");
    }

    @And("Verify invalid user password message")
    public void verifyInvalidUserPasswordMessage() throws Exception {
        for (int i = 4; i >= 2; i--) {
            log.info("Click on login button.");
            loginPage.clickOnLoginBtn();
            String message = "You have " + i + " more password attempts. Once the limit is reached, your account will be locked for 5 minutes";
//            Assert.assertTrue(loginPage.verifyErrorMessage(message),
//                    message + " : not displayed.");
            Assert.assertTrue(loginPage.verifyErrorMessage("Invalid email or password. Please try again."),
                    "Error message not matched/found.");
        }
    }

    @Then("Login into HCP app")
    public void loginIntoHCPApp() throws Exception {
        enterEmailAndPassword("valid");
        verifyLoginSuccessfully();
    }

    @Then("verify HCP app logout successfully")
    public void verifyHCPAppLogoutSuccessfully() throws Exception {
        loginPage = new LoginPage(clinician);

        log.info("Click on Settings tab.");
        loginPage.navigateToSettingsTab();

        log.info("Verify Setting tab displayed.");
        Assert.assertTrue(loginPage.isSettingsTabDisplayed(),
                "Not able to navigate to settings tab after click on settings.");

        log.info("Click on Log out option.");
        loginPage.clickOn("Log out");

        Assert.assertTrue(loginPage.isLogOutOptionDisplayed(),
                "Log out pop-up not displayed.");

        Assert.assertTrue(loginPage.isOptionDisplayed("Are you sure you want to log out?"),
                "[Are you sure you want to log out?] pop-up content not displayed.");

        Assert.assertTrue(loginPage.isOptionDisplayed("NO"),
                "[NO] button not displayed.");

        Assert.assertTrue(loginPage.isOptionDisplayed("YES"),
                "[YES] button not displayed.");

        log.info("CLick on NO button.");
        loginPage.clickOn("NO");

        Assert.assertFalse(loginPage.isOptionDisplayed("Are you sure you want to log out?"),
                "[Are you sure you want to log out?] pop-up content should not be" +
                        " displayed after click on [NO] button.");

        log.info("Verify Setting tab displayed.");
        Assert.assertTrue(loginPage.isSettingsTabDisplayed(),
                "Not able to navigate to settings tab after click on settings.");

        log.info("Click on [Log out] option.");
        loginPage.clickOn("Log out");

        Assert.assertTrue(loginPage.isLogOutOptionDisplayed(),
                "Log out pop-up not displayed.");

        Assert.assertTrue(loginPage.isOptionDisplayed("Are you sure you want to log out?"),
                "[Are you sure you want to log out?] pop-up content not displayed.");

        Assert.assertTrue(loginPage.isOptionDisplayed("NO"),
                "[NO] button not displayed.");

        Assert.assertTrue(loginPage.isOptionDisplayed("YES"),
                "[YES] button not displayed.");

        log.info("Click on [YES] button.");
        loginPage.clickOn("YES");

        Assert.assertTrue(loginPage.isLoginScreenDisplayed(),
                "Login screen should be displayed after logout.");
    }

    @Then("verify user should be logged in both browser and app")
    public void verifyUserShouldBeLoggedInBothBrowserAndApp() {

        log.info("Verify login successfully in HCP App.");
        Assert.assertTrue(loginPage.verifyLogin(),
                "User logged out form HCP app after login into web.");
        com.biofourmis.biovitals.pagefactory.web.LoginPage loginWeb = new com.biofourmis.biovitals.pagefactory.web.LoginPage(driver);

        Assert.assertTrue(loginWeb.verifyHomePage(), "User logged out from web dashboard.");
    }

    @And("logout HCP user")
    public void logoutHCPUser() throws Exception {
        loginPage = new LoginPage(clinician);

        log.info("Click on Settings tab.");
        loginPage.navigateToSettingsTab();

        log.info("Verify Setting tab displayed.");
        Assert.assertTrue(loginPage.isSettingsTabDisplayed(),
                "Not able to navigate to settings tab after click on settings.");

        log.info("Click on Log out option.");
        loginPage.clickOn("Log out");

        log.info("Click on [YES] button.");
        loginPage.clickOn("YES");

        Assert.assertTrue(loginPage.isLoginScreenDisplayed(),
                "Login screen should be displayed after logout.");
    }
}
