package com.biofourmis.biovitals.stepdefinitions.hcp;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.HandOffPage;
import com.biofourmis.biovitals.pagefactory.hcp.PatientsPage;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;

@Log4j
public class HandOffHook {

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    @Autowired(required = false)
    private TestConfig config;
    @Autowired
    private Faker faker;

    private CustomUtils customUtils = new CustomUtils(null);
    private HandOffPage handOffPage;
    private Map<String, String> details = new LinkedHashMap<>();
    private Map<String, String> previousData = new HashMap<>();

    private void setHandOffDetails(String arg0) {
        if (details.isEmpty()) {
            details.put("severity", arg0);
            details.put("summary", faker.gameOfThrones().quote() + LocalDateTime.now());
            details.put("awareness", Arrays.asList("AOx3", "Ambulatory", "Altered", "Nonverbal at baseline").get(faker.random().nextInt(0, 3)));
            details.put("synthesis", faker.aquaTeenHungerForce().character() + LocalDateTime.now());
            details.put("customAwareness", faker.dune().character() + LocalDateTime.now());
            customUtils.setHandOffDetails(details);
        }
    }

    private void updateHandOffDetails(String arg0) {
        previousData.putAll(customUtils.getHandOffDetails());
        details.put("severity", arg0);
        details.put("summary", faker.backToTheFuture().character() + LocalDateTime.now());
        List<String> data = new ArrayList<>(Arrays.asList("AOx3", "Ambulatory", "Altered", "Nonverbal at baseline"));
        data.remove(customUtils.getHandOffDetails().get("awareness"));
        String temp[] = customUtils.getHandOffDetails().get("awareness").split(",");
        for (String awa : temp)
            data.remove(awa.strip());
        details.put("awareness", "," + customUtils.getHandOffDetails().get("awareness") + "," + data.get(faker.random().nextInt(0, 1)));
        details.put("synthesis", faker.lordOfTheRings().character() + LocalDateTime.now());
        details.put("customAwareness", faker.aviation().aircraft() + LocalDateTime.now());
        customUtils.setPreviousHandOffDetails(previousData);
        customUtils.setHandOffDetails(details);
    }

    @Then("verify Handoff patient screen displayed")
    public void verifyHandoffPatientScreenDisplayed() {
        handOffPage = new HandOffPage(clinician);

        log.info("Verify Handoff page displayed.");
        Assert.assertTrue(handOffPage.isHandoffPageDisplayed(),
                "Handoff page not displayed.");

        String patient = customUtils.getPatientDetails(config).get("Name");
        String mrn = customUtils.getPatientDetails(config).get("MRN");

        log.info("Verify Patient Name should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed(patient), "Patient name as " + patient + "not displayed.");

        log.info("Verify Patient MRN/Reference ID should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed(mrn), "Patient MRN/Reference ID as " + mrn + "not displayed.");

        log.info("Verify [Illness severity] header should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Illness severity"), "[Illness severity] header not displayed.");

        log.info("Verify [Stable] severity should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Stable"), "[Stable] severity not displayed.");

        log.info("Verify [“Watcher”] severity should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("“Watcher”"), "[“Watcher”] severity not displayed.");

        log.info("Verify [Unstable] severity should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Unstable"), "[Unstable] severity not displayed.");

        log.info("Verify [Patient summary] header should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Patient summary"), "[Patient summary] header not displayed.");

        log.info("Verify [Situational awareness and contingency planning] header should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Situational awareness and contingency planning"), "[Situational awareness and contingency planning] header not displayed.");

        log.info("Verify [AOx3] option should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("AOx3"), "[AOx3] option not displayed.");

        log.info("Verify [Ambulatory] option should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Ambulatory"), "[Ambulatory] option not displayed.");

        log.info("Verify [Altered] option should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Altered"), "[Altered] option not displayed.");

        log.info("Verify [Nonverbal at baseline] option should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Nonverbal at baseline"), "[Nonverbal at baseline] option not displayed.");

        handOffPage.swipeUp();

        log.info("Verify [Customise your own option] option should be displayed.");
        Assert.assertTrue(handOffPage.isCustomiseOption(), "[Customise your own option] option not displayed.");

        log.info("Verify [Synthesis] header should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Synthesis"), "[Synthesis] header not displayed.");

        log.info("Verify [Hand off] header should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Hand off"), "[Hand off] header not displayed.");

        log.info("[Hand off] button should be displayed");
        Assert.assertTrue(handOffPage.isHandOffBtnDisplayed(), "Hand off button not displayed.");

    }

    @Then("verify cancelling hand off after filling all details")
    public void verifyCancellingHandOffAfterFillingAllDetails() throws Exception {
        handOffPage = new HandOffPage(clinician);
        String summary = faker.aquaTeenHungerForce().character();
        String synthesis = faker.lordOfTheRings().character();

        log.info("Hand off button should be disable until all fields are not filled.");
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(), "Hand off button should be disable until all fields are not filled");

        log.info("Enter Illness severity as Stable.");
        handOffPage.selectSeverity("Stable");

        log.info("Hand off button should be disable until all fields are not filled.");
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(), "Hand off button should be disable until all fields are not filled");

        log.info("Enter patient summary as [" + summary + "]");
        handOffPage.enterSummary(summary);

        log.info("Hand off button should be disable until all fields are not filled.");
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(), "Hand off button should be disable until all fields are not filled");

        log.info("Select awareness as [AOx3]");
        handOffPage.selectSituationalOptions("AOx3", true);

        log.info("Hand off button should be disable until all fields are not filled.");
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(), "Hand off button should be disable until all fields are not filled");

        handOffPage.swipeUp();

        log.info("Enter Synthesis as [" + synthesis + "]");
        handOffPage.enetrSynthesis(synthesis);

        log.info("Hand off button should be disable until all fields are not filled.");
        Assert.assertTrue(handOffPage.ishandOffBtnEnable(), "Hand off button should be enable after all fields are filled");

        log.info("Click on Hand off button.");
        handOffPage.clickOnHandOffbtn();

        log.info("Wait for Proceed pop up");
        Assert.assertTrue(handOffPage.waitForProceedPop(), "Hand off patient proceed pop up not displayed.");

        log.info("Verify [Please make sure all details are accurate.] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Please make sure all details are accurate."), "[Please make sure all details are accurate.] not displayed.");

        log.info("Verify [Hand off patient?] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Hand off patient?"), "[Hand off patient?] not displayed.");

        log.info("Verify [Cancel] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Cancel"), "[Cancel] not displayed.");

        log.info("Verify [Proceed] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Proceed"), "[Proceed] not displayed.");

        log.info("Click on cancel option.");
        handOffPage.clickOnCancelOption();

        log.info("Verify Handoff page displayed.");
        Assert.assertTrue(handOffPage.isHandoffPageDisplayed(),
                "Handoff page not displayed after clicking on cancel option.");
    }

    @Then("^verify hand off after (filling|updating) all details$")
    public void verifyHandOffAfterFillingAllDetails(String arg0) throws Exception {
        handOffPage = new HandOffPage(clinician);
        PatientsPage patientsPage = new PatientsPage(clinician);
        String patient = customUtils.getPatientDetails(config).get("Name");

        if (arg0.equalsIgnoreCase("filling")) {
            log.info("Create Hand off details.");
            setHandOffDetails("Stable");
        } else {
            log.info("Update Hand off details.");
            updateHandOffDetails("Unstable");
        }

        log.info("Hand off button should be disable until all fields are not filled.");
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(), "Hand off button should be disable until all fields are not filled");

        log.info("Enter Illness severity as Stable.");
        handOffPage.selectSeverity(details.get("severity"));

        log.info("Enter patient summary as [" + details.get("summary") + "]");
        handOffPage.enterSummary(details.get("summary"));

        log.info("Select awareness as " + details.get("awareness"));
        handOffPage.selectSituationalOptions(details.get("awareness"), true);

        handOffPage.swipeUp();

        log.info("Enter Synthesis as [" + details.get("synthesis") + "]");
        handOffPage.enetrSynthesis(details.get("synthesis"));

        log.info("Click on Hand off button.");
        handOffPage.clickOnHandOffbtn();

        log.info("Wait for Proceed pop up");
        Assert.assertTrue(handOffPage.waitForProceedPop(),
                "Hand off patient proceed pop up not displayed.");

        log.info("Click on proceed option.");
        handOffPage.clickOnProceedOption();

        log.info("Collect submission date time.");
        LocalDateTime local = LocalDateTime.now();

        log.info("Verify Handoff successfully toast message.");
        Assert.assertTrue(handOffPage.verifyHandOffSuccessToast(patient),
                patient + " is handed off successfully");

        log.info("Open menu option for searched patient.");
        patientsPage.openPatientMenu();

        log.info("Click on option [Handoff patient].");
        patientsPage.clickOnMenu("Handoff patient");

        log.info("Verify handoff summary to make sure previous handoff done successfully.");
        Assert.assertTrue(handOffPage.verifyhandOffSummary(details.get("summary")),
                "Hand off summary not matched. Expected [" + details.get("summary") + "].");

        //Contact Physician Automation for Questions
        local.getMonth().getDisplayName(TextStyle.SHORT, Locale.getDefault());
        DateTimeFormatter df = DateTimeFormatter.ofPattern("hh:mm a");
        String time = local.format(df);
        if (time.startsWith("0"))
            time = time.substring(1, time.length());

        String lastHandoff = "Last updated: " + local.getMonth().getDisplayName(TextStyle.SHORT, Locale.getDefault())
                + " " + local.getDayOfMonth() + " " + time;
        handOffPage.swipeUp();
        log.info("Verify last updated time.");
        Assert.assertTrue(handOffPage.lastUpdatedTime(lastHandoff), "Last handoff time should be [" + lastHandoff + "].");

        //TODO
    }

    @Then("^verify (Cancel|Proceed) hand off when No changes from prior visit option is selected$")
    public void verifyHandOffWhenNoChangesFromPriorVisitOptionIsSelected(String arg0) throws Exception {
        handOffPage = new HandOffPage(clinician);

        PatientsPage patientsPage = new PatientsPage(clinician);
        String patient = customUtils.getPatientDetails(config).get("Name");
        String summary = faker.aquaTeenHungerForce().character();
        String synthesis = faker.lordOfTheRings().character();

        if (arg0.equalsIgnoreCase("Proceed")) {
            log.info("Open menu option for searched patient.");
            patientsPage.openPatientMenu();

            log.info("Click on option [Handoff patient].");
            patientsPage.clickOnMenu("Handoff patient");
        }

        log.info("Enter Illness severity as Stable.");
        handOffPage.selectSeverity("Stable");

        log.info("Get summary value.");
        String expected = handOffPage.getHandOffSummary();

        log.info("Enter patient summary as [" + summary + "]");
        handOffPage.enterSummary(summary);

        log.info("Select awareness as [Ambulatory]");
        handOffPage.selectSituationalOptions("Ambulatory", true);

        handOffPage.swipeUp();

        log.info("Enter Synthesis as [" + synthesis + "]");
        handOffPage.enetrSynthesis(synthesis);

        log.info("Select No changes from prior visits");
        handOffPage.clickNoChange();

        log.info("Verify [Override all changes?] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Override all changes?"), "[Override all changes?] option not displayed.");

        log.info("Verify [Details from prior visit will replace the new changes that you have made.] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Details from prior visit will replace the new changes that you have made."),
                "[Details from prior visit will replace the new changes that you have made.] option not displayed.");

        log.info("Verify [Cancel] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Cancel"), "[Cancel] not displayed.");

        log.info("Verify [Proceed] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Proceed"), "[Proceed] not displayed.");

        if (arg0.equals("Proceed")) {
            log.info("Click on proceed option.");
            handOffPage.clickOnProceedOption();

//            log.info("No changes option should be selected.");
//            Assert.assertTrue(handOffPage.isNoChaneSelected(),
//                    "No Change option should be selected after cancel pop-up.");

            log.info("Click on Hand off button.");
            handOffPage.clickOnHandOffbtn();

            log.info("Click on proceed option.");
            handOffPage.clickOnProceedOption();

            log.info("Verify Handoff successfully toast message.");
            Assert.assertTrue(handOffPage.verifyHandOffSuccessToast(patient),
                    patient + " is handed off successfully");

            log.info("Open menu option for searched patient.");
            patientsPage.openPatientMenu();

            log.info("Click on option [Handoff patient].");
            patientsPage.clickOnMenu("Handoff patient");

            log.info("Verify handoff severity to make sure previous handoff done successfully.");
            Assert.assertTrue(handOffPage.verifyhandOffSummary(expected),
                    "Hand off summary not matched. Expected [" + expected + "].");
        } else {

            log.info("Click on cancel.");
            handOffPage.clickOnCancelOption();

            log.info("Verify Handoff page displayed.");
            Assert.assertTrue(handOffPage.isHandoffPageDisplayed(),
                    "Handoff page not displayed.");

            log.info("No changes option should not selected.");
            Assert.assertFalse(handOffPage.isNoChaneSelected(),
                    "No Change option should not be selected after cancel pop-up.");

            log.info("Click on back icon.");
            handOffPage.goBackToMyPatient();

            log.info("By clicking on back icon user should navigate to my patients screen");
            Assert.assertTrue(handOffPage.isHomePageDisplayed(),
                    "By clicking on back icon user should navigate to my patients screen");
        }
    }

    @Then("Proceed with No changes from prior visit option and press back on home screen")
    public void proceedWithNoChangesFromPriorVisitOptionAndPressBackOnHomeScreen() throws Exception {
        handOffPage = new HandOffPage(clinician);

        PatientsPage patientsPage = new PatientsPage(clinician);
        String patient = customUtils.getPatientDetails(config).get("Name");
        String summary = faker.aquaTeenHungerForce().character();
        String synthesis = faker.lordOfTheRings().character();

        log.info("Enter Illness severity as Stable.");
        handOffPage.selectSeverity("Stable");

        log.info("Get summary value.");
        String expected = handOffPage.getHandOffSummary();

        log.info("Enter patient summary as [" + summary + "]");
        handOffPage.enterSummary(summary);

        log.info("Select awareness as [Altered]");
        handOffPage.selectSituationalOptions("Altered", true);

        handOffPage.swipeUp();

        log.info("Enter Synthesis as [" + synthesis + "]");
        handOffPage.enetrSynthesis(synthesis);

        log.info("Select No changes from prior visits");
        handOffPage.clickNoChange();

        log.info("Verify [Override all changes?] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Override all changes?"), "[Override all changes?] option not displayed.");

        log.info("Verify [Details from prior visit will replace the new changes that you have made.] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Details from prior visit will replace the new changes that you have made."),
                "[Details from prior visit will replace the new changes that you have made.] option not displayed.");

        log.info("Verify [Cancel] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Cancel"), "[Cancel] not displayed.");

        log.info("Verify [Proceed] should be displayed.");
        Assert.assertTrue(handOffPage.isTextDisplayed("Proceed"), "[Proceed] not displayed.");

        log.info("Click on proceed option.");
        handOffPage.clickOnProceedOption();

        log.info("Click on back icon.");
        handOffPage.goBackToMyPatient();

        log.info("By clicking on back icon user should navigate to my patients screen");
        Assert.assertTrue(handOffPage.isHomePageDisplayed(),
                "By clicking on back icon user should navigate to my patients screen");
    }

    @Then("verify user cannot perform hand off for patient not in carer giver list")
    public void verifyUserCannotPerformHandOffForPatientNotInCarerGiverList() throws Exception {
        handOffPage = new HandOffPage(clinician);
        log.info("Verify patient handoff page displayed.");
        Assert.assertTrue(handOffPage.isHandoffPageDisplayed(),
                "Hand off page not displayed for unassigned patient.");

        log.info("Verify handoff options are not intractable.");
        Assert.assertTrue(handOffPage.isSummaryEditable(),
                "Summary option is editable for un-assigned patient.");
    }

    @And("verify hand off details on web dashboard")
    public void verifyHandOffDetailsOnWebDashboard() throws Exception {
        com.biofourmis.biovitals.pagefactory.web.HandOffPage handWeb =
                new com.biofourmis.biovitals.pagefactory.web.HandOffPage(web);
        HomePage homePage = new HomePage(web);
        homePage.searchPatientWithoutWait(CustomUtils.mnr);
        handWeb.clickOnPatientOptions();
        handWeb.clickOnOption("Hand off");
        handWeb.verifyDetails(details);
    }

    @Then("verify hand off details on HCP app")
    public void verifyHandOffDetailsOnHCPApp() {
        details.putAll(customUtils.getHandOffDetails());

        System.out.println();
    }

    @And("after reload all changes reflect in app")
    public void afterReloadAllChangesReflectInApp() throws Exception {
        log.info("Verify summary after auto refresh of hand off.");
        Assert.assertTrue(handOffPage.verifyhandOffSummary(details.get("summary")),
                "Patient summary not matched after accept auto refresh hand off.");

        //TODO verify others
    }

    @Then("verify auto refresh pop-up displayed in HCP app")
    public void verifyAutoRefreshPopUpDisplayedInHCPApp() {

        Assert.assertTrue(handOffPage.waitForAutoReloadHandOffPopUp(),
                "hand off Auto refresh pop up not displayed.");

        log.info("Verify auto refresh message [Someone else has updated handoff details]");
        Assert.assertTrue(handOffPage.isAutoReloadPopTextDisplayed("Someone else has updated handoff details"),
                "Auto refresh Message [Someone else has updated handoff details] not displayed.");

        Assert.assertTrue(handOffPage.isAutoReloadPopTextDisplayed("Reload to retrieve the latest update?"),
                "Auto refresh Message [Reload to retrieve the latest update?] not displayed.");

        Assert.assertTrue(handOffPage.isAutoReloadPopTextDisplayed("Dismiss"),
                "Auto refresh Button [Dismiss] not displayed.");

        Assert.assertTrue(handOffPage.isAutoReloadPopTextDisplayed("Reload"),
                "Auto refresh Button [Reload] not displayed.");
    }
}
