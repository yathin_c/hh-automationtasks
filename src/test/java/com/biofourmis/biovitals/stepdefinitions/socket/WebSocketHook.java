package com.biofourmis.biovitals.stepdefinitions.socket;

import com.biofourmis.biovitals.configs.AppConfig;
import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.constants.configuration.WebSocketConfig;
import com.biofourmis.biovitals.model.User;
import com.biofourmis.biovitals.model.patients.*;
import com.biofourmis.biovitals.model.socket.*;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.biofourmis.biovitals.utilities.RestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.SkipException;
import org.testng.annotations.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Log4j
public class WebSocketHook {

    @Autowired(required = false)
    private WebSocketConfig webSocketConfig;
    @Autowired(required = false)
    private TestConfig testConfig;
    @Autowired(required = false)
    private CustomConfig customConfig;
    @Autowired
    private AppConfig appConfig;
    @Autowired
    private Faker faker;
    private User user;

    private RestUtils restUtils = new RestUtils();
    private List<Map<String, String>> patientInformation;
    private List<String> executableFiles = new ArrayList<>();

    @Given("Generate patient data for load execution")
    public void generatePatientDataForLoadExecution() throws Throwable {

        //Generate Login Credentials
        patientInformation = new ArrayList<>();
        customConfig.loginIntoAppAndGenerateToken("Physician");

        //Collect site info
        user = customConfig.getUserConfig();

        //Get all patients list
        Map<String, String> data = new LinkedHashMap<>();
        data.put("url", customConfig.getBaseUrl() + "api/v1/patients");
        data.put("param", "myPatients=true");
        data.put("authToken", customConfig.getAccessToken());
        data.put("reqMethod", "GET");
        Response response = restUtils.call(data);
        Patients[] patientsData = new ObjectMapper()
                .convertValue(response
                                .getBody()
                                .jsonPath()
                                .get("data.patients")
                        , Patients[].class);

        //Create patients if required
        // if (patientsData.length < webSocketConfig.getVUsers()) {
        //Create new patient
        //    createPatient(webSocketConfig.getVUsers() - patientsData.length);
        //createPatient(webSocketConfig.getVUsers() - patientsData.length);
        List<String> patientIds = createPatient(webSocketConfig.getVUsers());
        data.put("url", customConfig.getBaseUrl() + "api/v1/patients");
        data.put("authToken", customConfig.getAccessToken());
        data.put("reqMethod", "GET");
        response = restUtils.call(data);
        patientsData = new ObjectMapper()
                .convertValue(response
                                .getBody()
                                .jsonPath()
                                .get("data.patients")
                        , Patients[].class);
        // }

        //Collect Hash Value for all patients
        for (int i = 0; i < webSocketConfig.getVUsers(); i++) {
            Map<String, String> temp = new LinkedHashMap<>();
            String id = patientIds.get(i);
            //String id = patientsData[i].getId();
            temp.put("id", id);

            data.put("url", customConfig.getBaseUrl() + "api/v1/patients/" + id + "/genqr?forceGenerate=true");
            data.put("authToken", customConfig.getAccessToken());
            data.put("reqMethod", "GET");
            response = restUtils.call(data);
            String qrHash = response.getBody().jsonPath().getString("data.qrHash");
            temp.put("qrHash", qrHash);

            //Collect Access token
            data = new LinkedHashMap<>();
            data.put("url", customConfig.getBaseUrl() + "api/v1/patients/" + id + "/acqr");
            data.put("reqMethod", "PATCH");
            data.put("qrHash", qrHash);
            response = restUtils.call(data);
            String accessToken = response.getBody().jsonPath().getString("data.accessToken");
            temp.put("accessToken", accessToken);
            patientInformation.add(temp);
        }
    }

    @And("Generate executable artillery file")
    public void generateExecutableArtilleryFile() throws IOException {
        for (Map<String, String> patient : patientInformation) {
            String filePath = generateExecutableFiles(patient);
            executableFiles.add(filePath);
        }
    }

    @Then("Start load testing execution")
    public void startLoadTestingExecution() throws InterruptedException, IOException {
        ExecutorService es = Executors.newFixedThreadPool(webSocketConfig.getVUsers());
        for (String path : executableFiles) {
            es.execute(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    String command = "npx artillery run _scriptFileName_ --output _reportFileName_"
                            .replace("_scriptFileName_", path)
                            .replace("_reportFileName_", path.replace("yml", "json"));
                    System.out.println("I'm running for path " + path);
                    Process p = Runtime.getRuntime().exec(command);
                    logOutput(p.getInputStream(), Thread.currentThread().getName(), Thread.currentThread());
                    logOutput(p.getErrorStream(), Thread.currentThread().getName() + " Error: ", Thread.currentThread());
                    p.waitFor();
                }
            });
        }
        es.shutdown();
        boolean finished = es.awaitTermination(webSocketConfig.getDuration() + 1000, TimeUnit.SECONDS);

        //  if (!finished)
        //      throw new SkipException("Execution is not yet completed. Skipping test for now. Try to re-run with different time frame.");

        //Comment for now
        // String reportFile = generateCommonJsonFile();

        for (String fileName : executableFiles) {
            String reportFile = fileName.replace("yml", "json");
            if (!new File(reportFile).exists())
                continue;
            String command = "npx artillery report _reportFileName_"
                    .replace("_reportFileName_", reportFile);

            Process p = Runtime.getRuntime().exec(command);

            Thread.sleep(10000);
            try {
                p.destroy();
            } catch (Exception e) {
            }
            //Send Report
            CustomUtils customUtils = new CustomUtils(null);
            customUtils.sendReport(reportFile + ".html", "Performance", "hh-automation-reports");
        }
    }

    private String generateCommonJsonFile() throws IOException {
        Aggregate aggregate = new Aggregate();
        ArrayList<Intermediate> intermediateArrayList = new ArrayList<>();
        for (String path : executableFiles) {
            String config = (FileUtils.readFileToString(new File(path.replace("yml", "json")),
                    StandardCharsets.UTF_8)).strip().replaceAll("\n", "");

            SocketReport socket = new ObjectMapper().readValue(config, SocketReport.class);

            Latency latency = new Latency();
            Rps rps = new Rps();
            ScenarioDuration scenarioDuration = new ScenarioDuration();
            ScenarioCounts scenarioCounts = new ScenarioCounts();
            Errors errors = new Errors();
            Codes codes = new Codes();
            CustomStats customStats = new CustomStats();
            Counters counters = new Counters();
            Phases phases = new Phases();
            Phases[] phasesArr = new Phases[1];

            if (Objects.isNull(aggregate.getTimestamp()))
                aggregate.setTimestamp(socket.getAggregate().getTimestamp());

            if (Objects.isNull(aggregate.getScenariosCreated()))
                aggregate.setScenariosCreated(socket.getAggregate().getScenariosCreated());
            else
                aggregate.setScenariosCreated(aggregate.getScenariosCreated() + socket.getAggregate().getScenariosCreated());

            if (Objects.isNull(aggregate.getScenariosCompleted()))
                aggregate.setScenariosCompleted(socket.getAggregate().getScenariosCompleted());
            else
                aggregate.setScenariosCompleted(aggregate.getScenariosCompleted() + socket.getAggregate().getScenariosCompleted());

            if (Objects.isNull(aggregate.getRequestsCompleted()))
                aggregate.setRequestsCompleted(socket.getAggregate().getRequestsCompleted());
            else
                aggregate.setRequestsCompleted(aggregate.getRequestsCompleted() + socket.getAggregate().getRequestsCompleted());

            if (Objects.isNull(aggregate.getMatches()))
                aggregate.setMatches(socket.getAggregate().getMatches());
            else
                aggregate.setMatches(aggregate.getMatches() + socket.getAggregate().getMatches());

            if (Objects.isNull(aggregate.getScenariosAvoided()))
                aggregate.setScenariosAvoided(socket.getAggregate().getScenariosAvoided());
            else
                aggregate.setScenariosAvoided(aggregate.getScenariosAvoided() + socket.getAggregate().getScenariosAvoided());

            if (Objects.isNull(aggregate.getLatency()))
                aggregate.setLatency(socket.getAggregate().getLatency());
            else {
                Latency temp = aggregate.getLatency();
                temp.setMax(temp.getMax() + socket.getAggregate().getLatency().getMax());
                temp.setMin(temp.getMin() + socket.getAggregate().getLatency().getMin());
                temp.setMedian(temp.getMedian() + socket.getAggregate().getLatency().getMedian());
                temp.setP95(temp.getP95() + socket.getAggregate().getLatency().getP95());
                temp.setP99(temp.getP99() + socket.getAggregate().getLatency().getP99());
                //Total
                aggregate.setLatency(temp);
            }

            if (Objects.isNull(aggregate.getRps()))
                aggregate.setRps(socket.getAggregate().getRps());
            else {
                Rps temp = aggregate.getRps();
                temp.setCount(temp.getCount() + socket.getAggregate().getRps().getCount());
                temp.setMean(temp.getMean() + socket.getAggregate().getRps().getMean());
                //Total
                aggregate.setRps(temp);
            }

            if (Objects.isNull(aggregate.getScenarioDuration()))
                aggregate.setScenarioDuration(socket.getAggregate().getScenarioDuration());
            else {
                ScenarioDuration temp = aggregate.getScenarioDuration();
                temp.setMin(temp.getMin() + socket.getAggregate().getScenarioDuration().getMin());
                temp.setMax(temp.getMax() + socket.getAggregate().getScenarioDuration().getMax());
                temp.setMedian(temp.getMedian() + socket.getAggregate().getScenarioDuration().getMedian());
                temp.setP95(temp.getP95() + socket.getAggregate().getScenarioDuration().getP95());
                temp.setP99(temp.getP99() + socket.getAggregate().getScenarioDuration().getP99());

                //Total
                aggregate.setScenarioDuration(temp);
            }

            if (Objects.isNull(aggregate.getPhases()))
                aggregate.setPhases(socket.getAggregate().getPhases());
            else {
                Phases temp = aggregate.getPhases().get(0);
                temp.setDuration(temp.getDuration() + socket.getAggregate().getPhases().get(0).getDuration());
                temp.setArrivalRate(temp.getArrivalRate() + socket.getAggregate().getPhases().get(0).getArrivalRate());
                temp.setMaxVuser(temp.getMaxVuser() + socket.getAggregate().getPhases().get(0).getMaxVuser());

                ArrayList<Phases> tempPhase = new ArrayList<>();
                tempPhase.add(temp);

                //Total
                aggregate.setPhases(tempPhase);
            }

            intermediateArrayList.addAll(socket.getIntermediate());
        }

        int vUsers = webSocketConfig.getVUsers();

        //Calculation-------------->
        //Latency
        Latency latency = aggregate.getLatency();
        latency.setMax(latency.getMax() / vUsers);
        latency.setMin(latency.getMin() / vUsers);
        latency.setMedian(latency.getMedian() / vUsers);
        latency.setP95(latency.getP95() / vUsers);
        latency.setP99(latency.getP99() / vUsers);
        aggregate.setLatency(latency);

        //RPS
        Rps rps = aggregate.getRps();
        rps.setCount(rps.getCount() / vUsers);
        rps.setMean(rps.getMean() / vUsers);
        aggregate.setRps(rps);

        //Scenario Duration
        ScenarioDuration scenarioDuration = aggregate.getScenarioDuration();
        scenarioDuration.setMin(scenarioDuration.getMin() / vUsers);
        scenarioDuration.setMax(scenarioDuration.getMax() / vUsers);
        scenarioDuration.setMedian(scenarioDuration.getMedian() / vUsers);
        scenarioDuration.setP95(scenarioDuration.getP95() / vUsers);
        scenarioDuration.setP99(scenarioDuration.getP99() / vUsers);
        aggregate.setScenarioDuration(scenarioDuration);

        //Phases
        Phases phases = aggregate.getPhases().get(0);
        phases.setDuration(phases.getDuration() / vUsers);
        ArrayList<Phases> tempPhase = new ArrayList<>();
        tempPhase.add(phases);
        aggregate.setPhases(tempPhase);

        SocketReport socket = new SocketReport();
        socket.setAggregate(aggregate);
        socket.setIntermediate(intermediateArrayList);

        String data = new ObjectMapper().writeValueAsString(socket)
                .replaceAll("zero", "0")
                .replaceAll("twoHundred", "200")
                .replaceAll("fiveHundred", "500");
        //Filter others which are null

        String reportFileName = executableFiles.get(0).substring(0, executableFiles.get(0).lastIndexOf("/") + 1) + "report.json";
        File reportFile = new File(reportFileName);

        FileUtils.writeStringToFile(reportFile, data, StandardCharsets.UTF_8);
        return reportFileName;
    }

    private void logOutput(InputStream inputStream, String prefix, Thread thread) {
        new Thread(() -> {
            Scanner scanner = new Scanner(inputStream, "UTF-8");
            while (scanner.hasNextLine()) {
                synchronized (this) {
                    try {
                        System.out.println((prefix + scanner.nextLine()));
//                        if (scanner.nextLine().contains("NAN") || scanner.nextLine().contains("websocket error")) {
//                            try {
//                                //System.out.println((prefix + scanner.nextLine()))
//                                //thread.
//                                //System.out.println("----------------->Closing thread "+ thread.getName());
//                            } catch (Exception e) {
//                            }
//                        }
                    } catch (Exception e) {
                    }
                }
            }
            scanner.close();
        }).start();
    }

    private List<String> createPatient(int patients) throws Throwable {
        List<String> patientId = new ArrayList<>();
        for (int i = 0; i < patients; i++) {
            CreatePatient createPatient = CreatePatient.builder()
                    .mrn(String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)))
                    .firstName(faker.name().firstName())
                    .lastName(faker.name().lastName())
                    .dob("10/12/1980")
                    .gender("M")
                    .mobileNo(String.valueOf(LocalDateTime.now().minusMonths(2).toEpochSecond(ZoneOffset.UTC)))
                    .phoneNo(String.valueOf(LocalDateTime.now().minusMonths(3).toEpochSecond(ZoneOffset.UTC)))
                    .address(Address.builder()
                            .addressLine1(faker.address().fullAddress())
                            .addressLine2("")
                            .city(faker.address().city())
                            .state(faker.address().state())
                            .zip("010101")
                            .build())
                    .familyContacts(new FamilyContact[]{FamilyContact.builder()
                            .name(faker.name().lastName())
                            .phone("00101010101")
                            .relationship("Bro")
                            .build()})
                    .locale("en")
                    .role("patient")
                    .site(user.getSite().getId())
                    .medicalHistory(MedicalHistory.builder()
                            .allergies("")
                            .clinicalSummary("")
                            .contactPrecautions(null)
                            .admittingDiagonosis(null)
                            .codeStatus("")
                            .build())
                    .contacts(new String[]{user.getId()})
                    .build();

            Map<String, String> data = new LinkedHashMap<>();
            data.put("url", customConfig.getBaseUrl() + "api/v1/patients");
            data.put("authToken", customConfig.getAccessToken());
            data.put("reqMethod", "POST");
            data.put("reqBody", new ObjectMapper().writeValueAsString(createPatient));
            Response response = restUtils.call(data);
            if (response.statusCode() != 201)
                throw new RuntimeException("Not able to create patient. Causes : " + response.getBody().prettyPrint());

            patientId.add(response.jsonPath().get("data.patient.id"));
        }
        return patientId;
    }

    private String generateExecutableFiles(Map<String, String> patient) throws IOException {
        String filePath = appConfig.getCurrentWorkingDir() + customConfig.getSocketExecutableFilePath() + patient.get("id") + ".yml";
        //Prepare config template
        String config = (FileUtils.readFileToString(new File(appConfig.getCurrentWorkingDir() + customConfig.getTempFileDir() + "config.tmpl"),
                StandardCharsets.UTF_8));

        config = config.replace("_url_", webSocketConfig.getSocketUrl())
                .replace("_duration_", String.valueOf(webSocketConfig.getDuration()))
                .replace("_name_", "Emit logs for patient " + patient.get("id"))
                .replace("_token_", patient.get("accessToken"))
                .replaceAll("_env_", testConfig.getEnvironment().toLowerCase(Locale.ROOT));

        StringBuilder emits = new StringBuilder();

        for (String channel : customConfig.getWebSocketEvents().strip().split(",")) {
            String channelName = channel.strip();
            Integer interval = 0;

            String emit = (FileUtils.readFileToString(new File(appConfig.getCurrentWorkingDir() + customConfig.getTempFileDir() + "emit.tmpl"),
                    StandardCharsets.UTF_8));

            if (channelName.equalsIgnoreCase("heartbeat"))
                interval = faker.number().numberBetween(28, 30);
            else if (channelName.equalsIgnoreCase("deviceStatusUpdate"))
                interval = faker.number().numberBetween(30, 180);
            else if (channelName.equalsIgnoreCase("mobileDeviceStatusUpdate"))
                interval = faker.number().numberBetween(60, 210);

            emit = emit
                    .replace("_channel_", channelName)
                    .replace("_data_", getData(channelName, patient.get("id")))
                    .replace("_interval_", String.valueOf(interval));

            emits.append(emit);
        }

        StringBuilder fileData = new StringBuilder();
        fileData.append(config).append(emits);

        //Write into file
        FileUtils.writeStringToFile(new File(filePath),
                String.valueOf((fileData)), StandardCharsets.UTF_8);

        return filePath;
    }

    private String getData(String channelName, String id) throws JsonProcessingException {
        String value = null;

        if (channelName.equalsIgnoreCase("heartbeat")) {
            value = "{{ heartBeat }}";
        } else if (channelName.equalsIgnoreCase("deviceStatusUpdate")) {
            value = "[" + getDeviceStatusUpdate(id) + "]";
        } else if (channelName.equalsIgnoreCase("mobileDeviceStatusUpdate")) {
            value = getMobileDeviceStatusUpdate(id);
        } else
            throw new RuntimeException("This channel is not supported. Name [" + channelName + "]");

        String modifyValue = value
                .replaceAll("\"mobBleStatus\"", "{{ mobBleStatus }}")
                .replaceAll("\"mobCharging\"", "{{ mobCharging }}");

        modifyValue = modifyValue.replaceAll("\"deviceBleStatus\"", "{{ deviceBleStatus }}")
                .replaceAll("\"deviceCharging\"", "{{ deviceCharging }}");
        //.replaceAll("\"mobBleStatus\"", "{{ mobBleStatus }}");
        return modifyValue;
    }

    private String getMobileDeviceStatusUpdate(String id) throws JsonProcessingException {

        return new ObjectMapper().writeValueAsString(
                MobileDeviceStatusUpdate.builder()
                        .batteryLevel("{{ mobBatteryLevel }}")
                        .bluetoothStatus("mobBleStatus")
                        .bundleId("com.biofourmis.careathomerpm")
                        .code("{{ mobCode }}")
                        .device("android")
                        .deviceId(id)
                        .deviceModel("SM-P614")
                        .deviceToken("frYbyrlkRzyKf3dkEDkWAE:APA91bE8_ztG0N" +
                                faker.number().numberBetween(100, 999) +
                                "qogZqgnFV1fx_MOTbrfwdcRC" +
                                "y2nXaKHIpw1oU4z1WOgRzF3jlfKVKFMmlNH2ePjJYro65GBEm4fHhSjGeL-jGVwPaPux-ME" +
                                faker.number().numberBetween(100, 999))
                        .isAppDebugMode(false)
                        .isOnCharger("mobCharging")
                        .name(getDeviceName())
                        .networkStrength("{{ mobNet }}")
                        .version("1.44.0-debug")
                        .osVersion("Android 11")
                        .voipToken("")
                        .user(com.biofourmis.biovitals.model.socket.User
                                .builder()
                                ._id(id)
                                ._schema("Patient")
                                .build())
                        .build()
        );
    }

    private String getCode() {
        return List.of("NETWORK_UPDATE", "BLUETOOTH_UPDATE", "BATTERY_UPDATE")
                .parallelStream()
                .findAny().get();
    }

    private Integer getNetwork() {
        return List.of(1, 2, 3, 4)
                .parallelStream()
                .findAny().get();
    }

    private String getDeviceName() {
        return List.of("samsung")
                .parallelStream()
                .findAny().get();
    }

    private String getDeviceStatusUpdate(String id) throws JsonProcessingException {
        String ble = getBleState();
        return new ObjectMapper().writeValueAsString(
                DeviceStatusUpdate.builder()
                        .batteryLevel("{{ deviceBattery }}")
                        .bluetoothCode("{{ deviceBleCode }}")
                        .id(id)
                        .identifier("VC-EE" + faker.number().numberBetween(5, 9) + "4-BB7F-" + faker.number().numberBetween(0, 9) + "BFB")
                        .lastSyncTime("{{ lstSync }}")
                        .name("Vital Patch")
                        .status("deviceBleStatus")
                        .isOnCharger("deviceCharging")
                        .manufacturer("VitalConnect")
                        .type("continuous-monitoring-vc")
                        .typeLabel("Continuous monitoring")
                        .code("DEVICE_ON_SKIN")
                        .build()

                //VitalConnect
                //Vital Patch
                //continuous-monitoring-everion
                //VC2B008BF_01D83A
        );
    }

    private String getBleState() {
        return List.of("BLE_CONNECTED", "BLE_OUT_OF_RANGE", "BLE_DISCONNECTED", "NO_BLUETOOTH")
                .parallelStream()
                .findAny().get();
    }

    private Boolean getValue() {
        return List.of(true, false).stream().findAny().get();
    }
}
