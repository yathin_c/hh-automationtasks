package com.biofourmis.biovitals.stepdefinitions.patient;

import com.biofourmis.biovitals.pagefactory.patient.DiaryNotesPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.event.annotation.BeforeTestMethod;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Log4j
public class DiaryNotesHook {

    @Autowired(required = false)
    @Qualifier(value = "HH")
    private WebDriver patient;

    @Autowired
    private Faker faker;

    private DiaryNotesPage diaryNotesPage = null;
    private CustomUtils customUtils = new CustomUtils(null);

    @Then("Navigate to Diary and verify components in patient app")
    public void navigateToDiaryAndVerifyComponents() throws Exception {
        diaryNotesPage = new DiaryNotesPage(patient);

        log.info("Navigate to Diary notes tab.");
        Assert.assertTrue(diaryNotesPage.navigateToDiary(), "Diary screen not displayed.");

        log.info("Verify add new note button should be displayed.");
        Assert.assertTrue(diaryNotesPage.verifyAddNewNoteButtonDisplayed(),
                "Add new note button should be displayed.");

        log.info("Verify search note option.");
        Assert.assertTrue(diaryNotesPage.verifySearchDisplayed(),
                "Search note option should be displayed/enabled.");

        log.info("Verify start creating not message on empty screen.");
        Assert.assertTrue(diaryNotesPage.isOptionDisplayed(diaryNotesPage._getString("text.add.new.note.info")),
                "");

        log.info("Verify No notes yet! message on empty screen.");
        Assert.assertTrue(diaryNotesPage.isOptionDisplayed(diaryNotesPage._getString("text.no.notes.yet")),
                "");

    }


    @And("^Verify add a new diary note with (required|all) fields in patient app$")
    public void verifyAddANewDiaryNote(String arg0) throws Exception {
        diaryNotesPage = new DiaryNotesPage(patient);
        Map<String, String> diaryNote = customUtils.getDefaultDiaryNoteDetails(noteDetails());
        boolean isRequiredOnly = arg0.equals("required");

        log.info("Navigate to Diary notes tab.");
        Assert.assertTrue(diaryNotesPage.navigateToDiary(), "Diary screen not displayed.");

        log.info("Click on Add new note.");
        diaryNotesPage.clickOnAddNote();

        log.info("Verify add note screen.");

        List<String> otherTexts = Arrays.asList("Add new note",
                "Please call, don’t make a note if this is an urgent problem",
                "Note title", "Enter title", "Description", "Enter description",
                "Select icon (optional)", "Add pictures (optional)", "Add photo");
        for (String value : otherTexts) {
            log.info("Verify item [" + value + "] should be displayed");
            Assert.assertTrue(diaryNotesPage.verifyAddNotePage(value),
                    "[" + value + "] option should be displayed");
        }

        List<String> symptomType = Arrays.asList("Pain", "Fever", "Cough", "Vitals", "Eating",
                "Drinking", "Wound", "Medicine", "Weight", "Other");
        for (String value : symptomType) {
            log.info("Verify item [" + value + "] should be displayed");
            Assert.assertTrue(diaryNotesPage.verifyAddNotePage(value),
                    "[" + value + "] option should be displayed");
        }
        log.info("Add a new diary note with required fields only. Details [" + diaryNote + "].");
        diaryNotesPage.addNewDiaryNoteDetails(diaryNote, isRequiredOnly);

        log.info("Verify note added successfully.");
        String title = customUtils.getDefaultDiaryNoteDetails().get("Title");
        Assert.assertTrue(diaryNotesPage.verifyNoteAddedSuccessfully(title),
                "Diary note with title as [" + title + "] should be displayed.");

        log.info("Verify note details and click on back to diary.");
        Assert.assertFalse(diaryNotesPage.verifyNoteDetails(customUtils.getDefaultDiaryNoteDetails(), isRequiredOnly, false),
                "Note description should not be displayed.");
    }

    @Then("^Verify update diary note with (required|all) fields in patient app$")
    public void verifyUpdateDiaryNoteWithRequiredFieldsInPatientApp(String arg0) throws Exception {
        diaryNotesPage = new DiaryNotesPage(patient);
        boolean isRequiredOnly = arg0.equals("required");

        String title = customUtils.getDefaultDiaryNoteDetails().get("Title");
        log.info("Open a note with title [" + title + "].");
        diaryNotesPage.openNote(title);

        log.info("Update diary note.");
        int isUpdate = diaryNotesPage.updateNoteDetails(noteDetails(), isRequiredOnly);
        int count = Integer.parseInt(customUtils.getDefaultDiaryNoteDetails().get("Images"));
        customUtils.getDefaultDiaryNoteDetails().put("Images", String.valueOf(count + isUpdate));

        title = customUtils.getDefaultDiaryNoteDetails().get("Title");
        log.info("Verify updated title of note. Title : " + title);
        Assert.assertTrue(diaryNotesPage.verifyNoteAddedSuccessfully(title),
                "Note with title [" + title + "] should be displayed.");

        Assert.assertFalse(diaryNotesPage.verifyNoteDetails(customUtils.getDefaultDiaryNoteDetails(), isRequiredOnly, true),
                "Note description should not be displayed.");
    }

    private Map<String, String> noteDetails() {
        List<String> iconsType = customUtils.getIconType();
        iconsType.remove(customUtils.getDefaultDiaryNoteDetails().get("Type"));
        Map<String, String> updatedNoteDetails = new LinkedHashMap<>();
        updatedNoteDetails.put("Title", faker.name().username() + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        updatedNoteDetails.put("Description", customUtils.getRandomString());
        updatedNoteDetails.put("Type", iconsType.get(faker.number().numberBetween(0, iconsType.size())));
        return customUtils.getDefaultDiaryNoteDetails(updatedNoteDetails);
    }

    @And("Verify response on diary note in patient app")
    public void verifyResponseOnDiaryNoteInPatientApp() throws Exception {
        diaryNotesPage = new DiaryNotesPage(patient);

        String title = customUtils.getDefaultDiaryNoteDetails().get("Title");
        log.info("Verify Clinician response on diary note.");
        Assert.assertTrue(diaryNotesPage.verifyResponse(title),
                "[Talk soon] as a response should be displayed on note [" + title + "].");

        customUtils.setLastAcknowledgeNote(title);
    }

    @And("Verify mandatory fields while add new note")
    public void verifyMandatoryFieldsWhileAddNewNote() throws Exception {
        diaryNotesPage = new DiaryNotesPage(patient);

        log.info("Navigate to Diary notes tab.");
        Assert.assertTrue(diaryNotesPage.navigateToDiary(), "Diary screen not displayed.");

        diaryNotesPage.clickOnAddNote();

        Assert.assertFalse(diaryNotesPage.isSaveNoteBtnEnable(),
                "");
        diaryNotesPage.addNoteTitle(customUtils.getRandomString() + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));

        Assert.assertFalse(diaryNotesPage.isSaveNoteBtnEnable(),
                "");
        diaryNotesPage.iconType(customUtils.getIconType().get(1));
        Assert.assertFalse(diaryNotesPage.isSaveNoteBtnEnable(),
                "");
        diaryNotesPage.addImg(1);
        Assert.assertFalse(diaryNotesPage.isSaveNoteBtnEnable(),
                "");

        diaryNotesPage.addDescription(customUtils.getRandomString());
        Assert.assertTrue(diaryNotesPage.isSaveNoteBtnEnable(),
                "");
    }

    @And("Verify canceling note after adding details")
    public void verifyCancelingNoteAfterAddingDetails() throws Exception {
        diaryNotesPage = new DiaryNotesPage(patient);

        log.info("Navigate to Diary notes tab.");
        Assert.assertTrue(diaryNotesPage.navigateToDiary(), "Diary screen not displayed.");

        diaryNotesPage.clickOnAddNote();

        String title = customUtils.getRandomString() + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        diaryNotesPage.addNoteTitle(title);

        diaryNotesPage.iconType(customUtils.getIconType().get(1));

        diaryNotesPage.addImg(1);

        diaryNotesPage.addDescription(customUtils.getRandomString());

        diaryNotesPage.clickOnCancelBtn();

        Assert.assertFalse(diaryNotesPage.verifyNoteAddedSuccessfully(title),
                "");


    }

    @Then("Verify patient can't edit responded diary notes")
    public void verifyPatientCanTEditRespondedDiaryNotes() {
        Assert.assertFalse(diaryNotesPage.isEditButtonVisible(),
                "");

        diaryNotesPage.clickOnBackToDiary();
    }
}
