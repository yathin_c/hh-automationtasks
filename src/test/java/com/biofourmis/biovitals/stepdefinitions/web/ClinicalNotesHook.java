package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.ClinicalNotesPageApp;
import com.biofourmis.biovitals.pagefactory.web.ClinicalNotesPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.TextStyle;
import java.util.*;

@Log4j
public class ClinicalNotesHook {
    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;
    @Autowired
    private CustomConfig customConfig;
    @Autowired
    private TestConfig testConfig;
    @Autowired
    private Faker faker;

    private ClinicalNotesPage clinicalNotesPage;
    private ClinicalNotesPageApp clinicalNotesPageApp;
    private final CustomUtils customUtils = new CustomUtils(null);

    @Then("Verify clinical notes page")
    public void verifyClinicalNotesPage() throws Exception {
        clinicalNotesPage = new ClinicalNotesPage(web);

        clinicalNotesPage.log("Click on clinical notes.");
        clinicalNotesPage.clickOnClinicalNotes();

        clinicalNotesPage.log("Verify New clinical note button should be displayed.");
        Assert.assertTrue(clinicalNotesPage.verifyNewClinicalNotButtonWhenNoNote(),
                "New Clinical note button should be visible.");

        List<String> stringOnScreen = Arrays.asList(
                "No clinical notes yet",
                "You can use this space to keep notes about patient's condition or update.");
        for (String value : stringOnScreen) {
            clinicalNotesPage.log("Verify [" + value + "] should be displayed.");
            Assert.assertTrue(clinicalNotesPage.verifyNewClinicalNoteText(value),
                    "[" + value + "] should be visible.");
        }
    }

    @And("Verify empty fields error message")
    public void addNewNoteAndVerifyEmptyFieldsErrorMessage() throws Exception {
        clinicalNotesPage = new ClinicalNotesPage(web);
        clinicalNotesPage.log("Click on clinical notes.");
        clinicalNotesPage.clickOnClinicalNotes();

        clinicalNotesPage.log("Click on add new clinical note.");
        clinicalNotesPage.clickOnAddNewNote();

        clinicalNotesPage.log("Verify new clinical note heading.");
        Assert.assertTrue(clinicalNotesPage.verifyNewClinicalNoteHeading(), "Notes heading [New clinical note] should be found.");

        clinicalNotesPage.log("Verify [Title] heading present.");
        Assert.assertTrue(clinicalNotesPage._isTextContainsPresent("Title"),
                "[Title] should be displayed.");

        clinicalNotesPage.log("Verify [Description] heading present.");
        Assert.assertTrue(clinicalNotesPage._isTextContainsPresent("Description"),
                "[Description] should be displayed.");

        String titleMsg = "Please specify title for your note";
        String descMsg = "Please specify description for your note";
        clinicalNotesPage.log("Verify title field error message. " + titleMsg);
        clinicalNotesPage.log("Verify description field error message. " + descMsg);
        Assert.assertTrue(clinicalNotesPage.verifyErrorMessage(titleMsg, descMsg), "Input fields error message [Please specify title for your note / Please specify description for your note] not displayed.");

        clinicalNotesPage.log("Verify Create button disabled.");
        Assert.assertFalse(clinicalNotesPage.createButtonDisable(), "Create button should be disable while input fields are empty.");
    }

    private Map<String, String> details;

    private void createNewNote() throws Exception {
        details = new LinkedHashMap<>(customUtils.getDefaultClinicalNotesDetails(true));

        Assert.assertFalse(clinicalNotesPage.isCreateBtnEnable(),
                "Create button should be disable.");

        clinicalNotesPage.log("Add note title as [" + details.get("Title") + "].");
        clinicalNotesPage.addTitle(details.get("Title"));
        Assert.assertFalse(clinicalNotesPage.isCreateBtnEnable(),
                "Create button should be disable.");

        clinicalNotesPage.log("Add note description as [" + details.get("Description") + "].");
        clinicalNotesPage.addDescription(details.get("Description"));
        Assert.assertTrue(clinicalNotesPage.isCreateBtnEnable(),
                "Create button should be enable.");

        clinicalNotesPage.log("Click on create button.");
        clinicalNotesPage.clickOnCreateBtn();

        Assert.assertTrue(clinicalNotesPage.verifyToastMessage("Clinical note created successfully"),
                "Clinical note created successfully toast not displayed.");
    }

    private void verifyCreatedNote() throws Exception {
        log.info("Verify newly created note.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails(false);

        String title = details.get("Title");
        String description = details.get("Description");

        clinicalNotesPage.log("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        clinicalNotesPage.log("Note with title [" + title + "] should be present.");
        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(title),
                "Note title [" + title + "] should be displayed.");

        clinicalNotesPage.log("Note description [" + description + "] should be present.");
        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(description),
                "Note description [" + description + "] should be displayed.");

        clinicalNotesPage.log("Created by clinician name [" + testConfig.getClinicianName() + "] should be displayed.");
        Assert.assertTrue(clinicalNotesPage.verifyCareGiver(testConfig.getClinicianName()),
                "Clinician name should be displayed as [" + testConfig.getClinicianName() + "].");
    }

    private void clickOnAddNewNote() throws Exception {
        clinicalNotesPage = new ClinicalNotesPage(web);

        clinicalNotesPage.log("Click on Clinical notes.");
        clinicalNotesPage.clickOnClinicalNotes();

        clinicalNotesPage.log("Click on Add new note.");
        clinicalNotesPage.clickOnAddNewNote();
    }

    private void searchForNoteAndUpdateTheTitle(String arg0) throws Exception {
        String title = customUtils.getDefaultClinicalNotesDetails(false).get("Title");

        clinicalNotesPage.log("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        clinicalNotesPage.log("Click on Edit icon.");
        clinicalNotesPage.clickOnEditIcon();

        Map<String, String> updateNote = new LinkedHashMap<>();
        if (arg0.equals("title")) {
            updateNote.put("Title", faker.lordOfTheRings().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            updateNote.put("Description", customUtils.getDefaultClinicalNotesDetails(false).get("Description"));
            clinicalNotesPage.log("Update note  title as [" + updateNote.get("Title") + "].");
            clinicalNotesPage.updateNoteTitle(customUtils.getDefaultClinicalNotesDetails(updateNote));
        } else {
            updateNote.put("Title", customUtils.getDefaultClinicalNotesDetails(false).get("Title"));
            updateNote.put("Description", faker.aquaTeenHungerForce().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            clinicalNotesPage.log("Update note  description as [" + updateNote.get("Description") + "].");
            clinicalNotesPage.updateNoteDescription(customUtils.getDefaultClinicalNotesDetails(updateNote));
        }
    }

    private void searchForNoteAndUpdate() throws Exception {
        String title = customUtils.getDefaultClinicalNotesDetails(false).get("Title");

        clinicalNotesPage.log("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        Map<String, String> updateNote = new LinkedHashMap<>();
        updateNote.put("Title", faker.lordOfTheRings().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        updateNote.put("Description", faker.aquaTeenHungerForce().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));

        clinicalNotesPage.log("Click on Edit icon.");
        clinicalNotesPage.clickOnEditIcon();

        clinicalNotesPage.log("Update note  title as [" + updateNote.get("Title") + "].");
        clinicalNotesPage.updateNoteTitle(customUtils.getDefaultClinicalNotesDetails(updateNote));

        clinicalNotesPage.log("Update note  description as [" + updateNote.get("Description") + "].");
        clinicalNotesPage.updateNoteDescription(customUtils.getDefaultClinicalNotesDetails(updateNote));
    }

    private void verifyUpdatedNote() throws Exception {
        clinicalNotesPage.log("Verify updated created note.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails(false);

        String title = details.get("Title");
        String description = details.get("Description");

        clinicalNotesPage.log("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        clinicalNotesPage.log("Note with title [" + title + "] should be present.");
        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(title),
                "Note title [" + title + "] should be displayed.");

        clinicalNotesPage.log("Note description [" + description + "] should be present.");
        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(description),
                "Note description [" + description + "] should be displayed.");
    }

    public void searchForNote() throws Exception {
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails(false);

        String title = details.get("Title");
        clinicalNotesPage.log("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);
    }

    private void verifyConfirmationPopUpAndDecline(String arg0) throws Exception {
        clinicalNotesPage.log("Verify newly created note.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails(false);

        clinicalNotesPage.log("Click on delete pop-up");
        clinicalNotesPage.verifyDeletePopUP();

        if (arg0.equals("accept")) {
            clinicalNotesPage.log("Accept delete pop-up.");
            clinicalNotesPage.performCancelPopUp(true);
//            clinicalNotesPage.log("Search for note [" + title + "].");
//            clinicalNotesPage.searchForNote(title);
//            clinicalNotesPage.log("Note should not be displayed.");
//            Assert.assertFalse(clinicalNotesPage.verifyCreatedNote(title),
//                    "Note title [" + title + "] should not be displayed.");
        } else {
            clinicalNotesPage.log("Decline delete pop-up.");
            clinicalNotesPage.performCancelPopUp(false);
//            clinicalNotesPage.log("Search for note [" + title + "].");
//            clinicalNotesPage.searchForNote(title);
//            clinicalNotesPage.log("Note should be displayed.");
//            Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(title),
//                    "Note title [" + title + "] should be displayed.");
        }
    }

    @Then("Verify update a note")
    public void verifyUpdateANote() throws Exception {
        searchForNoteAndUpdateTheTitle("title");

        clinicalNotesPage.log("Click on save.");
        clinicalNotesPage.clickOnSave();

        log.info("Verify updated note details.");
        verifyUpdatedNote();

        log.info("Search for note.");
        searchForNoteAndUpdateTheTitle("description");

        clinicalNotesPage.log("Click on save.");
        clinicalNotesPage.clickOnSave();

        log.info("Update note details.");
        verifyUpdatedNote();

        log.info("Search and update note details.");
        searchForNoteAndUpdate();

        clinicalNotesPage.log("Click on save.");
        clinicalNotesPage.clickOnSave();

        log.info("Verify updated note details.");
        verifyUpdatedNote();
    }

    @Then("Verify Delete a note")
    public void deleteANote() throws Exception {
        String title = customUtils.getDefaultClinicalNotesDetails(false).get("Title");
        log.info("Search for note.");
        searchForNote();

        clinicalNotesPage.log("Delete note.");
        clinicalNotesPage.deleteNote();

        clinicalNotesPage.log("Decline delete pop-up.");
        verifyConfirmationPopUpAndDecline("decline");

        clinicalNotesPage.log("Search for note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        clinicalNotesPage.log("Note should be displayed.");
        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(title),
                "Note title [" + title + "] should be displayed.");

//        clinicalNotesPage.log("Search for note.");
//        searchForNote();

        clinicalNotesPage.log("Delete note.");
        clinicalNotesPage.deleteNote();

        clinicalNotesPage.log("Accept delete pop-up.");
        verifyConfirmationPopUpAndDecline("accept");

        clinicalNotesPage.log("Verify note deleted toast message [Clinical note deleted successfully] displayed.");
        clinicalNotesPage.verifyToastMessage("Clinical note deleted successfully");

        clinicalNotesPage.log("Search for note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        clinicalNotesPage.log("Note should not be displayed.");
        Assert.assertFalse(clinicalNotesPage.verifyCreatedNote(title),
                "Note title [" + title + "] should not be displayed.");
    }

    @Then("Verify Add a note")
    public void verifyAddANote() throws Exception {
        clickOnAddNewNote();
        createNewNote();
        verifyCreatedNote();
    }

    @Then("Verify Search filter input field")
    public void verifySearchFilterInputField() throws Exception {
        clinicalNotesPage = new ClinicalNotesPage(web);

        log.info("Get Clinical note details.");
        Map<String, String> data = customUtils.getDefaultClinicalNotesDetails(false);

        Assert.assertTrue(clinicalNotesPage.verifySearchInputOptions(),
                "Search input field should be visible.");

        String title = data.get("Title");
        clinicalNotesPage.log("Enter values [" + title + "] in search field.");
        clinicalNotesPage.inputValueInSearchField(title);

        Assert.assertTrue(clinicalNotesPage.verifySearchedNoteDisplayed(title),
                "Note with title [" + title + "] should be displayed.");

        String randomValue = faker.random().hex() + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);

        clinicalNotesPage.log("Enter values [" + randomValue + "] in search field.");
        clinicalNotesPage.inputValueInSearchField(randomValue);

        Assert.assertTrue(clinicalNotesPage.verifyCrossIconAfterEnterValueInField(),
                "Cross icon should be displayed in search filter to clear input value.");

        Assert.assertFalse(clinicalNotesPage.verifySearchedNoteDisplayed(randomValue),
                "Note with random values " + randomValue + " should not be displayed.");

        Assert.assertTrue(clinicalNotesPage.verifyRandomSearchmessage("No clinical notes found"),
                "No clinical notes found message should be displayed.");

        Assert.assertTrue(clinicalNotesPage.verifyRandomSearchmessage("Try to choose different date and filter combination."),
                "Try to choose different date and filter combination. should be displayed.");

        clinicalNotesPage.log("Verify click on cross button.");
        clinicalNotesPage.clickOnCrossBtn();

        Assert.assertTrue(clinicalNotesPage.verifySearchedNoteDisplayed(data.get("Title")),
                "After clicking on cross button search filter value should be reset." +
                        " And previous created notes should be displayed.");
    }

    @And("Verify Calendar filter option")
    public void verifyCalendarFilterOption() throws Exception {
        clinicalNotesPage.log("Click on Calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Current day option should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Current day"),
                "Current day option should be displayed.");

        clinicalNotesPage.log("Previous day option should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Previous day"),
                "Previous day option should be displayed.");

        clinicalNotesPage.log("Last 7 days option should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Last 7 days"),
                "Last 7 days option should be displayed.");

        clinicalNotesPage.log("Last 2 weeks option should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Last 2 weeks"),
                "Last 2 weeks option should be displayed.");

        String calenderMonth = LocalDateTime.now().getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()) + "  " +
                LocalDateTime.now().getYear();

        clinicalNotesPage.log("[" + calenderMonth + "] month and year value should be displayed on the top of calendar.");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed(calenderMonth),
                "[" + calenderMonth + "] month and year value should be displayed on the top of calendar.");

        log.info("Get Clinical notes.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails(false);

        clinicalNotesPage.log("Click on Current day option.");
        clinicalNotesPage.clickOnOption("Current day");

        log.info("Wait for it.");
        clinicalNotesPage._sleep(3);
        String title = details.get("Title");

        clinicalNotesPage.log("Note with title [" + title + "] should be displayed in current day search.");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note with title [" + title + "] should be displayed in current day search.");

        clinicalNotesPage.log("Click on Calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Click on Previous day option.");
        clinicalNotesPage.clickOnOption("Previous day");

        log.info("Wait for it.");
        clinicalNotesPage._sleep(3);

        clinicalNotesPage.log("Note with title [" + title + "] should not be displayed under past day search.");
        Assert.assertFalse(clinicalNotesPage.isNoteDisplayed(title),
                "Note with title [" + title + "] should not be displayed under past day search.");

        clinicalNotesPage.log("No clinical notes found message should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical notes found message should be displayed.");

        clinicalNotesPage.log("Click on Calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Click on Last 7 days option.");
        clinicalNotesPage.clickOnOption("Last 7 days");

        log.info("Wait for it.");
        clinicalNotesPage._sleep(3);

        clinicalNotesPage.log("Note with title [" + title + "] should be displayed in  Last 7 days search.");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note with title [" + title + "] should be displayed in  Last 7 days search.");

        clinicalNotesPage.log("Click on Calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Click on Last 2 weeks option.");
        clinicalNotesPage.clickOnOption("Last 2 weeks");

        log.info("Wait for it.");
        clinicalNotesPage._sleep(3);

        clinicalNotesPage.log("Note with title [" + title + "] should be displayed in  Last 2 weeks search.");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note with title [" + title + "] should be displayed in  Last 2 weeks search.");
    }

    @And("Verify Caregiver filter option")
    public void verifyCaregiverFilterOption() throws Exception {
        clinicalNotesPage.log("Click on clear all filter option.");
        clinicalNotesPage.clickOnClearAll();

        clinicalNotesPage.log("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("[Select care team members] message should be displayed under filter search.");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Select care team members"),
                "[Select care team members] message should be displayed under filter search.");

        clinicalNotesPage.log("Click on inner filter search.");
        clinicalNotesPage.clickOnFilterSearch();

        String clinician = testConfig.getClinicianName();

        clinicalNotesPage.log("Search clinician [" + clinician + "]");
        clinicalNotesPage.searchClinician(clinician);

        clinicalNotesPage.log("Select clinician [" + clinician + "]");
        clinicalNotesPage.selectClinician(clinician);

        clinicalNotesPage.log("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        clinicalNotesPage.log("Clinician [" + clinician + "] should be displayed in notes details.");
        Assert.assertTrue(clinicalNotesPage.isClinicianNameDisplayed(clinician),
                "Clinician [" + clinician + "] should be displayed in notes details.");

        clinicalNotesPage.log("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("Reset filter button should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isResetFilterDisplayed(),
                "Reset filter button should be displayed.");

        clinicalNotesPage.log("Click on Reset filter.");
        clinicalNotesPage.clickOnResetFilter();

        clinicalNotesPage.log("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("Click on filter search.");
        clinicalNotesPage.clickOnFilterSearch();

        clinician = testConfig.getNurseName();

        clinicalNotesPage.log("Search clinician [" + clinician + "]");
        clinicalNotesPage.searchClinician(clinician);

        clinicalNotesPage.log("Select clinician [" + clinician + "]");
        clinicalNotesPage.selectClinician(clinician);

        clinicalNotesPage.log("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        clinicalNotesPage.log("No clinical notes found message should be displayed because no note created yet with clinician [" + clinician + "].");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical notes found message should be displayed because no note created yet with clinician [" + clinician + "].");

        clinicalNotesPage.log("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("Reset filter button should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isResetFilterDisplayed(),
                "Reset filter button should be displayed.");

        clinicalNotesPage.log("Click on reset filter button.");
        clinicalNotesPage.clickOnResetFilter();

        clinicalNotesPage.log("Recently created notes should be displayed.");
        Assert.assertFalse(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "Recently created notes should be displayed.");
    }

    @And("Verify Calendar and Caregiver filter options together and Clear all")
    public void verifyCalendarAndCaregiverFilterOptionsTogether() throws Exception {
        clinicalNotesPage.log("Click on calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Get note details.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails(false);

        clinicalNotesPage.log("Click on Current day option.");
        clinicalNotesPage.clickOnOption("Current day");

        clinicalNotesPage.log("Select clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("Click on search under clinician filter.");
        clinicalNotesPage.clickOnFilterSearch();

        String clinician = testConfig.getClinicianName();

        clinicalNotesPage.log("Search clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.searchClinician(clinician);

        clinicalNotesPage.log("Select clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.selectClinician(clinician);

        clinicalNotesPage.log("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        String title = details.get("Title");

        clinicalNotesPage.log("Clinical note [" + title + "] should be displayed while filters selected are date as Current day and clinician as [" + clinician + "].");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Clinical note [" + title + "] should be displayed while filters selected are date as Current day and clinician as [" + clinician + "].");

        clinicalNotesPage.log("Note [" + title + "] should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note [" + title + "] should be displayed.");

        clinicalNotesPage.log("Click on [Clear all] option.");
        clinicalNotesPage.clickOnClearAll();

        clinicalNotesPage.log("Click on calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Click on Previous day option.");
        clinicalNotesPage.clickOnOption("Previous day");

        clinicalNotesPage.log("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("Click on search under clinician filter.");
        clinicalNotesPage.clickOnFilterSearch();

        clinicalNotesPage.log("Search clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.searchClinician(clinician);

        clinicalNotesPage.log("Select clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.selectClinician(clinician);

        clinicalNotesPage.log("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        clinicalNotesPage.log("No clinical note should be displayed while filters selected are date as Previous day and clinician as [" + clinician + "].");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical note should be displayed while filters selected are date as Previous day and clinician as [" + clinician + "].");

        clinicalNotesPage.log("Click on clear all.");
        clinicalNotesPage.clickOnClearAll();

        clinicalNotesPage.log("Clinical notes list should be displayed after clear all filters.");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(details.get("Title")),
                "Clinical notes list should be displayed after clear all filters.");

        clinicalNotesPage.log("Click on calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Click on Current day filter.");
        clinicalNotesPage.clickOnOption("Current day");

        clinicalNotesPage.log("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("Click on search under clinician filter.");
        clinicalNotesPage.clickOnFilterSearch();

        clinician = testConfig.getNurseName();

        clinicalNotesPage.log("Search clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.searchClinician(clinician);

        clinicalNotesPage.log("Select clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.selectClinician(clinician);

        clinicalNotesPage.log("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        clinicalNotesPage.log("No clinical note should be displayed while filters selected are date as current day and clinician as [" + clinician + "].");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical note should be displayed while filters selected are date as current day and clinician as [" + clinician + "].");

        clinicalNotesPage.log("Click on clear all.");
        clinicalNotesPage.clickOnClearAll();

        clinicalNotesPage.log("Clinical notes list should be displayed after clear all filters.");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(details.get("Title")),
                "Clinical notes list should be displayed after clear all filters.");
    }

    @And("Verify Calendar and Caregiver filter options together")
    public void verifyCalendarAndCaregiverFilterTogether() throws Exception {
        clinicalNotesPage.log("Click on calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Get note details.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails(false);

        clinicalNotesPage.log("Click on Current day option.");
        clinicalNotesPage.clickOnOption("Current day");

        clinicalNotesPage.log("Select clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("Click on search under clinician filter.");
        clinicalNotesPage.clickOnFilterSearch();

        String clinician = testConfig.getClinicianName();

        clinicalNotesPage.log("Search clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.searchClinician(clinician);

        clinicalNotesPage.log("Select clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.selectClinician(clinician);

        clinicalNotesPage.log("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        String title = details.get("Title");

        clinicalNotesPage.log("Clinical note [" + title + "] should be displayed while filters selected are date as Current day and clinician as [" + clinician + "].");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Clinical note [" + title + "] should be displayed while filters selected are date as Current day and clinician as [" + clinician + "].");

        clinicalNotesPage.log("Note [" + title + "] should be displayed.");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note [" + title + "] should be displayed.");

        clinicalNotesPage.log("Click on calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        clinicalNotesPage.log("Click on Previous day option.");
        clinicalNotesPage.clickOnOption("Previous day");

        clinicalNotesPage.log("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        clinicalNotesPage.log("Click on search under clinician filter.");
        clinicalNotesPage.clickOnFilterSearch();

        clinicalNotesPage.log("Search clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.searchClinician(clinician);

        clinicalNotesPage.log("Select clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.selectClinician(clinician);

        clinicalNotesPage.log("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        clinicalNotesPage.log("No clinical note should be displayed while filters selected are date as Previous day and clinician as [" + clinician + "].");
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical note should be displayed while filters selected are date as Previous day and clinician as [" + clinician + "].");
    }

    @And("Verify edit and delete option not visible")
    public void verifyEditAndDeleteOptionNotVisible() throws Exception {
        clinicalNotesPage.log("Click on Clinical notes.");
        clinicalNotesPage.clickOnClinicalNotes();

        searchForNote();

        clinicalNotesPage.log("Edit icon should not be displayed.");
        Assert.assertFalse(clinicalNotesPage.verifyEditIconDisplayed(),
                "Edit icon should not be displayed.");

        clinicalNotesPage.log("Delete icon should not be displayed.");
        Assert.assertFalse(clinicalNotesPage.verifyDeleteIconDisplayed(),
                "Delete icon should not be displayed.");
    }

    @Then("verify clinical notes page")
    public void verifyClinicalNotesPageInHCP() throws Exception {
        clinicalNotesPageApp = new ClinicalNotesPageApp(clinician);

        Assert.assertTrue(clinicalNotesPageApp.verifyClinicalPagNavBar(testConfig.getPrimaryPatientName()));

        clinicalNotesPageApp.navigateBackToPatient();

        Assert.assertTrue(clinicalNotesPageApp.verifyNoClinicalNotesYetMessage("No clinical notes yet"));

        clinicalNotesPageApp.navigateBackToPatient();
    }

    @Then("verify auto refresh clinical note screen")
    public void verifyAutoRefreshClinicalNoteScreen() {
        clinicalNotesPageApp = new ClinicalNotesPageApp(clinician);
        Map<String, String> noteDetails = customUtils.getDefaultClinicalNotesDetails(false);

        log.info("Wait for page auto refresh.");
        clinicalNotesPageApp.waitForPageAutoRefresh();

        clinicalNotesPage.log("Verify clinical note present with title [" + clinicalNotesPageApp.isNoteDetailsDisplayed(noteDetails.get("Title") + "]"));
        Assert.assertTrue(clinicalNotesPageApp.isNoteDetailsDisplayed(noteDetails.get("Title")),
                "Clinical note not found after waiting 20 seconds of auto refresh in HCP app, while create from hco web. " +
                        "Note title [" + clinicalNotesPageApp.isNoteDetailsDisplayed(noteDetails.get("Title")) + "]");
    }

    @Then("Verify deleted note not displayed in HCP App")
    public void verifyDeletedNoteNotDisplayedInHCPApp() {
        clinicalNotesPageApp = new ClinicalNotesPageApp(clinician);
        log.info("Get Default Clinical Notes.");
        Map<String, String> note = customUtils.getDefaultClinicalNotesDetails(false);
        clinicalNotesPage.log(note);
        String expected = note.get("Title");
        Assert.assertTrue(clinicalNotesPageApp.verifyNoteDeleted(expected),
                "Task [" + expected + "] should not be displayed after delete.");
    }

    @Then("^Verify (added|updated) note displayed in HCP App$")
    public void verifyAddedNoteDisplayedInHCPApp(String arg0) throws Exception {
        log.info("Get Default Clinical Notes.");
        Map<String, String> note = customUtils.getDefaultClinicalNotesDetails(false);
        clinicalNotesPage.log(note);
        clinicalNotesPageApp = new ClinicalNotesPageApp(clinician);

        String expected = note.get("Title");
        Assert.assertTrue(clinicalNotesPageApp.verifyNoteTitleDisplayed(expected),
                "Title [" + expected + "] not found");
        expected = note.get("Description");
        Assert.assertTrue(clinicalNotesPageApp.verifyNoteDescriptionDisplayed(expected),
                "Desc [" + expected + "] not found");

        //XCUIElementTypeStaticText[@name="Clinical notes (1) "]
//XCUIElementTypeStaticText[@name="asdf"]
//XCUIElementTypeStaticText[@name="Jan 21,2022 at 9:20 PM"]
//XCUIElementTypeStaticText[@name="Physician Automation"]
//XCUIElementTypeStaticText[@name="Physician"]
    }
}
