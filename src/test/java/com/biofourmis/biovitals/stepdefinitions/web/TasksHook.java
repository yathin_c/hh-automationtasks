package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.TasksPageApp;
import com.biofourmis.biovitals.pagefactory.web.TasksPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

@Log4j
public class TasksHook {
    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired
    private Faker faker;
    @Autowired
    private TestConfig testConfig;

    private TasksPage tasks;
    private TasksPageApp taskPageApp;
    private String openTaskValue;
    private String patientMrn;
    private Map<String, String> details = new LinkedHashMap<>();
    private Map<String, String> updatedDetails = new LinkedHashMap<>();
    private CustomUtils customUtils = new CustomUtils(null);

    @Given("^Verify create new task with (Required|All) fields$")
    public void verify_create_new_task_with_fields(String string) throws Exception {
        tasks = new TasksPage(web);
        tasks.log("Click on Task icon.");
        tasks.clickOnTaskIcon();
        boolean isAll = string.equals("All");
        tasks.log("Click on New Task.");
        tasks.clickOnNewTask();

        Map<String, String> taskValue = getTaskValue(isAll);
        tasks.log("Input task description as [" + taskValue.get("Description") + "].");
        tasks.inputTaskDescription(taskValue.get("Description"));

        if (isAll) {
            tasks.log("Input optional fields. [" + taskValue + "]");
            details = tasks.inputOptionalFields(taskValue);
        }
        tasks.log("Click on Create task.");
        tasks.createTask();

        tasks.log("Search and click on created task. [" + details.get("Description") + "]");
        tasks.viewTask(details.get("Description"));

        tasks.log("Verify task created successfully. Task description [" + taskValue.get("Description") + "].");
        tasks.verifyCreatedTaskSuccessfully(taskValue.get("Description"));
        if (isAll) {
            tasks.log("Verify optional fields.");
            tasks.verifyOptionalFieldsValue(taskValue);
        }
    }

    private void verifyCreatedTask(boolean isAll) throws Exception {
        tasks = new TasksPage(web);
        tasks.log("Search and click on created task. [" + details.get("Description") + "]");
        tasks.viewTask(details.get("Description"));

        tasks.log("Verify task created successfully.");
        tasks.verifyCreatedTaskSuccessfully(details.get("Description"));
        if (isAll) {
            tasks.log("Verify optional fields.");
            tasks.verifyOptionalFieldsValue(details);
        }
    }

    private Map<String, String> getTaskValue(boolean isAll) {
        if (!details.isEmpty())
            return details;

        details.put("Description", faker.aviation().aircraft() + LocalDateTime.now());
        details.put("AssignedBy", testConfig.getClinicianName());
        details.put("AssignedOn", "");
        details.put("AssignedTo", "--");
        details.put("DueDate", "--");
        if (isAll)
            details.put("type", "all");
        else
            details.put("type", "required");
        return details;
    }

    private Map<String, String> updateTaskValue() {
        if (details.isEmpty())
            throw new RuntimeException("No Task Value found.");

        updatedDetails.put("Description", faker.aviation().aircraft() + LocalDateTime.now());
        updatedDetails.put("AssignedBy", details.get("AssignedBy"));
        updatedDetails.put("AssignedOn", details.get("AssignedOn"));

        updatedDetails.put("AssignedTo", "--");
        updatedDetails.put("DueDate", "--");

        updatedDetails.put("LastUpdatedBy", "");
        updatedDetails.put("LastUpdatedOn", "");

        return updatedDetails;
    }

    @Then("Verify comment on created task")
    public void verifyCommentOnCreatedTask() throws Exception {
        String desc = details.get("Description");
        tasks.log("View task [" + desc + "].");
        tasks.viewTask(desc);
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        tasks.log("Add comment [" + comment + "].");
        tasks.addComment(comment);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        Assert.assertTrue(tasks.verifyCloseIcon(),
                "Task not closed after clicking on close icon.");

        tasks.log("Click on task icon.");
        tasks.clickOnTaskIcon();
        tasks.log("View task [" + desc + "].");
        tasks.viewTask(desc);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");
    }

    @And("^Verify edit a task with (Required|All) fields$")
    public void verifyEditATaskWithFields(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        tasks.log("Click on edit task.");
        tasks.clickOnEditTask();

        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        tasks.log("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            tasks.log("Update all input fields as [" + details + "].");
            details = tasks.inputOptionalFields(details);
        }

        tasks.log("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Task with updates description npt found. Description [" + desc + "].");

        tasks.log("Click on Back option.");
        tasks.VerifyClickOnBack();

        tasks.log("View task [" + desc + "].");
        tasks.viewTask(details.get("Description"));

        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Task optional fields not matched after updating. Expected [" + details + "].");
    }

    @Then("Verify complete a task")
    public void verifyCompleteATask() throws Exception {
        tasks = new TasksPage(web);
        tasks.log("Click on [Complete] a task.");
        tasks.completeTask();

        tasks.log("Add Completed details.");
        details.put("CompletedOn", getAssignedOn());
        details.put("CompletedBy", testConfig.getClinicianName());

        tasks.log("Open Completed task option.");
        tasks.clickOnCompletedTasks();

        tasks.log("verify task [" + details.get("Description") + "] should be present in completed section.");
        tasks.viewTask(details.get("Description"));
    }

    @And("Verify comment on completed task")
    public void verifyCommentOnCompletedTask() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        tasks.log("Add comment in task.");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Verify comment added successfully. Expected [" + comment + "].");

        tasks.log("Click on close icon.");
        tasks.verifyCloseIcon();
        tasks.log("Click on task icon.");
        tasks.clickOnTaskIcon();
        tasks.log("Open Completed task option.");
        tasks.clickOnCompletedTasks();
        tasks.log("Search task. Description [" + details.get("Description") + "].");
        tasks.viewTask(details.get("Description"));
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment on task is [" + comment + "].");
    }

    @Then("Verify re-open completed task")
    public void verifyReOpenCompletedTask() throws Exception {
        tasks = new TasksPage(web);

        String desc = details.get("Description");
        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        tasks.log("Click on reopen task.");
        tasks.clickOnReOpen();

        tasks.log("Verify task re-opened successfully.");
        Assert.assertTrue(tasks.verifyTaskReopened(),
                "Not able to reopen completed task. Task description [" + desc + "].");

        tasks.log("Click on close icon.");
        tasks.verifyCloseIcon();

        tasks.log("Click on task icon.");
        tasks.clickOnTaskIcon();

        tasks.log("Search and verify task displayed in open task section. Task description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify comment on re-opened task")
    public void verifyCommentOnReOpenedTask() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        String desc = details.get("Description");
        tasks.log("Add comment in task. [" + comment + "].");
        tasks.addComment(comment);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Expected comment is [" + comment + "].");

        tasks.log("Click on close icon.");
        tasks.verifyCloseIcon();

        tasks.log("Click on task icon.");
        tasks.clickOnTaskIcon();

        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Expected comment is [" + comment + "].");
    }

    @Then("^Verify edit re-open task with (Required|All) fields$")
    public void verifyEditReOpenTaskWithFields(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        tasks.log("Click on reopen task.");
        tasks.clickOnEditTask();
        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        tasks.log("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            tasks.log("Update optional fields.");
            details = tasks.inputOptionalFields(details);
        }

        tasks.log("Click on info.");
        tasks.clickOnUpdate();
        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Task with description [" + desc + "] not found.");
        tasks.log("Click on back.");
        tasks.VerifyClickOnBack();
        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Option fields updated value not matched. [" + details + "].");
    }

    @Then("Verify complete a re-open task")
    public void verifyCompleteAReOpenTask() throws Exception {
        String desc = details.get("Description");
        tasks.log("Click on complete task.");
        tasks.completeTask();

        tasks.log("Open complete task(s) option.");
        tasks.clickOnCompletedTasks();
        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify closing task container")
    public void verifyClosingTaskContainer() throws Exception {
        tasks = new TasksPage(web);

        tasks.log("Click on Task icon.");
        tasks.clickOnTaskIcon();
        Assert.assertTrue(tasks.verifyCloseIcon(),
                "Not able to close Task container after click on close icon..");

        tasks.log("Click on Task icon.");
        tasks.clickOnTaskIcon();
        String description = faker.aquaTeenHungerForce() + LocalDateTime.now().toString();

        tasks.log("Click on new Task");
        tasks.clickOnNewTask();

        tasks.log("Enter task description.");
        tasks.inputTaskDescription(description);

        Assert.assertTrue(tasks.verifyCloseIcon(description),
                "Task should not be created without after click ob cancel icon.");
    }

    @Then("Verify Cancel button functionality")
    public void verifyCancelButtonFunctionality() throws Exception {
        tasks.log("Click on Task icon.");
        tasks.clickOnTaskIcon();
        String description = faker.aquaTeenHungerForce() + LocalDateTime.now().toString();
        tasks.log("Click on New Task option.");
        tasks.clickOnNewTask();
        tasks.log("Enter task description. [" + description + "]");
        tasks.inputTaskDescription(description);
        tasks.log("Verify after canceling the task. Task should not be created.");
        Assert.assertFalse(tasks.verifyCancelButton(description),
                "Task created with description [" + description + "] by just input description and click on cancel.");
    }

    @And("Verify edit a task and remove optional fields")
    public void verifyEditATaskAndRemoveOptionalFields() throws Exception {
        tasks.log("Click on Edit.");
        tasks.clickOnEditTask();
        tasks.log("Remove optional fields values.");
        tasks.removeOptionalFields();
        tasks.log("Click on update.");
        tasks.clickOnUpdate();
        //TODO Verify removed tasks once test case added in Zephyr
    }

    @And("^Verify create new task with (Required|All) fields on tasks tab$")
    public void verifyCreateNewTaskWithRequiredFieldsOnTaskTab(String arg0) throws Exception {
        tasks = new TasksPage(web);
        boolean isAll = arg0.equals("All");

        tasks.log("Scroll to patient.");
        tasks.searchForPatientInTasks(customUtils.getDefaultPatientDetails().get("MRN"));

        tasks.log("Click on new task.");
        tasks.clickOnNewTaskOnTasks(customUtils.getDefaultPatientDetails().get("MRN"));

        Map<String, String> taskValue = getTaskValue(isAll);
        String desc = taskValue.get("Description");
        tasks.log("Enter task description.");
        tasks.inputTaskDescription(desc);

        if (isAll) {
            tasks.log("Input optional fields. Values [" + taskValue + "]");
            details = tasks.inputOptionalFields(taskValue);
        }

        tasks.log("Click on Create.");
        tasks.createTask();
        desc = details.get("Description");
        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyCreatedTaskSuccessfully(desc),
                "Task not created with details [" + details + "].");
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(taskValue),
                    "Task details not matched. Values [" + details + "]");
    }

    @Then("Verify comment on created task on tasks tab")
    public void verifyCommentOnCreatedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        tasks.log("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");

        tasks.log("Refresh page.");
        tasks._refreshPage();

        String desc = details.get("Description");
        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @And("Verify comment on completed task on tasks tab")
    public void verifyCommentOnCompletedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        tasks.log("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
        tasks.log("Refresh page.");
        tasks._refreshPage();
        tasks.log("Click on complete task.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        String desc = details.get("Description");
        tasks.log("Search task. Description [" + desc + "].");
        tasks.searchForPatientInTasks(customUtils.getDefaultPatientDetails().get("MRN"));
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @Then("Verify re-open completed task on tasks tab")
    public void verifyReOpenCompletedTasksTab() throws Exception {
        String desc = details.get("Description");

        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        tasks.log("Click on reopen.");
        tasks.clickOnReOpen();

        Assert.assertTrue(tasks.verifyTaskReopened(),
                "Task with description [" + desc + "] not re-open.");

        tasks.log("Refresh page.");
        tasks._refreshPage();

        tasks.log("Verify task should be displayed under open tasks. Task description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify comment on re-opened task on tasks tab")
    public void verifyCommentOnReOpenedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        tasks.log("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");

        tasks.log("Refresh page.");
        tasks._refreshPage();

        String desc = details.get("Description");
        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @And("^Verify edit a task with (Required|All) fields on tasks tab$")
    public void verifyEditATaskWithRequiredFieldsOnTasksTab(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        tasks.log("Click on edit.");
        tasks.clickOnEditTask();

        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        tasks.log("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            tasks.log("Update optional fields..");
            details = tasks.inputOptionalFields(details);
            tasks.log("Optional fields entered as [" + details + "].");
        }

        tasks.log("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Updated task description not found as [" + desc + "].");

        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Optional fields value not matched. Values [" + details + "].");
    }

    @Then("^Verify edit re-open task with (Required|All) fields on tasks tab$")
    public void verifyEditReOpenTaskWithFieldsTab(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        tasks.log("Click on edit.");
        tasks.clickOnEditTask();
        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());

        String desc = details.get("Description");
        tasks.log("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            tasks.log("Update optional fields..");
            details = tasks.inputOptionalFields(details);
            tasks.log("Optional fields entered as [" + details + "].");
        }

        tasks.log("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Updated task description not found as [" + desc + "].");

        tasks.log("Click on close icon.");
        tasks.verifyCloseIcon();

        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Optional fields value not matched. Values [" + details + "].");
    }

    @Then("Verify complete a task on task tab")
    public void verifyCompleteATaskOnTaskTab() throws Exception {
        tasks.log("Complete a task.");
        tasks.completeTask();

        tasks.log("Open Completed task option.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        tasks.log("verify task should be present in completed section.");
        tasks.viewTask(details.get("Description"));
    }

    @Then("Verify complete a re-open task on task tab")
    public void verifyCompleteAReOpenTaskOnTaskTab() throws Exception {
        String desc = details.get("Description");
        tasks.log("Click on complete task.");
        tasks.completeTask();

        tasks.log("Open complete task(s) option.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        tasks.log("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @When("^Add new task with (required|all) fields$")
    public void addNewTaskWithRequiredFields(String arg0) throws Exception {
        taskPageApp = new TasksPageApp(clinician);

        Map<String, String> taskDetails = getTaskValue(false);

        String name = testConfig.getPrimaryPatientName();
        String[] sNameArr = name.split(" ");
        String sName = "";
        for (String val : sNameArr)
            sName += val.charAt(0);
        sName = sName.toUpperCase(Locale.ROOT);

        tasks.log("Click on add new task.");
        taskPageApp.clickOnAddNewTaskIcon(sName);

        tasks.log("Enter Task description.");
        taskPageApp.addTaskDescription(taskDetails.get("Description"));


        if (arg0.equalsIgnoreCase("all")) {
            tasks.log("Select Due date and time");

            tasks.log("Select assignee as " + testConfig.getClinicianName());
        }

        taskPageApp.clickOnCreate();
    }

    @Then("verify created task in HCP web")
    public void verifyCreatedTaskInHCPWeb() throws Exception {
        verifyCreatedTask(false);
    }

    /**
     *
     */

    @Then("^verify (Active|Archived) task tab$")
    public void verifyActiveTaskTab(String arg0) throws Exception {
        TasksPage webPage = new TasksPage(web);
        if (arg0.equalsIgnoreCase("Active"))
            verifyActiveTab(webPage);
        else
            verifyArchivedTab(webPage);
    }

    private void verifyArchivedTab(TasksPage tasksPage) throws Exception {
        tasksPage.log("Verify [Archived] tab displayed.");
        Assert.assertTrue(tasksPage.isTabDisplayed("Archived"),
                "Archived tab not displayed.");

        tasksPage.log("Click on [Archived] tab.");
        tasksPage.clickOnTab("Archived");

        tasksPage.log("Verify [Archived] tab displayed.");
        Assert.assertTrue(tasksPage.isTabDisplayed("Archived"),
                "Archived tab not displayed.");

        tasksPage.log("Verify [Archived] task is selected.");
        Assert.assertTrue(tasksPage.isTabSelected("Archived"),
                "Archived tab not selected under Tasks.");

        tasksPage.log("Verify [Active] task should not be selected after clicked on [Archived].");
        Assert.assertFalse(tasksPage.isTabSelected("Active"),
                "Active tab should not be selected under Tasks when Archived is selected.");
    }

    private void verifyActiveTab(TasksPage tasksPage) {
        tasksPage.log("Verify [Active] tab displayed.");
        Assert.assertTrue(tasksPage.isTabDisplayed("Active"),
                "Active tab not displayed.");

        tasksPage.log("Verify [Active] task is selected by default.");
        Assert.assertTrue(tasksPage.isTabSelected("Active"),
                "Active tab not selected under Tasks.");

        tasksPage.log("Verify [Archived] task not selected by default.");
        Assert.assertFalse(tasksPage.isTabSelected("Archived"),
                "Archived tab should not be selected under Tasks when Active is selected.");
    }

    @Then("verify Task page navigated successfully")
    public void verifyTaskPageNavigatedSuccessfully() {
        TasksPage webPage = new TasksPage(web);
        webPage.log("Verify Task page displayed successfully.");
        Assert.assertTrue(webPage.isTaskPageDisplayed(), "Task page not displayed.");
    }

    @Then("^verify create new task under (Tasks|Patient) Page with (Required|All) fields in HCP web$")
    public void verifyCreateNewTaskUnderTasksPageWithRequiredFieldsInHCPWeb(String arg0, String arg1) throws Exception {
        TasksPage webPage = new TasksPage(web);
        if (arg0.equalsIgnoreCase("Patient")) {
            webPage.log("Click on Task icon.");
            webPage.clickOnTaskIcon();

            webPage.log("Click on New Task.");
            webPage.clickOnNewTask();
        } else {
            webPage.log("Search for patient. MRN [" + testConfig.getPrimaryPatientMRN() + "].");
            webPage.searchForPatientInTasks(testConfig.getPrimaryPatientMRN());

            webPage.log("Click on New Task.");
            webPage.clickOnNewTaskOnTasks(testConfig.getPrimaryPatientMRN());
        }

        if (arg1.equalsIgnoreCase("Required"))
            createTaskWithRequired(webPage);
        else
            createTaskWithAll(webPage);
    }

    private void createTaskWithAll(TasksPage tasksPage) throws Exception {
        tasksPage.log("Create task details.");
        getTaskValue(true);

        tasksPage.log("Create task with all fields");
        tasksPage.log("Input task description as [" + details.get("Description") + "].");
        tasksPage.inputTaskDescription(details.get("Description"));

        tasksPage.log("Select due date time.");
        String due = tasksPage.setDueDate();
        tasksPage.log("Due date time selected as [" + due + "].");

        tasksPage.log("Select Assigned To . [" + testConfig.getNurseName() + "]");
        tasksPage.selectAssigneeTo(testConfig.getNurseName());

        tasksPage.log("Click on Create task.");
        tasksPage.createTask();

        details.put("AssignedOn", getAssignedOn());
        details.put("DueDate", due);
        details.put("AssignedTo", testConfig.getNurseName());

        tasksPage.log("Search and click on created task. [" + details.get("Description") + "]");
        tasksPage.viewTask(details.get("Description"));

        tasksPage.log("Verify task created successfully. Task description [" + details.get("Description") + "].");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        tasksPage.log("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        tasksPage.log("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        tasksPage.log("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        tasksPage.log("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");
    }

    private String getAssignedOn() {
        String pattern = "hh:mm a";
        DateFormat dateFormat = new SimpleDateFormat(pattern);

        LocalTime now = LocalTime.now();
        String time = now.format(DateTimeFormatter.ofPattern(pattern)).strip();

        if (time.startsWith("0"))
            return time.substring(1, time.length());
        else
            return time;
    }

    private void createTaskWithRequired(TasksPage tasksPage) throws Exception {
        tasksPage.log("Create task details.");
        getTaskValue(false);

        //tasksPage.log("Create task with required fields");

        tasksPage.log("Input task description. [" + details.get("Description") + "].");
        tasksPage.inputTaskDescription(details.get("Description"));

        tasksPage.log("Click on Create task.");
        tasksPage.createTask();

        details.put("AssignedOn", getAssignedOn());

        tasksPage.log("Search and click on created task. [" + details.get("Description") + "]");
        tasksPage.viewTask(details.get("Description"));

        tasksPage.log("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        tasksPage.log("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        tasksPage.log("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        tasksPage.log("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        tasksPage.log("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");
    }

    @And("^verify task comment on (Patient|Task) page$")
    public void verifyTaskCommentOnPatientPage(String arg0) throws Exception {
        TasksPage webPage = new TasksPage(web);
        if (arg0.equalsIgnoreCase("Task")) {
            webPage.log("Verify comment on Tasks page.");
            verifyCommentOnTasksSec(webPage);
        } else {
            webPage.log("Verify comment on Patient details page.");
            verifyCommentOnPatientSec(webPage);
        }
    }

    private void verifyCommentOnPatientSec(TasksPage taskPage) throws Exception {
        String desc = details.get("Description");
        taskPage.log("View task [" + desc + "].");
        taskPage.viewTask(desc);
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        taskPage.log("Verify comment box label.");
        Assert.assertTrue(taskPage.verifyCommentLabel(), "Comment place holder not displayed. [Leave a comment]");

        taskPage.log("Add comment [" + comment + "].");
        taskPage.addComment(comment);

        String commentData = testConfig.getClinicianName() + ", Today, " + getAssignedOn();
        taskPage.log("Verify commented successfully. Comment details [" + comment + "].");
        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        taskPage.log("Click on close icon.");
        Assert.assertTrue(taskPage.verifyCloseIcon(),
                "Task not closed after clicking on close icon.");

        taskPage.log("Click on task icon.");
        taskPage.clickOnTaskIcon();
        taskPage.log("View task [" + desc + "].");
        taskPage.viewTask(desc);
        taskPage.log("Verify commented message [" + comment + "] should be displayed.");
        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        taskPage.log("Verify Commented By and comment posted date time. [" + commentData + "]");
        Assert.assertTrue(taskPage.verifyCommentDetails(comment, commentData),
                "Comment details not matched. Expected details [" + commentData + "]");
    }

    private void verifyCommentOnTasksSec(TasksPage taskPage) throws Exception {
        String desc = details.get("Description");
        taskPage.log("View task [" + desc + "].");
        taskPage.viewTask(desc);
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        taskPage.log("Verify comment box label.");
        Assert.assertTrue(taskPage.verifyCommentLabel(), "Comment place holder not displayed. [Leave a comment]");

        taskPage.log("Add comment [" + comment + "].");
        taskPage.addComment(comment);

        String commentData = testConfig.getClinicianName() + ", Today, " + getAssignedOn();
        taskPage.log("Verify commented successfully. Comment details [" + comment + "].");
        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        Assert.assertTrue(taskPage.verifyCloseIcon(),
                "Task not closed after clicking on close icon.");

        taskPage.log("View task [" + desc + "].");
        taskPage.viewTask(desc);
        taskPage.log("Verify commented message [" + comment + "] should be displayed.");
        Assert.assertTrue(taskPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        taskPage.log("Verify Commented By and comment posted date time. [" + commentData + "]");
        Assert.assertTrue(taskPage.verifyCommentDetails(comment, commentData),
                "Comment details not matched. Expected details [" + commentData + "]");
    }

    @Then("^verify edit task with (Required|All) fields$")
    public void verifyEditTaskWithRequiredFields(String arg0) throws Exception {
        TasksPage webPage = new TasksPage(web);

        webPage.log("Verify Edit button displayed.");
        Assert.assertTrue(webPage.isEditButtonDisplayed(), "Edit button not displayed on task tab.");

        webPage.log("Verify Edit icon displayed.");
        Assert.assertTrue(webPage.isEditIconDisplayed(), "Edit icon not displayed on task tab.");

        webPage.log("Click on Edit task button.");
        webPage.clickOnEditTask();

        webPage.log("Update task details.");
        updateTaskValue();

        if (arg0.equalsIgnoreCase("All"))
            editTaskAllFields(webPage);
        else
            editRequiredFields(webPage, true);
    }

    private void editTaskAllFields(TasksPage tasksPage) throws Exception {
        tasksPage.log("Update task description. Updated description should be [" + updatedDetails.get("Description") + "].");
        tasksPage.updateDescription(updatedDetails.get("Description"));

        tasksPage.log("Update task due date time.");
        tasksPage.updateDueDate();

        tasksPage.log("Update Assign To. [" + testConfig.getClinicianName() + "].");
        tasksPage.selectAssigneeTo(testConfig.getClinicianName());

        tasksPage.log("Click on Cancel.");
        tasksPage.clickOnCancel();

        /**
         * VERIFY NO CHANGES UPDATE
         */
        tasksPage.log("Search and click on created task. [" + details.get("Description") + "]");
        tasksPage.viewTask(details.get("Description"));

        tasksPage.log("Verify task description should not changed.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        tasksPage.log("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        tasksPage.log("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        tasksPage.log("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        tasksPage.log("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        /**
         * Enter details again
         */

        tasksPage.log("Click on edit button.");
        tasksPage.clickOnEditTask();

        tasksPage.log("Update task description. Updated task description [" + updatedDetails.get("Description") + "].");
        tasksPage.updateDescription(updatedDetails.get("Description"));

        tasksPage.log("Update task due date time.");
        String dueDate = tasksPage.updateDueDate();

        tasksPage.log("Update Assign To.");
        tasksPage.selectAssigneeTo(testConfig.getClinicianName());

        tasksPage.log("Click on Update.");
        tasksPage.clickOnUpdate();

        updatedDetails.put("DueDate", dueDate);
        updatedDetails.put("AssignedTo", testConfig.getClinicianName());
        updatedDetails.put("UpdatedBy", testConfig.getClinicianName());
        updatedDetails.put("UpdatedOn", getAssignedOn());

        tasksPage.log("Update Previous task details into dic.");
        details.putAll(updatedDetails);

        /**
         * VERIFY NO CHANGES UPDATE
         */
        tasksPage.log("Search and click on created task. [" + updatedDetails.get("Description") + "]");
        tasksPage.viewTask(updatedDetails.get("Description"));

        tasksPage.log("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        tasksPage.log("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        tasksPage.log("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        tasksPage.log("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        tasksPage.log("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        tasksPage.log("Verify task Updated By [" + details.get("UpdatedBy") + "].");
        Assert.assertTrue(tasksPage.verifyUpdatedBy(details.get("Description"), details.get("UpdatedBy")),
                "Updated By value not matched. Expected [" + details.get("UpdatedBy") + "]");

        tasksPage.log("Verify task Updated on [" + details.get("UpdatedOn") + "].");
        Assert.assertTrue(tasksPage.verifyUpdatedOn(details.get("Description"), details.get("UpdatedOn")),
                "Updated On  value not matched. Expected [" + details.get("UpdatedOn") + "]");

        /**
         * TILL HERE
         */

    }

    private void editRequiredFields(TasksPage tasksPage, boolean removeOptional) throws Exception {

        tasksPage.log("Update task description.");
        tasksPage.updateDescription(updatedDetails.get("Description"));

        tasksPage.log("Remove task due date time.");
        tasksPage.removeDueDate();

        tasksPage.log("Remove task Assign To.");
        tasksPage.removeAssignedTo();

        tasksPage.log("Click on Cancel.");
        tasksPage.clickOnCancel();

        /**
         * VERIFY NO CHANGES UPDATE
         */
        tasksPage.log("Search and click on created task. [" + details.get("Description") + "]");
        tasksPage.viewTask(details.get("Description"));

        tasksPage.log("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        tasksPage.log("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        tasksPage.log("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        tasksPage.log("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        tasksPage.log("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        /**
         * Enter details again
         */

        tasksPage.log("Click on edit button.");
        tasksPage.clickOnEditTask();

        tasksPage.log("Update task description.");
        tasksPage.updateDescription(updatedDetails.get("Description"));

        tasksPage.log("Remove task due date time.");
        tasksPage.removeDueDate();

        tasksPage.log("Remove task Assign To.");
        tasksPage.removeAssignedTo();

        tasksPage.log("Click on Update.");
        tasksPage.clickOnUpdate();

        updatedDetails.put("DueDate", "--");
        updatedDetails.put("AssignedTo", "--");
        updatedDetails.put("UpdatedBy", testConfig.getClinicianName());
        updatedDetails.put("UpdatedOn", getAssignedOn());

        tasksPage.log("Update Previous task details into dic.");
        details.putAll(updatedDetails);

        /**
         * VERIFY NO CHANGES UPDATE
         */
        tasksPage.log("Search and click on created task. [" + updatedDetails.get("Description") + "]");
        tasksPage.viewTask(updatedDetails.get("Description"));

        tasksPage.log("Verify task created successfully.");
        tasksPage.verifyCreatedTaskSuccessfully(details.get("Description"));

        tasksPage.log("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        tasksPage.log("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        tasksPage.log("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        tasksPage.log("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        tasksPage.log("Verify task Updated By [" + details.get("UpdatedBy") + "].");
        Assert.assertTrue(tasksPage.verifyUpdatedBy(details.get("Description"), details.get("UpdatedBy")),
                "Updated By value not matched. Expected [" + details.get("UpdatedBy") + "]");

        tasksPage.log("Verify task Updated on [" + details.get("UpdatedOn") + "].");
        Assert.assertTrue(tasksPage.verifyUpdatedOn(details.get("Description"), details.get("UpdatedOn")),
                "Updated On  value not matched. Expected [" + details.get("UpdatedOn") + "]");

        /**
         * TILL HERE
         */

    }

    @Then("verify completed task section displayed correctly")
    public void verifyCompletedTaskSectionDisplayedCorrectly() throws Exception {
        TasksPage tasksPage = new TasksPage(web);
        tasksPage.log("Verify completed task page.");

        tasks.log("Verify task Assigned By [" + details.get("AssignedBy") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedBy(details.get("Description"), details.get("AssignedBy")),
                "Assigned By value not matched. Expected [" + details.get("AssignedBy") + "]");

        tasks.log("Verify task Assigned On [" + details.get("AssignedOn") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedOn(details.get("Description"), details.get("AssignedOn")),
                "Assigned On value not matched. Expected [" + details.get("AssignedOn") + "]");

        tasks.log("Verify task Assigned To [" + details.get("AssignedTo") + "].");
        Assert.assertTrue(tasksPage.verifyAssignedTo(details.get("Description"), details.get("AssignedTo")),
                "Assigned To value not matched. Expected [" + details.get("AssignedTo") + "]");

        tasks.log("Verify task Due Date [" + details.get("DueDate") + "].");
        Assert.assertTrue(tasksPage.verifyDueDate(details.get("Description"), details.get("DueDate")),
                "Due Date value not matched. Expected [" + details.get("DueDate") + "]");

        tasks.log("Verify task Completed by [" + details.get("CompletedBy") + "].");
        Assert.assertTrue(tasksPage.verifyCompletedBy(details.get("Description"), details.get("CompletedBy")),
                "Completed by value not matched. Expected [" + details.get("CompletedBy") + "]");

        tasks.log("Verify task Completed on [" + details.get("CompletedOn") + "].");
        Assert.assertTrue(tasksPage.verifyCompletedOn(details.get("Description"), details.get("CompletedOn")),
                "Completed on value not matched. Expected [" + details.get("CompletedOn") + "]");

        //Reopen Button
        tasks.log("Verify Reopen button displayed.");
        Assert.assertTrue(tasksPage.isReopenButtonDisplayed(),
                "Reopen button not displayed on completed task.");

        //Close Icon
        tasks.log("Verify Close icon displayed.");
        Assert.assertTrue(tasksPage.isCloseIconDisplayed(),
                "Close icon not displayed.");

        //Verify active comment section.
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        tasks.log("Verify comment box label.");
        Assert.assertTrue(tasksPage.verifyCommentLabel(), "Comment place holder not displayed. [Leave a comment]");

        tasks.log("Add comment [" + comment + "].");
        tasksPage.addComment(comment);

        String commentData = testConfig.getClinicianName() + ", Today, " + getAssignedOn();
        tasks.log("Verify commented text should be [" + comment + "]");
        Assert.assertTrue(tasksPage.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        tasks.log("Verify Commented By and comment posted date time. Details [" + commentData + "].");
        Assert.assertTrue(tasksPage.verifyCommentDetails(comment, commentData),
                "Comment details not matched. Expected details [" + commentData + "]");

        //Reopen Button
        tasks.log("Verify Reopen button displayed.");
        Assert.assertTrue(tasksPage.isReopenButtonDisplayed(),
                "Reopen button not displayed on completed task.");

        //Close Icon
        tasks.log("Verify Close icon displayed.");
        Assert.assertTrue(tasksPage.isCloseIconDisplayed(),
                "Close icon not displayed.");
    }

    @Then("^verify cancel button functionality on (Patient|Tasks) page while creating new task$")
    public void verifyCancelButtonFunctionalityWhileCreatingNewTask(String arg0) throws Exception {
        TasksPage webPage = new TasksPage(web);
        if (arg0.equalsIgnoreCase("Patient")) {
            webPage.log("Click on Task icon.");
            webPage.clickOnTaskIcon();

            webPage.log("Click on New Task.");
            webPage.clickOnNewTask();
        } else {
            webPage.log("Search for patient mrn [" + testConfig.getPrimaryPatientMRN() + "].");
            webPage.searchForPatientInTasks(testConfig.getPrimaryPatientMRN());

            webPage.log("Click on New Task.");
            webPage.clickOnNewTaskOnTasks(testConfig.getPrimaryPatientMRN());
        }

        log.info("Create task details.");
        getTaskValue(true);

        log.info("Create task with all fields");
        webPage.log("Input task description as " + details.get("Description") + ".");
        webPage.inputTaskDescription(details.get("Description"));

        webPage.log("Select due date time.");
        String due = webPage.setDueDate();
        webPage.log("Due date time selected as " + due);

        webPage.log("Select Assigned To . [" + testConfig.getNurseName() + "]");
        webPage.selectAssigneeTo(testConfig.getNurseName());

        webPage.log("Click on cancel button.");
        webPage.clickOnCancel();

        webPage.log("Verify task with description [" + details.get("Description") + "] should not created.");
        Assert.assertFalse(webPage.isTaskPresent(details.get("Description")),
                "Task should not be created with description [" + details.get("Description") + "] after clicking on cancel button.");
    }

    /**
     * Mobile Scripts
     */

    @And("Search patient name under tasks")
    public void searchPatientNameUnderTasks() throws Exception {
        taskPageApp = new TasksPageApp(clinician);
        if (CustomUtils.mnr != null)
            patientMrn = CustomUtils.mnr;
        else
            patientMrn = testConfig.getPrimaryPatientMRN();

        taskPageApp.log("CLick on Tasks tab.");
        //Instead of this navigate to task by search patient and click on View task.
        taskPageApp.clickOnTasks();
        taskPageApp.log("Scroll till patient given MRN.");
        taskPageApp.scrollTillPatient(patientMrn);

        taskPageApp.log("Read open task value.");
        openTaskValue = taskPageApp.getOpenTasksValue(patientMrn);
        taskPageApp.log("Click on Add new task icon.");
        taskPageApp.clickOnAddNewTaskIcon(patientMrn);
    }

    @And("^Verify create new task in HCP app with (Required|All) fields$")
    public void verifyCreateNewTaskInHCPAppWithRequiredFields(String arg0) throws Exception {
        boolean flag = arg0.equals("All");
        tasks.log("Verify create task button should be disable when all fields are empty.");
        Assert.assertFalse(taskPageApp.verifyCreateButtonEnable(),
                "Create button should be disable before entering required field details.");

        tasks.log("Get values to create a task.");
        details = getTaskValue(true);

        tasks.log("Add task details.");
        taskPageApp.addNewTaskDetails(details, flag);

        tasks.log("Add task description.");
        taskPageApp.updateTaskDescription(details.get("Description"));

        if (flag) {
            tasks.log("Add due date.");
            details = taskPageApp.addDueDateTime(details);
            tasks.log("Add assignee.");
            taskPageApp.addAssignee(details.get("Assignee"));
        }

        tasks.log("Verify Created button should be enable after entering required fields.");
        Assert.assertTrue(taskPageApp.verifyCreateButtonEnable(),
                "Create button should be enable after entering required field details.");

        tasks.log("Click on create button.");
        taskPageApp.clickOnCreate();

        tasks.log("Get updated open task value.");
        String newOpenTaskValue = taskPageApp.getOpenTasksValue(patientMrn);

        tasks.log("Verifying old Open task value with latest Open task value.");
        Assert.assertFalse(openTaskValue.equalsIgnoreCase(newOpenTaskValue),
                "Open task count not updated after adding new task. Value before adding [" + openTaskValue + "] after adding [" + newOpenTaskValue + "].");

        tasks.log("Verify created task displayed under open tasks.");
        Assert.assertTrue(taskPageApp.verifyTaskAddedSuccessFully(patientMrn, details),
                "Added task with description [" + details.get("Description") + " not displayed.");

        if (flag) {
            tasks.log("Navigate to created task.");
            taskPageApp.viewTask(details.get("Description"));
            String expected = details.get("Assignee").strip(), actual = taskPageApp.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPageApp.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }
    }

    @Then("Verify comment on created task in HCP app")
    public void verifyCommentOnCreatedTaskInHCPApp() throws Exception {
        tasks.log("Navigate to created task.");
        taskPageApp.viewTask(details.get("Description"));

        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        tasks.log("Add comment in task. [" + comment + "].");
        taskPageApp.addComment(comment);

        tasks.log("Scroll till comment");
        taskPageApp.scrollComments();
        tasks.log("Verify commented value should match.");
        Assert.assertTrue(taskPageApp.verifyCommentAddSuccessfully(comment),
                "Comment [" + comment + "] not displayed.");
    }

    @And("^Verify edit a task in HCP app with (All|Required) fields$")
    public void verifyEditATaskInHCPAppWithRequiredFields(String arg0) throws Exception {
        tasks.log("Click on Edit task.");
        taskPageApp.clickOnEditTask();
        boolean flag = arg0.equals("All");
        String desc = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        details.replace("Description", desc);
        taskPageApp.updateTaskDescription(desc);
        if (flag) {
            tasks.log("Update due date.");
            details = taskPageApp.addDueDateTime(details);
            tasks.log("Update assignee name.");
            taskPageApp.addAssignee(details.get("Assignee"));
        }
        tasks.log("Click on update task.");
        taskPageApp.updateTask();

        desc = details.get("Description");
        Assert.assertTrue(taskPageApp.verifyUpdatedTaskDescription(desc),
                "Expected updated task description is [" + desc + "].");
        if (flag) {
            String expected = details.get("Assignee").strip(),
                    actual = taskPageApp.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPageApp.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }
    }

    @Then("Verify complete a task in HCP app")
    public void verifyCompleteATaskInHCPApp() throws Exception {
        tasks.log("Click on Complete task.");
        taskPageApp.completeATask();
        tasks.log("Click on Completed task section.");
        taskPageApp.clickOnCompletedTask(patientMrn);

        tasks.log("Verify task displayed under complete task section.");
        String desc = details.get("Description");
        Assert.assertTrue(taskPageApp.verifyTaskDisplayedUnderCompleted(desc),
                "Task with desc [" + desc + "] should be displayed.");
    }

    @And("Verify comment on completed task in HCP app")
    public void verifyCommentOnCompletedTaskInHCPApp() throws Exception {
        verifyCommentOnCreatedTaskInHCPApp();
    }

    @Then("Verify re-open completed task in HCP app")
    public void verifyReOpenCompletedTaskInHCPApp() throws Exception {
        tasks.log("Click on complete a task.");
        taskPageApp.completeATask();
        tasks.log("Click on Open task section.");
        taskPageApp.clickOnOpen(patientMrn);
        tasks.log("Verify re-open task should be displayed under Open task section.");
        String desc = details.get("Description");
        Assert.assertTrue(taskPageApp.verifyReOpenTask(desc),
                "Re-opened task should be displayed under Open task section with description [" + desc + "].");
    }

    @And("Verify comment on re-opened task in HCP app")
    public void verifyCommentOnReOpenedTaskInHCPApp() throws Exception {
        tasks.log("Navigate to task.");
        taskPageApp.viewTask(details.get("Description"));
        verifyCommentOnCreatedTaskInHCPApp();
    }

    @Then("^Verify edit re-open task in HCP app with (All|Required) fields$")
    public void verifyEditReOpenTaskInHCPAppWithAllFields(String arg0) throws Exception {
        tasks.log("Click on Edit task.");
        taskPageApp.clickOnEditTask();
        boolean flag = arg0.equals("All");
        String desc = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        details.replace("Description", desc);

        tasks.log("Update task description.");
        taskPageApp.updateTaskDescription(desc);
        if (flag) {
            tasks.log("Update task due date.");
            details = taskPageApp.addDueDateTime(details);
            tasks.log("Edit assignee.");
            taskPageApp.addAssignee(details.get("Assignee"));
        }
        tasks.log("Click on update task.");
        taskPageApp.updateTask();

        tasks.log("Verify task description.");
        Assert.assertTrue(taskPageApp.verifyUpdatedTaskDescription(desc),
                "Task description [" + desc + "] should be displayed after updating.");
        if (flag) {
            String expected = details.get("Assignee").strip(),
                    actual = taskPageApp.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPageApp.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }

    }

    @Then("Verify complete a re-open task in HCP app")
    public void verifyCompleteAReOpenTaskInHCPApp() throws Exception {
        verifyCompleteATaskInHCPApp();
    }

    @Then("Verify Tasks screen content")
    public void verifyTasksScreenContent() {
        taskPageApp = new TasksPageApp(clinician);
        taskPageApp.log("Verify Task title should be displayed.");
        Assert.assertTrue(taskPageApp.isTitleDisplayed(), "Task tab title not displayed.");

        taskPageApp.log("Verify Active tasks option should be displayed.");
        Assert.assertTrue(taskPageApp.isTabOptionDisplayed("Active"), "Active tab not displayed.");

        taskPageApp.log("Verify Archived tasks option should be displayed.");
        Assert.assertTrue(taskPageApp.isTabOptionDisplayed("Archived"), "Archived tab not displayed.");
    }

    @And("Go back to home screen and navigate to Tasks section")
    public void goBackToHomeScreenAndNavigateToTasksSection() throws Exception {
        taskPageApp.log("Click on Patients tab.");
        taskPageApp.clickOnPatients();

        taskPageApp.log("Click on Tasks tab.");
        taskPageApp.clickOnTasks();

        taskPageApp.log("Verify Task screen options.");
        verifyTasksScreenContent();
    }

    //XCUIElementTypeOther[@name="TR"]//following-sibling:: XCUIElementTypeCell[1]//*[@label='ic arrow down detail']
    //Open(3)
    //Completed(0)
    //XCUIElementTypeOther[@name='AS']//*[@name='ic add']

    //XCUIElementTypeNavigationBar[@name="New Task"]

    //XCUIElementTypeStaticText[@name="Task description*"]

    //XCUIElementTypeStaticText[@name="Due date & time"]
    //XCUIElementTypeStaticText[@name="Assign to"]
    //XCUIElementTypeStaticText[@name="Create"]

    //XCUIElementTypeImage[@name="ic-calendar"]
    //XCUIElementTypeButton[@name="ic edit task clear"]
    //On Jan 19,2022 at 8:27 PM

    //XCUIElementTypeImage[@name="ic-arrow-next-detail"]
    //XCUIElementTypeStaticText[@name="Select care team member"]


    //XCUIElementTypeButton[@name="Tasks"]

    //XCUIElementTypeNavigationBar[@name="Select care team member"]
    //XCUIElementTypeButton[@name="New Task"]
    //XCUIElementTypeStaticText[@name="Select care team member"]
    ////XCUIElementTypeStaticText[@name="Assign"]
    ////XCUIElementTypeStaticText[@name="Physicians"]

}
