package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.constants.scripts.ClinicInfo;
import com.biofourmis.biovitals.pagefactory.web.AddPatientPage;
import com.biofourmis.biovitals.pagefactory.web.HandOffPage;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.pagefactory.web.MyPatientAllPatientPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import org.apache.commons.math3.analysis.function.Add;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Log4j
public class MyPatientHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver web;
    @Autowired
    private Faker faker;
    @Autowired
    private TestConfig testConfig;
    @Autowired
    private ClinicInfo clinicInfo;


    private CustomUtils customUtils = new CustomUtils(null);
    private HomePage homePage;
    private MyPatientAllPatientPage myPatientAllPatientPage;
    private HandOffPage handOffPage;

    @And("Verify {string} add and remove patient form my patients")
    public void verifyAddAndRemovePatientFormMyPatients(String arg0) throws Exception {
        homePage = new HomePage(web);
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);
        handOffPage = new HandOffPage(web);

        myPatientAllPatientPage.log("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);

        boolean isPresentInMy = !homePage.isPatientPresent(arg0);
        boolean whatNext = false;

        if (isPresentInMy) {
            Assert.assertTrue(verifyRemovePatientFromMy(arg0),
                    "Patient should be removed from My patients.");
            Assert.assertTrue(verifyPatientPresentInAll(arg0),
                    "Patient should be displayed in All patients.");
            whatNext = false;
        } else {
            goToAllPatients();

            myPatientAllPatientPage.log("Search for patient.[" + arg0 + "].");
            homePage.searchPatient(arg0);
            Assert.assertTrue(verifyAddPatientIntoMy(arg0),
                    "Patient should be displayed under My patients.");
            Assert.assertTrue(verifyPatientPresentInAll(arg0),
                    "Patient should be displayed in All patients.");
            whatNext = true;
        }

        if (whatNext) {
            goToMyPatients();
            myPatientAllPatientPage.log("Search for patient.[" + arg0 + "].");
            homePage.searchPatient(arg0);
            Assert.assertTrue(verifyRemovePatientFromMy(arg0),
                    "Patient should be removed from My patients.");
        } else {
            goToAllPatients();

            myPatientAllPatientPage.log("Search for patient.[" + arg0 + "].");
            homePage.searchPatient(arg0);
            Assert.assertTrue(verifyAddPatientIntoMy(arg0),
                    "Patient should be displayed under My patients.");
        }
        Assert.assertTrue(verifyPatientPresentInAll(arg0),
                "Patient should be displayed in All patients.");

    }

    private boolean verifyPatientPresentInAll(String arg0) throws Exception {
        goToAllPatients();
        myPatientAllPatientPage.log("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);
        return !homePage.isPatientPresent(arg0);
    }

    private void goToAllPatients() throws Exception {
        myPatientAllPatientPage.log("Navigate to home.");
        homePage.goToHomePage();
        homePage.clickOnHamburger();
        myPatientAllPatientPage.log("Navigate to All patients.");
        homePage.navigateTo("All patients");
    }

    private void goToMyPatients() throws Exception {
        myPatientAllPatientPage.log("Navigate to home.");
        homePage.goToHomePage();
        homePage.clickOnHamburger();
        myPatientAllPatientPage.log("Navigate to My patient.");
        homePage.navigateTo("My patients");
    }


    private boolean verifyRemovePatientFromMy(String arg0) throws Exception {
        handOffPage.clickOnPatientOptions();
        handOffPage.clickOnOption("Remove from My patients");
        myPatientAllPatientPage.clickOnRemove();
        myPatientAllPatientPage.log("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);
        return homePage.isPatientPresent(arg0);
    }

    private boolean verifyAddPatientIntoMy(String arg0) throws Exception {
        handOffPage.clickOnPatientOptions();
        handOffPage.clickOnOption("Add to My patients");
        goToMyPatients();
        myPatientAllPatientPage.log("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);
        return !homePage.isPatientPresent(arg0);
    }

    @Then("Verify add and remove patient form my patients")
    public void verifyAddAndRemovePatientFormMyPatients() throws Exception {
        if (Objects.isNull(CustomUtils.mnr)) {
            CustomUtils.mnr = testConfig.getPrimaryPatientMRN();
            customUtils.setPatientDetails(testConfig.getPrimaryPatientMRN(), testConfig.getPrimaryPatientName());
        }
        verifyAddAndRemovePatientFormMyPatients(CustomUtils.mnr);
    }

    @Then("Verify My patient list displayed")
    public void verifyMyPatientListDisplayed() throws Exception {
        if (Objects.isNull(CustomUtils.mnr)) {
            CustomUtils.mnr = testConfig.getPrimaryPatientMRN();
            customUtils.setPatientDetails(testConfig.getPrimaryPatientMRN(), testConfig.getPrimaryPatientName());
        }
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);
        Assert.assertTrue(myPatientAllPatientPage.verifyPatientListDisplayed(),
                "My patient list not displayed.");

        homePage = new HomePage(web);
        myPatientAllPatientPage.log("Verify My patient with MRN/RF.ID [" + CustomUtils.mnr + "] should be displayed under My patients tab.");
        homePage.searchPatientWithoutWait(CustomUtils.mnr);
        Assert.assertFalse(homePage.isPatientPresent(CustomUtils.mnr),
                "My Patient not found, MRN/RF.ID [" + CustomUtils.mnr + "].");

        myPatientAllPatientPage.log("Verify Other patient with MRN/RF.ID [" + testConfig.getUnassignedPatientMRN() + "] should not be displayed under My patients tab.");
        homePage.searchPatientWithoutWait(testConfig.getUnassignedPatientMRN());
        Assert.assertTrue(homePage.isPatientPresent(testConfig.getUnassignedPatientMRN()),
                "Other Patient should not be present, MRN/RF.ID [" + testConfig.getUnassignedPatientMRN() + "].");
        homePage.searchPatientWithoutWait(testConfig.getUnassignedPatientMRN());
    }

    @Then("Navigate to patient {string} under all patients")
    public void navigateToPatientUnderMyAllPatients(String arg0) throws Exception {
        userShouldNavigateSuccessfullyToAllPatientScreen();
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);
        myPatientAllPatientPage.searchInput(arg0);
        myPatientAllPatientPage.clickPatientMRN(arg0);
    }

    @Then("Navigate to patient under all patients")
    public void navigateToPatientAllPatients() throws Exception {
        userShouldNavigateSuccessfullyToAllPatientScreen();

        String arg0 = testConfig.getUnassignedPatientMRN();
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);
        myPatientAllPatientPage.searchInput(arg0);
        myPatientAllPatientPage.clickPatientMRN(arg0);
    }

    @Then("Verify search input field is clickable, accept input and clear by clicking on cross\\(X) icon")
    public void verifySearchInputFieldIsClickableAcceptInputAndClearByClickingOnCrossXIcon() throws Exception {
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);

        myPatientAllPatientPage.log("Search box should be displayed.");
        Assert.assertTrue(myPatientAllPatientPage.verifySearchFieldDisplayed(),
                "Search box not displayed on my patient page.");

        myPatientAllPatientPage.log("Search input filed should be clickable.");
        Assert.assertTrue(myPatientAllPatientPage.verifySearchFieldClickable(),
                "Search input filed should be clickable.");

        myPatientAllPatientPage.log("Search input field should able to accept keys.");
        Assert.assertTrue(myPatientAllPatientPage.verifySearchFieldAcceptInput(),
                "User not able to type in search field.");

        myPatientAllPatientPage.log("Clear icon should be displayed.");
        Assert.assertTrue(myPatientAllPatientPage.verifyClickOnClearIcon(),
                "Clear icon should be displayed after typing in search input.");

        myPatientAllPatientPage.log("After clicking on clear icon, input value should be cleared.");
        Assert.assertTrue(myPatientAllPatientPage.verifySearchFieldClear(),
                "Input value(s) not cleared after click on clear icon.");
    }

    @And("Check the input data for search component")
    public void checkTheInputDataForSearchComponent() throws Exception {
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);
        myPatientAllPatientPage.searchInput("Search patient");
        Assert.assertTrue(myPatientAllPatientPage.verifyNoExactResultFind(),
                "");
    }

    @Then("Verify QR code displayed by navigating through overflow menu")
    public void verifyQRCodeDisplayedByNavigatingThroughOverflowMenu() throws Exception {
        homePage = new HomePage(web);
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);

        myPatientAllPatientPage.clickOnThreeDots();

        myPatientAllPatientPage.clickOnShowQR();

        myPatientAllPatientPage.refreshExpiredQR();

        Assert.assertTrue(myPatientAllPatientPage.QRCodeDisplayed(),
                "QR code not displayed.");
    }

    @And("Try to close the QR code popup page")
    public void tryToCloseTheQRCodePopupPage() throws Exception {
        Assert.assertTrue(myPatientAllPatientPage.closeQRContainer(),
                "QR code pop-up not close after click on cross(X) icon.");
    }

    @Then("User should navigate successfully to All patient screen")
    public void userShouldNavigateSuccessfullyToAllPatientScreen() throws Exception {
        homePage = new HomePage(web);
        homePage.log("Click on Hamburger.");
        homePage.clickOnHamburger();

        homePage.log("Navigate to All patients");
        homePage.navigateTo("All patients");
    }

    @Then("^Navigate to (All|My) patient screen$")
    public void userToAllPatientScreen(String arg0) throws Exception {
        homePage = new HomePage(web);
        homePage.log("Click on Hamburger.");
        homePage.clickOnHamburger();

        homePage.log("Navigate to " + arg0 + " patients");
        homePage.navigateTo(arg0 + " patients");
        homePage._waitForPageLoad();
    }

    @And("All patient list of My patient and Other patient")
    public void allPatientShouldShowListOfMyPatientAndOtherPatient() throws Exception {
        if (Objects.isNull(CustomUtils.mnr)) {
            CustomUtils.mnr = testConfig.getPrimaryPatientMRN();
            customUtils.setPatientDetails(testConfig.getPrimaryPatientMRN(), testConfig.getPrimaryPatientName());
        }
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);

        Assert.assertTrue(myPatientAllPatientPage.verifyPatientListDisplayed(),
                "Patient(s) list displayed.");

        myPatientAllPatientPage.log("Verify My patient with MRN/RF.ID [" + CustomUtils.mnr + "] should be displayed under All patients tab.");
        homePage.searchPatientWithoutWait(CustomUtils.mnr);
        Assert.assertFalse(homePage.isPatientPresent(CustomUtils.mnr),
                "My Patient not found, MRN/RF.ID [" + CustomUtils.mnr + "].");

        myPatientAllPatientPage.log("Verify Other patient with MRN/RF.ID [" + testConfig.getUnassignedPatientMRN() + "] should be displayed under All patients tab.");
        Assert.assertFalse(homePage.isPatientPresent(testConfig.getUnassignedPatientMRN()),
                "Other Patient not found, MRN/RF.ID [" + testConfig.getUnassignedPatientMRN() + "].");
        homePage.searchPatientWithoutWait(testConfig.getUnassignedPatientMRN());
    }


    @Then("Verify No caregiver message")
    public void VerifyNoCareGiverMessage() {
        myPatientAllPatientPage = new MyPatientAllPatientPage(web);
        Assert.assertTrue(myPatientAllPatientPage.isMessageDisplayed("No clinician has been assigned"));
    }

    @Then("Add a patient with clinician only")
    public void AddAPatientWithClinicianOnly() throws Exception {

        homePage = new HomePage(web);
        AddPatientPage addPatientPage = new AddPatientPage(web);

        List<String> physician = List.of(testConfig.getClinicianName());
        List<String> nurse = List.of();

        Map<String, String> values = new HashMap<>();
        values.put("MRN", "MR" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Mob", "3" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Landline", "4" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Family_Pho_Num", "1" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Point_number", String.valueOf(faker.number().digits(10)));

        myPatientAllPatientPage.log("Try to create patient with details :" + customUtils.getDefaultPatientDetails(values));

        //Read Clinic Mandatory Fields
        myPatientAllPatientPage.log("Navigate to home page.");
        homePage.goToHomePage();

        myPatientAllPatientPage.log("Click on Add Patient.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");

        String[] patientOptions = clinicInfo.getAddPatientSteps().split(":");
        List<String> steps = new ArrayList<>();

        for (String option : patientOptions)
            if (option.contains("["))
                steps.add(option.substring(0, option.indexOf("[")).replaceAll("\\s", "").toLowerCase(Locale.ROOT));
            else
                steps.add(option.replaceAll("\\s", "").toLowerCase(Locale.ROOT));

        //For Personal Information page
        if (steps.contains("personalinformation")) {
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(values), false);
            addPatientPage.clickOnNext();
        }

        //For Medical Information page
        if (steps.contains("medicalinformation")) {
            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //For Care team information page
        if (steps.contains("careteam")) {
            log.info("Verify Care team information page.");
            addPatientPage.verifyCareTeamInfoPageRequiredFields(physician, nurse);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //Equipment information page
        if (steps.contains("equipmentinformation")) {
            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), true);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        log.info("Set QR Expire details.");
        customUtils.setQRExpireDetails(8);

        log.info("Save patient.");
        addPatientPage.savePatient();

        log.info("Update QR Expire details if required.");
        customUtils.updateQRExpireDetails(8);

        log.info("Set global MRN*");
        customUtils.setGlobalMNR(customUtils.getDefaultPatientDetails().get("MRN"));
        customUtils.setPatientDetails(customUtils.getDefaultPatientDetails().get("MRN"),
                customUtils.getDefaultPatientDetails().get("F_Name") + " " + customUtils.getDefaultPatientDetails().get("L_Name"));
        log.info("Set QR Hash details.");
        customUtils.setQrHashUrl(homePage.readQRUrl());

        log.info("Click on View patient option.");
        addPatientPage.clickOnViewPatientDetails();

        log.info("Verify new patient tutorial.");
        Assert.assertTrue(addPatientPage.verifyNewPatientTutorial(customUtils.getDefaultPatientDetails().get("MRN")),
                "Failed while verify new patient tutorial.");
    }

}
