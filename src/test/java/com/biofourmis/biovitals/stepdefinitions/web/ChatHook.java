package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.model.User;
import com.biofourmis.biovitals.pagefactory.patient.ContactPage;
import com.biofourmis.biovitals.pagefactory.web.ChatPage;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.en.*;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.sql.SQLOutput;
import java.util.*;

@Log4j
public class ChatHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver web;
    @Autowired
    private TestConfig testConfig;

    private ChatPage chatPage;
    private final CustomUtils customUtils = new CustomUtils(null);

    @And("^Verify navigate to chat section by clicking on chat icon on patient (home|details) screen$")
    public void verifyNavigateToChatSectionByClickingOnChatIconOnPatientHomeScreen(String arg0) throws Exception {
        chatPage = new ChatPage(web);
        boolean flag = arg0.equals("home");

        chatPage.log("Click on message icon.");
        chatPage.clickOnChatIconOnMyPatients();

        chatPage.log("Verify chat message container should be displayed.");
        Assert.assertTrue(chatPage.verifyChatWindowDisplayed(), "Chat messages container header should be displayed.");

        //chatPage.log("Adding call options in list.");
        List<String> chatScreenOptions = Arrays.asList("Video call", "Audio call");

        for (String option : chatScreenOptions) {
            chatPage.log("Verify " + option + " option is displayed.");
            Assert.assertTrue(chatPage.verifyChatScreenComponents(option), option + " not displayed.");
        }

        chatPage.log("Verify close (X) button on chat container should be displayed.");
        Assert.assertTrue(chatPage.isCloseChatButtonVisible(), "Close chat button not displayed.");

        chatPage.log("Verify input chat box should be displayed.");
        Assert.assertTrue(chatPage.isInputChatBoxVisible(), "Chat input box place holder [Write a message] not visible.");

        chatPage.log("Verify [Send] button should be displayed on chat container.");
        Assert.assertTrue(chatPage.isSendChatButtonVisible(), "Send button not visible on chat container.");

        chatPage.log("Verify by clicking on close icon chat container should be closed.");
        Assert.assertFalse(chatPage.closeChatContainer(), "Chat container should be close after clicking on close icon.");
    }

    @Then("Verify sending {int} text message from HCP web dashboard")
    public void verifySendingTextMessageFromHCPWebDashboard(int count) throws Exception {
        chatPage.log("Click on chat icon.");
        chatPage.clickOnChatIconOnMyPatients();

        chatPage.log("Verify chat container should be open.");
        Assert.assertTrue(chatPage.verifyChatWindowDisplayed(), "Chat container should be displayed.");

        log.info("Remove all chat messages from dic.");
        customUtils.resetChatData();

        for (int i = 0; i < count; i++) {
            String textMessage = customUtils.getMessageText(false);

            chatPage.log("Send text message [" + textMessage + "]");
            chatPage.sendTextMessage(textMessage);

            chatPage.log("Verify message sent successfully.");
            Assert.assertTrue(chatPage.verifySentTextMessage(textMessage), "Last sent message [" + textMessage + "] not listed in chat section.");
        }
    }

    @Then("Verify sending text message for other patient from HCP web dashboard")
    public void verifySendingTextMessageOtherFromHCPWebDashboard() throws Exception {

        chatPage.log("");
        chatPage.navigateToChatForOther();

        chatPage.log("Verify chat container should be open.");
        Assert.assertTrue(chatPage.verifyChatWindowDisplayed(), "Chat container should be displayed.");

        log.info("Remove all chat messages from dic.");
        customUtils.resetChatData();

        for (int i = 0; i < 1; i++) {
            String textMessage = customUtils.getMessageText(false);

            chatPage.log("Send text message [" + textMessage + "]");
            chatPage.sendTextMessage(textMessage);

            chatPage.log("Verify message sent successfully.");
            Assert.assertTrue(chatPage.verifySentTextMessage(textMessage), "Last sent message [" + textMessage + "] not listed in chat section.");
        }

//        chatPage.log("End Call");
//        chatPage.endCall();
    }

    @And("^Verify start (Audio|Video) call on patient (home|details) screen$")
    public void verifyStartAVideoCallOnPatientHomeScreen(String arg0, String arg1) throws Exception {
        chatPage = new ChatPage(web);
        boolean isVideo = arg0.equalsIgnoreCase("Video");

        if (isVideo) {
            chatPage.log("Click on Video call option.");
            chatPage.clickOnVideoCallIconOnMyPatients();
        } else {
            chatPage.log("Click on Audio call option.");
            chatPage.clickOnVoiceCallIconOnMyPatients();
        }

        chatPage.log("Verify call initiated successfully.");
        Assert.assertTrue(chatPage.isCallInitiated(), arg0 + " call not started.");

        Assert.assertTrue(chatPage.isCallEndButtonDisplayed(), "Call button not displayed.");
        Assert.assertTrue(chatPage._isTextContainsPresent("Calling..."), "");
        //System.out.println(chatPage._isTextContainsPresent("Call connection time out"));
        Assert.assertTrue(chatPage.isParticipantContainerDisplayed(), "Call participant container not displayed.");
        Assert.assertTrue(chatPage._isTextContainsPresent(testConfig.getClinicianName() + " (You)"), "");
        Assert.assertTrue(chatPage.isMaximumMinIconDisplayed(), "Minimize-Maximize button not displayed in call container.");

        chatPage.log("Click on End call button.");
        chatPage.clickOnEndCall();
        Assert.assertTrue(chatPage._isTextContainsPresent("End call"), "End call button should be displayed.");
    }

    @And("^Verify start (Audio|Video) call on patient (home|details) screen and cut$")
    public void verifyStartAVideoCallOnPatientScreen(String arg0, String arg1) throws Exception {
        verifyStartAVideoCallOnPatientHomeScreen(arg0, arg1);

        if (arg0.equals("Audio") || arg0.equals("Video")) {
            chatPage.log("Verify decline call.");
            Assert.assertFalse(chatPage.declineCall(), "Failed to decline " + arg0 + " call.");
        }
    }

    @Then("^Receive (Audio|Video) call on HCP web dashboard$")
    public void receiveAudioCallOnHCPWebDashboard(String arg0) {
        chatPage = new ChatPage(web);

        chatPage.log("Verify receive " + arg0 + " call.");
        Assert.assertTrue(chatPage.verifyReceiveCall(arg0), "Incoming call container not displayed after waiting 60 seconds.");
        boolean video = arg0.equals("Video");

        chatPage.log("Verify incoming call container elements.");
        if (video)
            Assert.assertTrue(chatPage.verifyReceiveDialogElements("Video call from"), "[Video call from] text not visible.");
        else
            Assert.assertTrue(chatPage.verifyReceiveDialogElements("Audio call from"), "[Audio call from] text not visible.");

        Assert.assertTrue(chatPage.verifyReceiveDialogElements("Accept"), "Accept button not visible.");
        Assert.assertTrue(chatPage.verifyReceiveDialogElements("Reject"), "Reject button not visible.");
    }

    @And("^Verify (Reject|Accept) call from HCP web dashboard$")
    public void verifyDeclineCallFromHCPWebDashboard(String arg0) throws Exception {
        chatPage = new ChatPage(web);

        chatPage.log(arg0 + " the call.");
        if (arg0.equals("Accept"))
            Assert.assertFalse(chatPage.verifyAcceptCall(), "Failed while accept the call.");
        else
            Assert.assertFalse(chatPage.verifyDeclineCall(), "Failed while reject the call.");
    }

    @Then("Verify End call from HCP Web dashboard")
    public void verifyEndCallFromHCPWebDashboard() throws Exception {
        chatPage = new ChatPage(web);
        chatPage.log("End the call.");
        Assert.assertFalse(chatPage.endCall(), "Failed while clicking on end call button.");
    }

    @Then("^Verify call (Rejected|Accepted) successfully in HCP web dashboard$")
    public void verifyCallRejectedSuccessfullyInHCPWebDashboard(String arg0) {
        chatPage = new ChatPage(web);

        if (arg0.equals("Rejected"))
            Assert.assertFalse(chatPage.verifyCallRejected(), "Call not rejected.");
        else
            Assert.assertTrue(chatPage.verifyCallAccepted(), "Call not accepted.");
    }

    @And("Verify Call ended successfully in HCP Web dashboard")
    public void verifyCallEndedSuccessfullyInHCPWebDashboard() {
        chatPage = new ChatPage(web);
        chatPage.log("verify call ended successfully.");
        Assert.assertFalse(chatPage.verifyCallAccepted(), "Call not ended successfully.");
    }

    @And("Verify badge count on HCP web dashboard")
    public void verifyBadgeCountAndChatOnHCPWebDashboard() throws Exception {
        chatPage = new ChatPage(web);
        int count = customUtils.getChat(true).size();

        //TODO GLOBAL BADGE COUNT
       // chatPage.log("Verify chat badge count.");
       // Assert.assertTrue(chatPage.verifyBadgeCountOnHome(count), "Badge count should be " + count);

        chatPage.log("Verify chat badge count on dashboard.");
        Assert.assertTrue(chatPage.verifyBadgeCount(count), "Badge count should be " + count);

        HomePage homePage = new HomePage(web);
        homePage.clickOnPatient(CustomUtils.mnr);
        homePage._waitForPageLoad();

        chatPage.log("Verify chat badge count patient vitals page.");
        Assert.assertTrue(chatPage.verifyBadgeCount(count), "Badge count should be " + count);
    }

    @And("^Start (Audio|Video) call$")
    public void startVoiceCall(String arg0) throws Exception {
        chatPage = new ChatPage(web);
        boolean isVideo = arg0.equalsIgnoreCase("Video");

        if (isVideo) {
            chatPage.log("Click on Video call option.");
            chatPage.clickOnVideoCallIconOnMyPatients();
        } else {
            chatPage.log("Click on Voice call option.");
            chatPage.clickOnVoiceCallIconOnMyPatients();
        }

        chatPage.log("Verify call initiated successfully.");
        Assert.assertTrue(chatPage.isCallInitiated(), arg0 + " call not started.");
    }

    @Then("Verify minimize and maximize chat container")
    public void verifyMinimizeAndMaximizeChatContainer() throws Exception {
        chatPage = new ChatPage(web);
        Assert.assertTrue(chatPage.verifyMinimizeContainer(),
                "Call container should be minimized in right bottom corner.");

        Assert.assertTrue(chatPage.verifyMaximizeContainer(),
                "Call container should be maximized.");
    }

    @Then("Verify perform action without minimize the call container")
    public void verifyPerformActionWithoutMinimizeTheCallContainer() throws Exception {
        chatPage = new ChatPage(web);
        chatPage.log("Hamburger should not be intractable while call container is maximized.");
        Assert.assertTrue(chatPage.clickOnHamburger(),
                "Hamburger should not be intractable.");
    }

    @And("Minimize the container and drag on other location")
    public void minimizeTheContainerAndDragOnOtherLocation() throws Exception {
        Assert.assertTrue(chatPage.verifyMinimizeContainer(),
                "Call container should be minimized in right bottom corner.");

        chatPage.log("Verify Call container can be draggable on screen when minimize.");
        Assert.assertTrue(chatPage.dragCallContainerToAnotherLoc(),
                "Not able to drag and drop call container ");
    }

    @Then("Perform action after minimize the call container")
    public void performActionAfterMinimizeTheCallContainer() throws Exception {
        chatPage = new ChatPage(web);
        chatPage.log("Hamburger should be intractable while call container is minimize.");
        Assert.assertTrue(chatPage.clickOnHamburger(),
                "Hamburger should be intractable and clicked.");
    }

    @And("Minimize the call container")
    public void minimizeTheCallContainer() throws Exception {
        chatPage = new ChatPage(web);
        Assert.assertTrue(chatPage.verifyMinimizeContainer(),
                "Call container should be minimized and container moved in right bottom corner of the screen.");
    }

    @Then("^Verify Video and Audio call icons are disable while clinician in call$")
    public void verifyVideoAndVoiceCallIconsAreDisableWhileClinicianInCall() throws Exception {
        chatPage = new ChatPage(web);

        chatPage.log("Verify Video and voice icons are disable.");
        Assert.assertTrue(chatPage.verifyVideoIconIsDisable(),
                "Video call icon should be disable while clinician is in call.");

        Assert.assertTrue(chatPage.verifyVoiceIconIsDisable(),
                "Voice call icon should be disable while clinician is in call.");

        chatPage.log("Click on message icon.");
        chatPage.clickOnMessageicon();

        Assert.assertTrue(chatPage.verifyVideoCallOptionInChatDisable(),
                "Video call option in chat section should be disable while clinician is in call.");

        Assert.assertTrue(chatPage.verifyVoiceCallOptionInChatDisable(),
                "Voice call option in chat section should be disable while clinician is in call.");

        Assert.assertTrue(chatPage.verifyVideoIconIsDisable(),
                "Video call icon should be disable while clinician is in call.");

        Assert.assertTrue(chatPage.verifyVoiceIconIsDisable(),
                "Voice call icon should be disable while clinician is in call.");
    }

    @Then("^Verify Video, Audio call and chat icons are (disable|invisible) for unassigned patients$")
    public void verifyVideoAndVoiceCallIconsAreDisableWhileClinician(String arg0) throws Exception {
        chatPage = new ChatPage(web);

        chatPage.log("Verify video, voice and chat icons should be " + arg0 + " for unassigned patients.");
        if (arg0.equals("disable")) {
            Assert.assertTrue(chatPage.verifyVideoIconIsDisable(),
                    "Video call icon should be disable while clinician is in call.");
            Assert.assertTrue(chatPage.verifyVoiceIconIsDisable(),
                    "Video call icon should be disable while clinician is in call.");
            Assert.assertTrue(chatPage.verifyChatIconIsDisable(),
                    "Chat icon should be disable while clinician is in call.");
        } else {
            Assert.assertTrue(chatPage.verifyVideoIconIsInvisible(),
                    "Video call icon should not be displayed while clinician is in call.");
            Assert.assertTrue(chatPage.verifyVoiceIconIsInvisible(),
                    "Voice call icon should not be displayed while clinician is in call.");
            Assert.assertTrue(chatPage.verifyChatIconIsInvisible(),
                    "Chat icon should not be displayed while clinician is in call.");
        }
    }

    @And("Verify sending text message from HCP web dashboard")
    public void verifySendingTextMessageFromHCPWebDashboard() throws Exception {
        chatPage.log("Verify chat container should be open.");
        Assert.assertTrue(chatPage.verifyChatWindowDisplayed(), "Chat container should be displayed.");

        log.info("Remove all chat messages.");
        customUtils.resetChatData();
        String textMessage = customUtils.getMessageText(false);

        chatPage.log("Send text message [" + textMessage + "]");
        chatPage.sendTextMessage(textMessage);

        chatPage.log("Verify message sent successfully.");
        Assert.assertTrue(chatPage.verifySentTextMessage(textMessage), "Last sent message [" + textMessage + "] not listed in chat section.");

    }

    @Then("Verify call connectivity more than {int} min")
    public void verifyCallConnectivityMoreThanMin(int arg0) throws Exception {
        chatPage = new ChatPage(web);
        chatPage.log("Waiting for " + arg0 + " minute(s) during calling.");
        chatPage.waitForMinutes(arg0);

        Assert.assertTrue(chatPage.verifyOnGoingCall(),
                "Call should be going on till " + arg0 + " minutes.");

        chatPage.log("End the call.");
        chatPage.endCall();
    }

    @And("Verify Video, Audio call options are disable for other patients")
    public void verifyVideoAudioCallOptionsAreDisableForOtherPatients() {
        chatPage.log("Video call should be disabled for other patients.");
        Assert.assertTrue(chatPage.verifyVideoIconDisableForOther(), "Video call should be disabled for other patients.");

        chatPage.log("Audio call should be disabled for other patients.");
        Assert.assertTrue(chatPage.verifyAudioIconDisableForOther(), "Audio call should be disabled for other patients.");

        try {
            chatPage.log("End the call.");
            chatPage.endCall();
        } catch (Exception e) {
            //For resetting the content.
        }
    }

    @And("Verify received text message in HCP web dashboard")
    public void verifyReceivedTextMessageInHCPWebDashboard() throws Exception {
        chatPage = new ChatPage(web);
        chatPage.log("Click on chat badge and navigate to chat container.");
        chatPage.clickOnBadgeAndNavigateToChat();

        ArrayList<String> textMessages = customUtils.getChat(true);
        Assert.assertEquals(chatPage.verifySentTextMessage(textMessages), textMessages, "Chat messages(s) [" + textMessages + "] not matched.");
    }

    @And("Verify Video call container details in HCP web dashboard")
    public void verifyVideoCallContainerDetailsInHCPWebDashboard() throws Exception {
        Assert.assertTrue(chatPage.isPatientNameDisplayedInCallContainer(customUtils.getDefaultPatientDetails().get("F_Name"),customUtils.getDefaultPatientDetails().get("L_Name")),
                "Patient name [" + customUtils.getPatientDetails(testConfig).get("Name") + "] should be displayed in call container.");
    }
}
