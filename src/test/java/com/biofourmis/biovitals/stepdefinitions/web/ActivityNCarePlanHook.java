package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.scripts.ClinicInfo;
import com.biofourmis.biovitals.pagefactory.web.ActivityNCarePlanPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.annotations.Optional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Log4j
public class ActivityNCarePlanHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver web;
    @Autowired
    private ClinicInfo clinicInfo;

    private ActivityNCarePlanPage activityNCarePlanPage;
    private CustomUtils customUtils = new CustomUtils(null);

    @Then("Verify Care plan page")
    public void verifyCarePlanPage() throws Exception {
        activityNCarePlanPage = new ActivityNCarePlanPage(web);

        activityNCarePlanPage.log("Navigate to Activity & Care plan.");
        activityNCarePlanPage.navigateToActivityNCarePlan();

        activityNCarePlanPage.log("Verify Care plan panel items [Care plan, Add a plan and it's icon].");
        Assert.assertTrue(activityNCarePlanPage.verifyCarePlanPanelDetails(), "Add Care plan icon not visible.");

        activityNCarePlanPage.log("Verify Add care plan input field.");
        Assert.assertTrue(activityNCarePlanPage.verifyAddPlanTextClickable(), "Placeholder for Add care plan input field not matched.");

        activityNCarePlanPage.log("Verify click on cancel Add care plan input field.");
        Assert.assertTrue(activityNCarePlanPage.verifyCancelPlanInput(), "Add care plan input field not canceled.");

        activityNCarePlanPage.log("Verify Add care plan icon is clickable.");
        Assert.assertTrue(activityNCarePlanPage.verifyAddPlanIconClickable(), "Add care plan icon not clickable.");

        activityNCarePlanPage.log("Verify click on cancel Add care plan input field.");
        Assert.assertTrue(activityNCarePlanPage.verifyCancelPlanInput(), "Add care plan input field not canceled.");
    }

    @Then("Verify Add a Care plan")
    public void verifyAddACarePlan() throws Exception {
        activityNCarePlanPage = new ActivityNCarePlanPage(web);

        activityNCarePlanPage.log("Navigate to Activity & Care plan.");
        activityNCarePlanPage.navigateToActivityNCarePlan();

        String carePlan = customUtils.getCarePlan();
        activityNCarePlanPage.log("Verify Add a care plan. Message : [" + carePlan + "]");
        Assert.assertTrue(activityNCarePlanPage.addCarePlan(carePlan),
                "Failed while Add a care plan. Message : [" + carePlan + "]");
    }

    @And("Verify Delete a Care plan")
    public void verifyDeleteACarePlan() throws Exception {
        activityNCarePlanPage = new ActivityNCarePlanPage(web);

        String carePlan = customUtils.getCarePlan();
        activityNCarePlanPage.log("Verify delete a care plan. Message : [" + carePlan + "].");
        Assert.assertFalse(activityNCarePlanPage.deleteCarePlan(carePlan),
                "Failed while delete a care plan. Message : [" + carePlan + "].");
    }

    @Then("Verify Activity Page details of clinic")
    public void verifyActivityPageDetails() throws Exception {
        activityNCarePlanPage = new ActivityNCarePlanPage(web);

        activityNCarePlanPage.log("Navigate to Activity & Care plan.");
        activityNCarePlanPage.navigateToActivityNCarePlan();

        activityNCarePlanPage.log("Read data from feature file of activities.");
        String[] activitiesNames = clinicInfo.getActivities().split(":");

        List<String> activities = new ArrayList<>();
        for (String names : activitiesNames)
            activities.add(names.strip());

        activityNCarePlanPage.log("Verify Activity & Care plan page details.");
        Assert.assertTrue(activityNCarePlanPage.verifyActivityPageDetails(), "Today's date not displayed on Activity timeline page.");

        activityNCarePlanPage.log("Verify Adherence Filter items.");
        Assert.assertTrue(activityNCarePlanPage.verifyAdherenceFilter(), "[Apply filter] option not displayed after clicking on Adherence filter.");

        activityNCarePlanPage.log("Verify Activities [" + activities + "] available in filter.");
        List<String> actual = activityNCarePlanPage.verifyAdherenceFilterItems();
        Assert.assertEquals(actual, activities, "Filter activities are not matched with expected activities are [" + activities + "] and actual activities are [" + actual + "].");

        activityNCarePlanPage.log("Verify Activities [" + activities + "] available in LHN menu.");
        System.out.println(activityNCarePlanPage.verifyActivities(activities));
        Assert.assertTrue(true, "LHN menu activities are not matched with expected activities [" + activities + "].");

        activityNCarePlanPage.log("Verify activities timeline for clinic " + clinicInfo.getClinicName().toUpperCase(Locale.ROOT));
        ArrayList<String> actualTimeLine = activityNCarePlanPage.verifyActivityTimeline();
        String message = "Activity timeline not matched with expected timeline for clinic " + clinicInfo.getClinicName().toUpperCase(Locale.ROOT) + ". Actual [" + actualTimeLine + "], ";
        ArrayList<String> expectedTimeLine;
        expectedTimeLine = customUtils.getTimeline(clinicInfo.getActivityTimeline());
        Assert.assertTrue(compareTimeLine(expectedTimeLine, actualTimeLine), message + "Expected [" + expectedTimeLine + "].");
    }

    @Then("Verify Add an Activity {string}")
    public void verifyAddAnActivity(String arg0) throws Exception {
        activityNCarePlanPage = new ActivityNCarePlanPage(web);

        activityNCarePlanPage.log("Navigate to Activity & Care plan.");
        activityNCarePlanPage.navigateToActivityNCarePlan();

        activityNCarePlanPage.log("Get next available timeline.");
        String nextTimeLine = customUtils.nextAvailableTimeline(clinicInfo.getActivityTimeline());
        activityNCarePlanPage.log("Next available time slot is :" + nextTimeLine);
        activityNCarePlanPage.log("Will add activity " + arg0 + "in timeline.");
        String addedActivityDetailedMessage = "";
        switch (arg0) {
            case "Use inhaler":
            case "Use nebulizer":
                activityNCarePlanPage.log("Add activity "+ arg0);
                customUtils.setActivityDetails(arg0, nextTimeLine, "");

                addedActivityDetailedMessage = "Add activity in calender timeline." +
                        " With details [ Activity Name :" + arg0 + ", Next Timeline :"
                        + nextTimeLine;
                activityNCarePlanPage.log(addedActivityDetailedMessage);
                activityNCarePlanPage.addActivityUseInhaler(arg0, nextTimeLine);
                break;
            case "Inject insulin":
                activityNCarePlanPage.log("Add Inject insulin");
                break;
            case "Check blood pressure":
                activityNCarePlanPage.log("Add Check blood pressure");
                break;
            case "Drink water":
                activityNCarePlanPage.log("Set activity details.");
                //No of glass: 2
                String noOfGlass = "2";
                customUtils.setActivityDetails(arg0, nextTimeLine, noOfGlass);

                Integer no = Integer.parseInt(noOfGlass);
                addedActivityDetailedMessage = "Add activity in calender timeline." +
                        " With details [ Activity Name :" + arg0 + ", Next Timeline :"
                        + nextTimeLine + ", No of Glass :" + no;
                activityNCarePlanPage.log(addedActivityDetailedMessage);
                activityNCarePlanPage.addActivityDrinkWater(arg0, nextTimeLine, no);
                break;
        }

        activityNCarePlanPage.log("Verify activity is added successfully.");
        Assert.assertTrue(activityNCarePlanPage.verifyActivityAdded(arg0),
                "Failed to add activity with details [" + addedActivityDetailedMessage + "].");
    }

    @Then("Verify Update an Activity {string}")
    public void verifyUpdateAndActivity(String arg0) throws Exception {
        String updateActivityDetailedMessage = "";
        String nextUpdatedTime = "";
        ArrayList<String> timeline = customUtils.getTimeline(clinicInfo.getActivityTimeline());

        activityNCarePlanPage.log("Get next available timeline.");
        String nextTimeline = timeline.get(timeline.indexOf(customUtils.nextAvailableTimeline(clinicInfo.getActivityTimeline())) + 1);
        activityNCarePlanPage.log("Next available time slot is :" + nextTimeline);
        activityNCarePlanPage.log("Will update activity " + arg0 + "in timeline.");
        switch (arg0) {
            case "Use inhaler":
            case "Use nebulizer":
                activityNCarePlanPage.log("Use inhaler");
                updateActivityDetailedMessage = "Updating activity in calender timeline." +
                        " With details [ Activity Name :" + arg0 + ", Next Timeline :"
                        + nextTimeline;
                customUtils.updateActivityDetails(arg0, nextTimeline);
                activityNCarePlanPage.log(updateActivityDetailedMessage);
                nextUpdatedTime = activityNCarePlanPage.verifyUpdateDrinkWaterActivity(arg0, nextTimeline);
                break;
            case "Inject insulin":
                activityNCarePlanPage.log("Inject insulin");
                break;
            case "Check blood pressure":
                activityNCarePlanPage.log("Check blood pressure");
                break;
            case "Drink water":
                activityNCarePlanPage.log("Update activity details.");
                updateActivityDetailedMessage = "Updating activity in calender timeline." +
                        " With details [ Activity Name :" + arg0 + ", Next Timeline :"
                        + nextTimeline;
                customUtils.updateActivityDetails(arg0, nextTimeline);
                activityNCarePlanPage.log(updateActivityDetailedMessage);
                nextUpdatedTime = activityNCarePlanPage.verifyUpdateDrinkWaterActivity(arg0, nextTimeline);
                break;
        }

        activityNCarePlanPage.log("Verify activity is updated successfully.");
        Assert.assertTrue(nextTimeline.equalsIgnoreCase(nextUpdatedTime),
                "Failed to update activity with details [" + updateActivityDetailedMessage + "].");
    }

    @Then("Verify Delete an Activity {string}")
    public void verifyDeleteAndActivity(String arg0) throws Exception {
        activityNCarePlanPage.log("Delete an activity [" + arg0 + "].");
        activityNCarePlanPage.verifyDeleteAnActivity(arg0);

        activityNCarePlanPage.log("Verify activity [" + arg0 + "] deleted successfully.");
        Assert.assertTrue(activityNCarePlanPage.isActivityDeleted(arg0), "Activity not deleted [" + arg0 + "].");
    }

    private boolean compareTimeLine(ArrayList<String> timeline, ArrayList<String> verifyActivityTimeline) {
        ArrayList<String> dataSet = new ArrayList<>(verifyActivityTimeline);
        activityNCarePlanPage.log("Comparing timeline.");
        for (String data : timeline)
            dataSet.remove(data);
        return dataSet.size() == 0;
    }

    @And("Adding a care plan should not be applicable for other patients")
    public void addingACarePlanShouldNotBeApplicableForOtherPatients() throws Exception {
        activityNCarePlanPage = new ActivityNCarePlanPage(web);
        activityNCarePlanPage.navigateToActivityNCarePlan();
        activityNCarePlanPage.verifyAddCarePlanIsNotVisible();
    }
}
