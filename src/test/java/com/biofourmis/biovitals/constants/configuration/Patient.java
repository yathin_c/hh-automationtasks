package com.biofourmis.biovitals.constants.configuration;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Patient {

    @JsonAlias({"patientName", "Patient Name" })
    private String patientName;
    @JsonAlias({"mrn", "MRN" })
    private String mrn;
}
