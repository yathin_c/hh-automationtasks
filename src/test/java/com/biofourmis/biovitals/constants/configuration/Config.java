package com.biofourmis.biovitals.constants.configuration;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class Config {

    @JsonAlias({ "property", "Property" })
    private String property;
    @JsonAlias({ "value", "Value" })
    private String value;
    @JsonAlias({ "description", "Description" })
    private String description;
}
