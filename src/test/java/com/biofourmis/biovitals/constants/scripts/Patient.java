package com.biofourmis.biovitals.constants.scripts;


import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Patient {

    String mrn;
    String fName;
    String lName;
    String gender;
    String dob;
    String addressLine1;
    String addressLine2;
    Integer zipCode;
    String state;
    Integer patientMob;
    Integer patientLandLine;
    String preferredLang;
    Integer familyPhone;
    String familialRelation;
    String codeStatus;
    String allergies;
    String contactPrecautions;
    String admittingDiagnosis;
    String clinicalSummary;
    String physicians;
    String nursesParamedics;
    String lengthOfStay;
    String devices;
    String documents;
    boolean shipUsePatientAddress;
    String shipAddress1;
    String shipAddress2;
    Integer shipZipCode;
    String shipCity;
    String shipState;
    String enrollmentType;
    String pointOfContact;
    String pointFName;
    String pointLName;
    Integer pointPhoneNumber;
    String emailEquipment;
    String qrStart;
    String qrEnd;
}
