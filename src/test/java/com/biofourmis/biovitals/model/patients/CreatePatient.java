package com.biofourmis.biovitals.model.patients;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreatePatient {
    String mrn;
    String firstName;
    String middleName;
    String lastName;
    String dob;//08/12/1987
    String gender;
    String mobileNo;
    String phoneNo;
    Address address;
    FamilyContact[] familyContacts;
    String locale;
    String role;
    String site;
    String[] contacts;
    MedicalHistory medicalHistory;
    @JsonIgnore
    EquipmentInfo equipmentInfo;
}
