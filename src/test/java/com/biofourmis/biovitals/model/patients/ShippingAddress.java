package com.biofourmis.biovitals.model.patients;

import lombok.Builder;

@Builder
public class ShippingAddress {
    String addressLine1;
    String addressLine2;
    String city;
    String state;
    String zip;
}
