package com.biofourmis.biovitals.model.patients;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AttentionSummary {

    Boolean isTitrationReady;
    Boolean labsDue;
    Integer openEvents;
}


