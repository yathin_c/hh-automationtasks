package com.biofourmis.biovitals.model.patients;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Address {
    //String[] location;
    String addressLine1;
    String addressLine2;
    String city;
    String state;
    String zip;
}
