package com.biofourmis.biovitals.model.patients;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Everion {
    @JsonIgnore
    String reason;
    @JsonIgnore
    String code;
    @JsonIgnore
    Integer status;
    @JsonIgnore
    String bluetoothIcon;
    @JsonIgnore
    String deviceIcon;
    @JsonIgnore
    String label;
    @JsonIgnore
    String deviceKey;
    @JsonIgnore
    String batteryLevel;
    @JsonIgnore
    String globalError;
    @JsonIgnore
    String bluetoothError;
    @JsonIgnore
    String shortReason;
    @JsonIgnore
    String shortBluetoothReason;
    @JsonIgnore
    String bluetoothCode;
    @JsonIgnore
    String globalErrorIcon;
    @JsonIgnore
    Boolean isOnCharger;
    @JsonIgnore
    String id;
    @JsonIgnore
    Long updatedAt;
    @JsonIgnore
    Long lastSyncTime;
}
