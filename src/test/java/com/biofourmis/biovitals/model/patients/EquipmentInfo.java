package com.biofourmis.biovitals.model.patients;

import lombok.Builder;

@Builder
public class EquipmentInfo {
    String trialDuration;
    String pointOfContact;
    String enrollmentType;
    String firstName;
    String middleName;
    String lastName;
    String phoneNo;
    ShippingAddress shippingAddress;
    DevicesKits devicesKits;
    String[]  kitDocuments;
    Boolean sameAsPatient;
    Boolean isEmailSent;
}
