package com.biofourmis.biovitals.model.patients;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Rules {
    String  bfrid;
    String category;
    Integer thresh;
    String cond;
    String attribute;
    String symptom;
    String id;
    String unit;
    Integer rangeMin;
    Integer rangeMax;

}
