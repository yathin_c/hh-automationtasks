package com.biofourmis.biovitals.model.patients;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VitalPatch {
    String reason;
    String code;
    Integer status;
    String bluetoothIcon;
    String deviceIcon;
    String label;
    String deviceKey;
    String batteryLevel;
}
