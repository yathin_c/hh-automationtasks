package com.biofourmis.biovitals.model.patients;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Patients {

    String id;
    @JsonIgnore
    String mrn;
    @JsonIgnore
    String dob;
    @JsonIgnore
    String firstName;
    @JsonIgnore
    String middleName;
    @JsonIgnore
    String lastName;
    @JsonIgnore
    String locale;
    @JsonIgnore
    String gender;
    @JsonIgnore
    String mobileNo;
    @JsonIgnore
    String phoneNo;
    @JsonIgnore
    Address address;
    @JsonIgnore
    String role;
    @JsonIgnore
    Integer status;
    @JsonIgnore
    String site;
    @JsonIgnore
    AttentionSummary attentionSummary;
    @JsonIgnore(value = true)
    MedicalHistory medicalHistory;
    @JsonIgnore
    String timezone;
    @JsonIgnore
    Integer tzOffset;
    @JsonIgnore
    String patientId;
    @JsonIgnore
    Long updatedAt;
    @JsonIgnore
    Vitals vitals;
    @JsonIgnore
    VitalsAt vitalsAt;
    @JsonIgnore
    VitalsFrom vitalsFrom;
    @JsonIgnore
    Rules[] rules;
    @JsonIgnore
    DeviceInfo deviceInfo;
    @JsonIgnore
    Tutorials tutorials;
    @JsonIgnore
    ConnectionStatus connectionStatus;
    @JsonIgnore
    ConnectionReasons connectionReasons;
    @JsonIgnore
    Boolean connected;
    @JsonIgnore
    PrimaryContact primaryContact;
    @JsonIgnore
    FamilyContact[] familyContact;
    @JsonIgnore
    Contacts[] contacts;
    @JsonIgnore
    Long createdAt;
    @JsonIgnore
    String presence;
    @JsonIgnore
    String deactivationTime;
    @JsonIgnore
    Boolean isMyPatient;

    @JsonIgnore
    EquipmentInfo equipmentInfo;

    @JsonAlias({"emrAccountNumber", "EMRAccountNumber"})
    String emrAccountNumber;
    @JsonIgnore
    String managingProvider;

    @JsonIgnore
    String photo;

    @JsonIgnore
    String dischargeOption;
}
