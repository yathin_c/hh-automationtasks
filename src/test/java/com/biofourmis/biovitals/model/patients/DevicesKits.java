package com.biofourmis.biovitals.model.patients;

import lombok.Builder;

@Builder
public class DevicesKits {
    String label;
    String value;
    String parentLabel;
    String parentValue;
}
