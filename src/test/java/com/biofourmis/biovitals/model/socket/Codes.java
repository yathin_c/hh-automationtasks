package com.biofourmis.biovitals.model.socket;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Codes {
    @JsonIgnore
    @JsonAlias({"0", "zero"})
    private Integer zero;

    @JsonIgnore
    @JsonAlias({"200","twoHundred"})
    private Integer twoHundred;

    @JsonIgnore
    @JsonAlias({"500","fiveHundred"})
    private Integer fiveHundred;
}
