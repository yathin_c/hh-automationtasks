package com.biofourmis.biovitals.model.socket;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Intermediate {
    private String timestamp;
    private Integer scenariosCreated;
    private Integer scenariosCompleted;
    private Integer requestsCompleted;
    private Latency latency;
    private Rps rps;
    private ScenarioDuration scenarioDuration;
    @JsonIgnore
    private ScenarioCounts scenarioCounts;
    private Errors errors;
    @JsonIgnore
    private Codes codes;
    private Integer matches;
    private Long[] latencies;
    private CustomStats customStats;
    private Counters counters;
    private Integer scenariosAvoided;
    private Integer concurrency;
    private Integer pendingRequests;
}
