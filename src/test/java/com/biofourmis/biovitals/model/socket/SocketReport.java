package com.biofourmis.biovitals.model.socket;

import lombok.Data;

import java.util.ArrayList;

@Data
public class SocketReport {
    private Aggregate aggregate;
    private ArrayList<Intermediate> intermediate;
}
