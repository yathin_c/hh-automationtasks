package com.biofourmis.biovitals.model.socket;

import lombok.Data;

@Data
public class Rps {

    private Integer count;
    private Double mean;
}
