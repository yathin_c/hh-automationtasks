package com.biofourmis.biovitals.model.socket;

import lombok.Data;

@Data
public class Phases {
    private Integer arrivalRate;
    private Integer duration;
    private Integer maxVuser;
}
