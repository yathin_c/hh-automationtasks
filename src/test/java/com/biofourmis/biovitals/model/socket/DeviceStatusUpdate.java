package com.biofourmis.biovitals.model.socket;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeviceStatusUpdate {
    String batteryLevel;
    String bluetoothCode;
    String id;
    String identifier;
    String isOnCharger;
    String lastSyncTime;
    String manufacturer;
    String name;
    String status;
    String type;
    String typeLabel;
    String code;
}
