package com.biofourmis.biovitals.model.socket;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;

@Data
public class Aggregate {
    private String timestamp;
    private Integer scenariosCreated;
    private Integer scenariosCompleted;
    private Integer requestsCompleted;
    private Latency latency;
    private Rps rps;
    private ScenarioDuration scenarioDuration;
    @JsonIgnore
    private ScenarioCounts scenarioCounts;
    private Errors errors;
    @JsonIgnore
    private Codes codes;
    private Integer matches;
    private CustomStats customStats;
    private Counters counters;
    private Integer scenariosAvoided;
    private ArrayList<Phases> phases;
}
