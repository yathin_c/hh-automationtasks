package com.biofourmis.biovitals.model.socket;

import lombok.Data;

@Data
public class ScenarioDuration {
    private Double min;
    private Double max;
    private Double median;
    private Double p95;
    private Double p99;
}
