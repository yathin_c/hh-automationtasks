package com.biofourmis.biovitals.model;

import lombok.Data;

@Data
public class Program {

    private String name;
    private String id;
}
