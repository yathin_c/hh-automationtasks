package com.biofourmis.biovitals.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class Tutorials {
    @JsonAlias({"patientOnboarding","patient-onboarding"})
    private boolean patientOnboarding;
}
