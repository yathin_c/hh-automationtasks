package com.biofourmis.biovitals.model;

import lombok.Data;

@Data
public class UserNotes {
    String patient;
    String title;
    String description;
    String createdBy;
    Integer status;

}

