package com.biofourmis.biovitals.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NonNull;

@Data
public class User {


    String appId;
    String app_type;
    String email;
    String firstName;
    String id;
    String lastName;
    String locale;
    String passwordResetAt;
    String photo;
    String refreshToken;
    String reqSource;
    String resetToken;
    String role;
    String sub;

    @JsonIgnore
    Policies policies;
    Program program;
    Tutorials tutorials;
    Site site;
    Site[] sites;
}
