package com.biofourmis.biovitals.model;

public class Zapi {

    private String projectKey;
    private String testCaseKey;
    private String testCycleKey;
    private String statusName;
    //private List<TestScriptResult> testScriptResults = new ArrayList<TestScriptResult>();
    private String executedById;
    private String assignedToId;



    public String getProjectKey() {
        return projectKey;
    }
    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }
    public String getTestCaseKey() {
        return testCaseKey;
    }
    public void setTestCaseKey(String testCaseKey) {
        this.testCaseKey = testCaseKey;
    }
    public String getTestCycleKey() {
        return testCycleKey;
    }
    public void setTestCycleKey(String testCycleKey) {
        this.testCycleKey = testCycleKey;
    }
    public String getStatusName() {
        return statusName;
    }
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    //    public List<TestScriptResult> getTestScriptResults() {
//        return testScriptResults;
//    }
//    public void setTestScriptResults(List<TestScriptResult> testScriptResults) {
//        this.testScriptResults = testScriptResults;
//    }
    public String getExecutedById() {
        return executedById;
    }
    public void setExecutedById(String executedById) {
        this.executedById = executedById;
    }
    public void setAssignedToId(String executedById) {
        this.assignedToId = assignedToId;
    }

//    public class TestScriptResult {
//        private String statusName;
//        public String getStatusName() {
//            return statusName;
//        }
//        public void setStatusName(String statusName) {
//            this.statusName = statusName;
//        }
//    }
}
